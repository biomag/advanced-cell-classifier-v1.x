# Advanced-Cell-Classifier
Advanced Cell Classifier is a data analyzer program to evaluate cell-based high-content screens developed at ETH Zurich. The basic aim is to provide a very accurate analysis with minimal user interaction using advanced machine learning methods.

More info coming soon...

**Developers information:**

- From 2015.11.24. you have to start the ACC with startup command and not with ACC_main_GUI.