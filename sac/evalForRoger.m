sacInit();

files = {'segmBigRing.arff', 'segmSmallRing.arff', 'segmCyto.arff'};

for j=1:length(files)
    
    d = sacReadArff(files{j});
    
%     for i=1:length(classifiers)
                        
        sacSuggest(d, {'RandomForest_Weka', 'LogitBoost_Weka', 'MLP_Weka'}, 'TimeLimit', 400, 'random');
        
%     end
    
end
