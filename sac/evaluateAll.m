clear all;

d=sacReadArff('c:\projects\ETH_LMC\software\ACC_Roger\b1_1_md3.arff');

XMLList = dir('classifiers/xml/*.xml');

for i=1:length(XMLList)
    for j=1:3        
        tic;
        [bestOptionString{i, j}, bestOptionStruct{i, j}, bestE{i, j}] = sacSuggest(XMLList(i).name(1:end-4), d, 60, 'random');
        ACCMtx(i, j) = bestE{i, j}.ACC;
        AUCMtx(i, j) = bestE{i, j}.waAU;
        timers(i, j) = toc;
    end;
end;