clear all;


% list of features
% dFeatureList = [17,1,3,14,2,23,13,18,22,4,15,24,12,16,21,20,6,25,10,19,11,5,9,7,8];
% dFeatureList = [4,16,6,7,15,1,2,23,21,14,24,10,3,17,11,22,19,13,8,18,12,25,5,9,20];
%dFeatureList = [1,14,18,6,23,15,16,10,7,21,4,2,3,17,11,24,22,19,8,13,25,9,12,5,20];

%SVM5
%dFeatureList = [1,16,4,8,6,20,3,7,2,17,9,11,5,18,24,10,22,21,23,14,15,19,25,12,13];
% SVM1
% dFeatureList = [1,16,4,7,6,20,3,8,2,22,18,9,17,11,5,24,21,13,15,10,23,19,14,25,12];


% %Mara
% % load data set
% d=sacReadArff('c:\projects\ETH_LMC\students\KevinSmith\sac\data\mara01.arff');
% dFeatureList = [8,2,24,18,1,39,14,36,28,7,12,5,11,6,19,13,21,16,23,4,17,32,3,29,26,37,33,15,45,9,40,48,44,31,47,20,10,43,30,38,46,25,22,34,27,41,35,42];

%Segment
% load data set
% d=sacReadArff('c:\projects\ETH_LMC\software\ACC_Sarah_NucImport\SAS_NI.arff');
% dFeatureList = [2,3,107,44,106,40,109,154,163,60,157,50,49,158,164,41,46,159,45,55,111,62,43,152,155,166,28,51,96,23,97,103,48,47,53,52,69,70,67,68,74,75,71,73,72,58,59,54,57,56,65,66,61,64,63,14,15,12,13,19,20,16,18,17,5,6,1,4,10,11,7,9,8,34,35,32,33,39,42,36,38,37,24,25,21,22,30,31,26,29,27,132,133,130,131,137,138,134,136,135,123,124,121,122,128,129,125,127,126,151,153,149,150,162,165,156,161,160,142,143,139,141,140,147,148,144,146,145,88,89,86,87,93,94,90,92,91,79,80,76,78,77,84,85,81,83,82,114,115,112,113,119,120,116,118,117,100,101,95,99,98,108,110,102,105,104];
%SVM5
% dFeatureList = [16,17,18,11,2,19,14,12,13,10,6,8,1,15,4,7,9,5,3];


% Roger darmachom
d=sacReadArff('c:\projects\ETH_LMC\software\ACC_Roger_Dharmacon\DGW3MD3.arff');
%dFeatureList = [1,5,3,6,2,4,8,27,12,26,7,24,25,15,17,18,14,23,11,22,13,21,19,10,9];
dFeatureList = [5,3,4,8,27,6,15,26,2,7,25,1,10,24,9,23,12,22,20,21,11,14,19,18,17,16,13]



figure;

lengthFL = length(dFeatureList);

for i=1:lengthFL-1
    
    i
    
    pFeatureList = sort(dFeatureList(lengthFL-i+1:lengthFL), 'descend');
    dNew = d;
    
    for j=1:length(pFeatureList)
        dNew.featureNames(pFeatureList(j)) = [];
        dNew.featureTypes(pFeatureList(j)) = [];
        dNew.instances(:,pFeatureList(j)) = [];
    end;
    
    [y,p] = sacCrossValidate('MLP_Weka', dNew, 5, 1); 
    [E s] = sacEvaluate(dNew.labels, p, '-ACC -waAU -q');
    bestACC(i) = E.ACC;
    bestAU(i) = E.waAU;
    
    %     [bestOptionString, bestOptionStruct, bestE] = sacSuggest('SVM_LibSVM', dNew, 60, 'random');
    %     bestACC(i) = bestE.ACC;
    %     bestAU(i) = bestE.waAU;
    
    plot(bestACC); hold on;
    plot(bestAU); hold on;
    drawnow;
    
    
end;

