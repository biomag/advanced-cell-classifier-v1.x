clear all;


% list of features
% dFeatureList = [17,1,3,14,2,23,13,18,22,4,15,24,12,16,21,20,6,25,10,19,11,5,9,7,8];
% dFeatureList = [4,16,6,7,15,1,2,23,21,14,24,10,3,17,11,22,19,13,8,18,12,25,5,9,20];
%dFeatureList = [1,14,18,6,23,15,16,10,7,21,4,2,3,17,11,24,22,19,8,13,25,9,12,5,20];

%SVM5
%dFeatureList = [1,16,4,8,6,20,3,7,2,17,9,11,5,18,24,10,22,21,23,14,15,19,25,12,13];
% SVM1
% dFeatureList = [1,16,4,7,6,20,3,8,2,22,18,9,17,11,5,24,21,13,15,10,23,19,14,25,12];


% %Mara
% % load data set
% d=sacReadArff('c:\projects\ETH_LMC\students\KevinSmith\sac\data\mara01.arff');
% dFeatureList = [8,2,24,18,1,39,14,36,28,7,12,5,11,6,19,13,21,16,23,4,17,32,3,29,26,37,33,15,45,9,40,48,44,31,47,20,10,43,30,38,46,25,22,34,27,41,35,42];

%Segment
% % load data set
% d=sacReadArff('c:\projects\ETH_LMC\students\KevinSmith\sac\data\segment-test.arff');
% dFeatureList = [14,17,18,19,11,2,16,12,6,10,13,8,1,15,7,4,9,5,3];
% %SVM5
% dFeatureList = [16,17,18,11,2,19,14,12,13,10,6,8,1,15,4,7,9,5,3];

% Roger darmachom
d=sacReadArff('c:\projects\ETH_LMC\software\ACC_Roger_Dharmacon\DGW3MD3.arff');
dFeatureList = [1,5,3,6,2,4,8,27,12,26,7,24,25,15,17,18,14,23,11,22,13,21,19,10,9,16,20];


figure;

lengthFL = length(dFeatureList);

for i=1:lengthFL-1
    
    i
    
    pFeatureList = sort(dFeatureList(lengthFL-i+1:lengthFL), 'descend');
    dNew = d;
    
    for j=1:length(pFeatureList)
        dNew.featureNames(pFeatureList(j)) = [];
        dNew.featureTypes(pFeatureList(j)) = [];
        dNew.instances(:,pFeatureList(j)) = [];
    end;
    
    [y,p] = sacCrossValidate('RandomForest_Weka', dNew, 10, 1);  %#ok<*ASGLU>
    [E s] = sacEvaluate(dNew.labels, p, '-ACC -waAU -q');
    bestACC(i) = E.ACC;
    bestAU(i) = E.waAU;
    
    %     [bestOptionString, bestOptionStruct, bestE] = sacSuggest('SVM_LibSVM', dNew, 60, 'random');
    %     bestACC(i) = bestE.ACC;
    %     bestAU(i) = bestE.waAU;
    
    plot(bestACC); hold on;
    plot(bestAU); hold on;
    drawnow;
    
    
end;

