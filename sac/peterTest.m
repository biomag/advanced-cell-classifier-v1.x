clear all;
sacInit;

%d=sacReadArff('testScreen.arff');
d=sacReadArff('c:\projects\ETH_LMC\software\ACC_Thomas_Qia_kinome\rs1md3detailed.arff');

XMLList = dir('classifiers/xml/*.xml');

for i=4:12%1:length(XMLList)
   for j=1:3
       tic;
       disp(XMLList(i).name(1:end-4));
       [bestOptionString{i, j}, bestOptionStruct{i, j}, bestE{i, j}] = sacSuggest(XMLList(i).name(1:end-4), d, 60, 'random');
       ACCMtx(i, j) = bestE{i, j}.ACC;
       AUCMtx(i, j) = bestE{i, j}.waAU;
       timers(i, j) = toc;
   end;
end;





figure; bar(mean(ACCMtx,2));
hold on;
for i=1:length(XMLList)
    errorbar(i, mean(ACCMtx(i,:)), std(ACCMtx(i,:)));
end
axis([0 18 .93 1]);
for i = 1:length(XMLList)
    str = XMLList(i).name(1:end-4);
    h = text(i, .985, str);
    set(h, 'Rotation', 90);
end
title('Classification Accuracy');


figure; bar(mean(timers,2));
hold on;
for i=1:length(XMLList)
    errorbar(i, mean(timers(i,:)), std(timers(i,:)));
end
axis([0 18 0 200]);
for i = 1:length(XMLList)
    str = XMLList(i).name(1:end-4);
    h = text(i, 140, str);
    set(h, 'Rotation', 90);
end
title('Time (s)')

    