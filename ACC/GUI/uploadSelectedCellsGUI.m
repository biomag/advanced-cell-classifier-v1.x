function varargout = uploadSelectedCellsGUI(varargin)
% UPLOADSELECTEDCELLSGUI MATLAB code for uploadSelectedCellsGUI.fig
%      UPLOADSELECTEDCELLSGUI, by itself, creates a new UPLOADSELECTEDCELLSGUI or raises the existing
%      singleton*.
%
%      H = UPLOADSELECTEDCELLSGUI returns the handle to a new UPLOADSELECTEDCELLSGUI or the handle to
%      the existing singleton*.
%
%      UPLOADSELECTEDCELLSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UPLOADSELECTEDCELLSGUI.M with the given input arguments.
%
%      UPLOADSELECTEDCELLSGUI('Property','Value',...) creates a new UPLOADSELECTEDCELLSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before uploadSelectedCellsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to uploadSelectedCellsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help uploadSelectedCellsGUI

% Last Modified by GUIDE v2.5 10-Feb-2016 22:32:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @uploadSelectedCellsGUI_OpeningFcn, ...
    'gui_OutputFcn',  @uploadSelectedCellsGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before uploadSelectedCellsGUI is made visible.
function uploadSelectedCellsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to uploadSelectedCellsGUI (see VARARGIN)

% Choose default command line output for uploadSelectedCellsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes uploadSelectedCellsGUI wait for user response (see UIRESUME)
% uiwait(handles.uploadFigure);

global CommonHandles;

if ~isempty( get(handles.classesListbox, 'String') )
    classID = get(handles.classesListbox, 'Value');
    imshow(CommonHandles.Classes{classID}.Icon, 'Parent', handles.classImageAxes);
end


% --- Outputs from this function are returned to the command line.
function varargout = uploadSelectedCellsGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in uploadButton.
function uploadButton_Callback(hObject, eventdata, handles)
% hObject    handle to uploadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.uploadStatusAxes, 'XLim', [0 1], 'YLim', [0 1]);

if ~isfield(handles, 'hp')
    hp = patch('XData', [0 0 0 0], ...
        'YData', [0 0 1 1], ...
        'Parent', handles.uploadStatusAxes,...
        'FaceColor', [0.2 0.6 0.2]);
else
    hp = handles.hp;
    
end

for i=1:100
    fractionDone = double(i)/100.0;
    pause(0.05);
    set(hp, 'XData', [0 fractionDone fractionDone 0]);
    set(handles.uploadButton,'String',sprintf('Uploading cells: %3d %%',i), 'Enable', 'off');
end

set(handles.uploadButton, 'String', 'Upload selected cells', 'Enable', 'on');

set(hp, 'XData', [0 0 0 0]);

handles.hp = hp;

guidata(hObject, handles)



% --- Executes on selection change in classesListbox.
function classesListbox_Callback(hObject, eventdata, handles)
% hObject    handle to classesListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns classesListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from classesListbox

global CommonHandles;

classID = get(hObject, 'Value');

imshow(CommonHandles.Classes{classID}.Icon,'Parent',handles.classImageAxes);


% --- Executes during object creation, after setting all properties.
function classesListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classesListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global CommonHandles;
if ~isempty(CommonHandles.Classes)
    ClassNames = cell(length(CommonHandles.Classes),1);
    for i=1:length(CommonHandles.Classes)
        ClassNames{i} = CommonHandles.Classes{i}.Name;
    end
else
    ClassNames = [];
end

set(hObject, 'String', ClassNames);

guidata(hObject, handles);



% --- Executes when user attempts to close uploadFigure.
function uploadFigure_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to uploadFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%TODO check if there is anything open (connection, progress etc.)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes during object creation, after setting all properties.
function uploadFigure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uploadFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% load('../temp/CH_test_20160205.mat');
global CommonHandles;


% --------------------------------------------------------------------
function settingsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to settingsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function nwSettingsMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to nwSettingsMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = networkSettingsGUI();
uiwait(h);



function cellNumberInput_Callback(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cellNumberInput as text
%        str2double(get(hObject,'String')) returns contents of cellNumberInput as a double


% --- Executes during object creation, after setting all properties.
function cellNumberInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in showCellsButton.
function showCellsButton_Callback(hObject, eventdata, handles)
% hObject    handle to showCellsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% global CommonHandles;

% TODO get classID

classID = get(handles.classesListbox, 'Value');

numOfCellsToShow = str2num(get(handles.cellNumberInput,'String'));

showStruct = struct('classID', classID, 'numOfCellsToShow', numOfCellsToShow);

showHighProbCells_GUI('UserData',showStruct);


% --- Executes during object creation, after setting all properties.
function showCellsButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showCellsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function classImageAxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classImageAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate classImageAxes



function numOfSelectedInput_Callback(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cellNumberInput as text
%        str2double(get(hObject,'String')) returns contents of cellNumberInput as a double


% --- Executes during object creation, after setting all properties.
function numOfSelectedInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
