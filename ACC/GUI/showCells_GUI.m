function varargout = showCells_GUI(varargin)
% SHOWCELLS_GUI MATLAB code for showCells_GUI.fig
%      SHOWCELLS_GUI, by itself, creates a new SHOWCELLS_GUI or raises the existing
%      singleton*.
%
%      H = SHOWCELLS_GUI returns the handle to a new SHOWCELLS_GUI or the handle to
%      the existing singleton*.
%
%      SHOWCELLS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWCELLS_GUI.M with the given input arguments.
%
%      SHOWCELLS_GUI('Property','Value',...) creates a new SHOWCELLS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showCells_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showCells_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showCells_GUI

% Last Modified by GUIDE v2.5 27-Jan-2016 09:49:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showCells_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @showCells_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showCells_GUI is made visible.
function showCells_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showCells_GUI (see VARARGIN)

% Choose default command line output for showCells_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showCells_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

leaf = get(handles.figure1, 'UserData');
global CommonHandles;
[imgs,~] = createCellsImage(CommonHandles.HC.Node.Images,leaf);
hSP = imscrollpanel(hObject,imshow(imgs));
set(hSP,'Units','pixels','Position',[10 70 720 500],'Tag','scrollPanel');
api = iptgetapi(hSP);
api.setVisibleLocation([0 0]);
api.setImageButtonDownFcn({@scrollPanel_ButtonDownFcn,hObject});

% --- Outputs from this function are returned to the command line.
function varargout = showCells_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function scrollPanel_ButtonDownFcn(hObject,eventdata,fHandle)

global CommonHandles;

% dummy code needed for old matlab
curpoint = get(gca,'CurrentPoint');
x = curpoint(1,1,1);
y = curpoint(1,2,1);

leaf = get(fHandle, 'UserData');
[~,mapping] = createCellsImage(CommonHandles.HC.Node.Images,leaf);
[selimgInfo,hit,xcoord,ycoord] = selectCellFromClassImage(x,y,mapping);

if hit
    % Get axes object
    hSp = get(fHandle,'Children');
    axes = findobj(hSp,'Type','Axes');
    
    % Remove previous rectangle if exists
    for i=1:length(CommonHandles.HC.Node.Images)
        obj = CommonHandles.HC.Node.Images{i};        
        if obj.selected == 1
            delete(CommonHandles.HC.Node.Images{i}.sHandle);
        end

        % Draw borders around the image
        lw = 3;
        imgxsize = CommonHandles.ClassImageInfo.ImageSize(1);
        imgysize = CommonHandles.ClassImageInfo.ImageSize(2);
        sepsize = CommonHandles.ClassImageInfo.SepSize;
        drawX = (xcoord-1) * imgxsize + (xcoord) * sepsize - lw;
        drawY = (ycoord-1) * imgysize + (ycoord) * sepsize - lw;
        hRec = rectangle('Position',[drawX, drawY, imgxsize + 2*lw - 1, imgysize + 2*lw - 1],'EdgeColor', 'red', 'LineWidth',lw, 'Parent', axes);
        obj.sHandle = hRec;
        obj.info = selimgInfo;
        obj.selected = 1;
        CommonHandles.HC.Node.Images{i} = obj;
    end
end

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
CommonHandles.HC.Node.Images = {};

uiresume(gcbf); 
close;


% --- Executes on button press in jumpToCell.
function jumpToCell_Callback(hObject, eventdata, handles)
% hObject    handle to jumpToCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

info = CommonHandles.HC.Node.Images{1}.info;

CommonHandles.SelectedImageName = info.ImageName;
CommonHandles.SelectedOriginalImageName = info.ImageName;
CommonHandles.SelectedPlate = info.PlateName;
CommonHandles = LoadSelectedImage(CommonHandles);
[~, fileNameexEx, ~, ~] = filepartsCustom(CommonHandles.SelectedImageName);
[~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx);
CommonHandles.SelectedCell = info.CellNumber;
CommonHandles = ShowSelectedCell(CommonHandles);
