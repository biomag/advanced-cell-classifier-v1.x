function varargout = createClasses_GUI(varargin)
% CREATECLASSES_GUI MATLAB code for createClasses_GUI.fig
%      CREATECLASSES_GUI, by itself, creates a new CREATECLASSES_GUI or raises the existing
%      singleton*.
%
%      H = CREATECLASSES_GUI returns the handle to a new CREATECLASSES_GUI or the handle to
%      the existing singleton*.
%
%      CREATECLASSES_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATECLASSES_GUI.M with the given input arguments.
%
%      CREATECLASSES_GUI('Property','Value',...) creates a new CREATECLASSES_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before createClasses_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to createClasses_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help createClasses_GUI

% Last Modified by GUIDE v2.5 19-Jan-2016 15:51:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @createClasses_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @createClasses_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before createClasses_GUI is made visible.
function createClasses_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to createClasses_GUI (see VARARGIN)

% Choose default command line output for createClasses_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes createClasses_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global CommonHandles;

% read in and list the class types
for i=1:numel(CommonHandles.ClassTypes)
    classTypeName{i} = CommonHandles.ClassTypes{i};
end
set(handles.classTypesPopup, 'String', classTypeName);

% parent selecting is visible only if the selected type is 'Child'
set(handles.selectParent, 'Visible', 'off');

for i=1:size(CommonHandles.Classes,2)
    classNames{i+1} = CommonHandles.Classes{i}.Name;
end

classNames{1} = 'Create new class';

if CommonHandles.HC.Node.createClass == 1
    set(handles.selectIconPushButton ,'Visible','off');
    set(handles.selectCell ,'Visible','off');
    set(handles.editSelectedIcon ,'Visible','off');
    set(handles.classTypesPopup,'Visible','off');
    
    set(handles.addToClass, 'String', classNames);
    set(handles.addToClass,'Visible','on');
    set(handles.addToClassText,'Visible','on');
    CommonHandles.HC.Class.AddingSamplesToExistingClass = 1;
    
    CommonHandles.HC.Class.FeatureMatrix = {};
    
else
    set(handles.addToClass,'Visible','off');
    set(handles.addToClassText,'Visible','off');
    CommonHandles.HC.Class.AddingSamplesToExistingClass = 0;
end




% --- Outputs from this function are returned to the command line.
function varargout = createClasses_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in createClassButton.
function createClassButton_Callback(hObject, eventdata, handles)
% hObject    handle to createClassButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% load classifiers/clusterers to the popupmenu
global CommonHandles;

% get the data for a class
typeList = get(handles.classTypesPopup,'String');
type = char(typeList(get(handles.classTypesPopup,'Value')));

% where to put the sample from HC
classNameList = get(handles.addToClass,'String');
className = char(classNameList(get(handles.addToClass,'Value')));
addToClassVisible = get(handles.addToClass,'Visible');

% choose the icon of the class
if (length(get(handles.editSelectedIcon, 'String')) < 1 && CommonHandles.HC.Node.createClass == 0)
    img = CommonHandles.CurrentSelectedCell;
elseif (length(get(handles.editSelectedIcon, 'String')) < 1 && CommonHandles.HC.Node.createClass == 1)
    data = get(handles.figure1, 'UserData');
    img = data{2};
    type = 'Normal';
else
    img = imread(get(handles.editSelectedIcon, 'String'));    
end

% get the data for a class
parentList = get(handles.selectParent, 'String');
parent = char(parentList(get(handles.selectParent, 'Value')));
name = get(handles.editClassName,'String');
labelNumbers = 0;

% set the Counter as the highest ID, so every ID will be unique
for i=1:length(CommonHandles.Classes)
   temp = CommonHandles.Classes{i}.ID;
   if (temp > CommonHandles.Counter)
       CommonHandles.Counter = temp;
   end
end

% keeping the id (== counter) unique
CommonHandles.Counter = CommonHandles.Counter + 1;
id = CommonHandles.Counter;

%{
    * the child type classes should be added next to their parents
    * if the first child is added to a parent than parents label numbers
    and trained cells can be added for this child, otherwise nothing
    happens
    * insert funciton inserts the struct to the right place into Classes
    * zeros column needed for keeping the consistency
    * in case of Normal or Regression type, the data is added to the end of
    the rows
%}
if(strcmp(type,'Child'))
    img = imresize(img, [40 NaN]);
    for i=1:size(CommonHandles.Classes,2)
        if((strcmp(CommonHandles.Classes{i}.Name, parent)) && strcmp(CommonHandles.Classes{i}.Type,'Normal'))                
            if ((i<size(CommonHandles.Classes,2)) && (~(strcmp(CommonHandles.Classes{i+1}.Type,'Child'))))
                button = questdlg('Add parents labels to child or delete parent?','Creating child','Add labels to child','I will delete Parent','I will delete Parent');
                if strcmp(button,'Add labels to child')
                    st = struct('Type',type,'Icon',img,'Name',name,'ID',id,'LabelNumbers',CommonHandles.Classes{i}.LabelNumbers);
                    CommonHandles.Classes = insert(CommonHandles.Classes, st ,i+1);
                    CommonHandles.TrainingSet.Class = insertZerosToClassVector(CommonHandles.TrainingSet.Class, i);

                    CommonHandles.Classes{i}.LabelNumbers = labelNumbers;
                elseif strcmp(button,'I will delete Parent')
                    disp('Delete Parent')
                end
            else
                st = struct('Type',type,'Icon',img,'Name',name,'ID',id,'LabelNumbers',CommonHandles.Classes{i}.LabelNumbers);
                CommonHandles.Classes = insert(CommonHandles.Classes, st ,i+1);
                CommonHandles.TrainingSet.Class = insertZerosToClassVector(CommonHandles.TrainingSet.Class, i);

                CommonHandles.Classes{i}.LabelNumbers = labelNumbers;
            end
        end
    end   
    
elseif strcmp(addToClassVisible,'off') || strcmp(className,'Create new class')
    img = imresize(img, [60 NaN]);
    CommonHandles.Classes{end+1}.Type = type;
    CommonHandles.Classes{end}.Icon = img;
    CommonHandles.Classes{end}.ID = id;
    CommonHandles.Classes{end}.Name = name;
    CommonHandles.Classes{end}.LabelNumbers = labelNumbers;
    
    for j=1:length(CommonHandles.TrainingSet.Class)
       CommonHandles.TrainingSet.Class{j} = [CommonHandles.TrainingSet.Class{j}; zeros(1)];
    end
end

if CommonHandles.HC.Node.createClass == 1
    
    data = get(handles.figure1, 'UserData');
    CommonHandles.HC.Class.FeatureMatrix = {};
    getRequiredFeatures(data{1});        
    CommonHandles.MaxCluster = length(CommonHandles.Classes);
    
    for t=1:size(CommonHandles.HC.Class.FeatureMatrix,2)
        featureMatrix(t,:) = CommonHandles.HC.Class.FeatureMatrix{t};
    end
        
    if strcmp(className,'Create new class')
        addHCSamplesToTrainingSet(featureMatrix);
    else
        addHCSamplesToExistingClass(className,featureMatrix);
    end
end

regressionClassChecker();
refreshClassesList();

CommonHandles.HC.Node.createClass = 0;
    
uiresume(gcbf); 
close;

% --- Executes on button press in createClassCancel.
function createClassCancel_Callback(hObject, eventdata, handles)
% hObject    handle to createClassCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(gcbf); 
close;


% --- Executes on selection change in classTypesPopup.
function classTypesPopup_Callback(hObject, eventdata, handles)
% hObject    handle to classTypesPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns classTypesPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from classTypesPopup
global CommonHandles;

%if the selected type is 'Child', show the possible parents
typeList = get(hObject,'String');
classType = char(typeList(get(hObject,'Value')));
if (strcmp(classType ,'Child'))
    classNames = {};    
    for i=1:numel(CommonHandles.Classes)
        if(strcmp(CommonHandles.Classes{i}.Type,'Normal'))
            classNames{end+1} = CommonHandles.Classes{i}.Name;
        end
    end
    if(0 < size(classNames))            
        set(handles.selectParent, 'String', classNames);
        set(handles.selectParent, 'Visible', 'on');
    end
else 
    set(handles.selectParent, 'Visible', 'off');
end

% --- Executes during object creation, after setting all properties.
function classTypesPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classTypesPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectIconPushButton.
function selectIconPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to selectIconPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

iconName = imgetfile();
set(handles.editSelectedIcon, 'String', iconName);


% --- Executes on selection change in selectParent.
function selectParent_Callback(hObject, eventdata, handles)
% hObject    handle to selectParent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectParent contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectParent


% --- Executes during object creation, after setting all properties.
function selectParent_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectParent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editSelectedIcon_Callback(hObject, eventdata, handles)
% hObject    handle to editSelectedIcon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSelectedIcon as text
%        str2double(get(hObject,'String')) returns contents of editSelectedIcon as a double


% --- Executes during object creation, after setting all properties.
function editSelectedIcon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSelectedIcon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editClassName_Callback(hObject, eventdata, handles)
% hObject    handle to editClassName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editClassName as text
%        str2double(get(hObject,'String')) returns contents of editClassName as a double


% --- Executes during object creation, after setting all properties.
function editClassName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editClassName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% inserting struct into the Classes
function c = insert(c,st,idx)
    c = [c(1:idx-1) {st} c(idx:end)];
    
% inserting Zeros column into TraininSet.Class
function c = insertZerosToClassVector(c, idx)
    func = @(x,idx) ([x{:}(1:idx-1); zeros(1); x{:}(idx:end)]);
    c = arrayfun(@(x)func(x,idx), c, 'UniformOutput', false);


% --- Executes on button press in selectCell.
function selectCell_Callback(hObject, eventdata, handles)
% hObject    handle to selectCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% with this, we keep editSelecteIcon's lenght to zero,
% so we can use the selected cell as the Icon
set(handles.editSelectedIcon, 'Visible', 'off');

function getRequiredFeatures(counter)

    global CommonHandles;
    
    if ~isempty(CommonHandles.HC.Node.Childs{counter})
        child_1 = CommonHandles.HC.Node.Childs{counter}(1);
        getRequiredFeatures(child_1);
        child_2 = CommonHandles.HC.Node.Childs{counter}(2);
        getRequiredFeatures(child_2);
    else
        CommonHandles.HC.Class.FeatureMatrix{end+1} = CommonHandles.HC.Node.LeafFeatures{counter};
    end


% --- Executes on selection change in addToClass.
function addToClass_Callback(hObject, eventdata, handles)
% hObject    handle to addToClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns addToClass contents as cell array
%        contents{get(hObject,'Value')} returns selected item from addToClass

classNameList = get(handles.addToClass,'String');
className = char(classNameList(get(handles.addToClass,'Value')));

if ~strcmp(className,'Create new class')
    set(handles.editClassName,'Visible','off');
else
    set(handles.editClassName,'Visible','on');
end


% --- Executes during object creation, after setting all properties.
function addToClass_CreateFcn(hObject, eventdata, handles)
% hObject    handle to addToClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
