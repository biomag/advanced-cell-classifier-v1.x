function varargout = AL_GUI(varargin)
% AL_GUI MATLAB code for AL_GUI.fig
%      AL_GUI, by itself, creates a new AL_GUI or raises the existing
%      singleton*.
%
%      H = AL_GUI returns the handle to a new AL_GUI or the handle to
%      the existing singleton*.
%
%      AL_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AL_GUI.M with the given input arguments.
%
%      AL_GUI('Property','Value',...) creates a new AL_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AL_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AL_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AL_GUI

% Last Modified by GUIDE v2.5 18-Sep-2015 12:13:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AL_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @AL_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AL_GUI is made visible.
function AL_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AL_GUI (see VARARGIN)

% Choose default command line output for AL_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AL_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);    



% --- Outputs from this function are returned to the command line.
function varargout = AL_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%UTIL functions in this GUI
%Calculate all the cells in the project meaning all the plates.
function cellNumber = countAllTheCells()    
    global CommonHandles;
    [f,~,~] = LoadAllFeatures(CommonHandles,0.2,1,CommonHandles.SelectedPlate);
    cellNumber = size(f,1).*5;    
    CommonHandles.featureDimension = size(f,2)+2;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(~, ~, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

global CommonHandles;

%Put on the objects to the GUI.

%get the dimensions of the GUI and position all the next GUI elements
%relative to that. (this can be helpful if the size of the GUI will change)
set(gcf,'Units','pixels');
ALGUIBoundaries = get(gcf,'Position');

%Make the tabs
tgroup = uitabgroup('Parent', gcf);
generalTab = uitab('Parent', tgroup, 'Title', 'General');
classificationTab = uitab('Parent', tgroup, 'Title', 'Classification');
regressionTab = uitab('Parent', tgroup, 'Title', 'Regression');

%Elements of general tab
uicontrol('Parent',generalTab,'Style','text','String','approximated # of cells in this experiment','Units','pixels','Position',[10,ALGUIBoundaries(4)-80,200,40]);
handles.nofCells = uicontrol('Parent',generalTab,'Style','text','Units','pixels','Position',[220,ALGUIBoundaries(4)-60,100,20]);

if ~isfield(CommonHandles,'totalCellNumber')
    CommonHandles.totalCellNumber = countAllTheCells();        
end
set(handles.nofCells,'String',num2str(CommonHandles.totalCellNumber));    

uicontrol('Parent',generalTab,'Style','text','String','desired # of cells in the sampling pool','Units','pixels','Position',[10,ALGUIBoundaries(4)-100,200,20]);
handles.sizeOfPoolEdit = uicontrol('Parent',generalTab,'Style','edit','Units','pixels','Position',[220,ALGUIBoundaries(4)-100,100,20],'BackgroundColor','w');
    
if isfield(CommonHandles,'SamplingPool') 
    handles.infoLabel = uicontrol('Parent',generalTab,'Style','text','String',['Sampling pool generated with ' num2str(length(CommonHandles.SamplingPool.Mapping)) ' cell.'],'Units','pixels','Position',[40,ALGUIBoundaries(4)-200,200,20]);
else 
    handles.infoLabel = uicontrol('Parent',generalTab,'Style','text','String','No sampling pool generated.','Units','pixels','Position',[40,ALGUIBoundaries(4)-200,200,20]);
end

%elements for classification tab
uicontrol('Parent',classificationTab,'Style','text','String','Selected classifier','Units','pixels','Position',[10,ALGUIBoundaries(4)-60,200,20]);
handles.selectedClassifier = uicontrol('Parent',classificationTab,'Style','popupmenu','Units','pixels','Position',[200,ALGUIBoundaries(4)-60,200,20],'BackgroundColor','w');
set(handles.selectedClassifier,'String',CommonHandles.ClassifierNames);

%elements for Regression tab
%the order is not top to bottom, but a bit different because we need to
%have the uihandles until we call to the functions.
uicontrol('Parent',regressionTab,'Style','text','String','Selected regressor','Units','pixels','Position',[10,ALGUIBoundaries(4)-60,200,20]);
handles.selectedRegressor = uicontrol('Parent',regressionTab,'Style','popupmenu','Units','pixels','Position',[200,ALGUIBoundaries(4)-60,200,20],'BackgroundColor','w');
set(handles.selectedRegressor,'String',CommonHandles.RegressorNames);

global Ghandles;
Ghandles.ARParamNames = [];
Ghandles.ARParamEdits = [];
handles.activeRegressorParameters = uipanel('Parent',regressionTab,'Title','Active regressor parameters','Units','pixels','Position',[10,ALGUIBoundaries(4)-260,400,100]);

uicontrol('Parent',regressionTab,'Style','text','String','Selected active learner','Units','pixels','Position',[10,ALGUIBoundaries(4)-100,200,20]);
handles.selectedActiveRegressor = uicontrol('Parent',regressionTab,'Style','popupmenu','Units','pixels','Position',[200,ALGUIBoundaries(4)-100,200,20],'BackgroundColor','w');
set(handles.selectedActiveRegressor,'Callback',{@changeActiveRegressorCallback,handles});
set(handles.selectedActiveRegressor,'String',CommonHandles.ActiveRegressorNames);

uicontrol('Parent',regressionTab,'Style','text','String','Initial training set size','Units','pixels','Position',[10,ALGUIBoundaries(4)-140,200,20]);
handles.initialTrainingSetSize = uicontrol('Parent',regressionTab,'Style','edit','String','5','Units','pixels','Position',[200,ALGUIBoundaries(4)-140,200,20],'BackgroundColor','w');



%button elements for general tab which needs the other tab's handles.
uicontrol('Parent',generalTab,'Style','pushbutton','String','Generate sampling pool','Units','pixels','Position',[40,ALGUIBoundaries(4)-160,200,40],'Callback',{@generatePool_Callback,handles});

uicontrol('Parent',generalTab,'Style','pushbutton','String','Start active learning','Units','pixels','Position',[40,ALGUIBoundaries(4)-280 200,40],'Callback',{@startAL_Callback,handles});


%The generatePool_callback function will make a sampling pool. This is a
%subset of the full set. The generated pool will be stored in the
%SamplingPool field of CommonHandles.
function generatePool_Callback(hObject, eventdata, handles)
global CommonHandles;

%get the desired the pool size from the gui
[sizeOfLabelingPool, status] = str2num(get(handles.sizeOfPoolEdit,'String'));
%check whether it's a number
if status
    %check whether it's smaller than the estimated size
    if (CommonHandles.totalCellNumber) >= sizeOfLabelingPool
        %check whether it's positive
        if (sizeOfLabelingPool > 0)                       
            samplingRatio = sizeOfLabelingPool / CommonHandles.totalCellNumber;
            
            [CommonHandles.SamplingPool.Features, CommonHandles.SamplingPool.Mapping, ~] = LoadAllFeatures(CommonHandles,samplingRatio,1,CommonHandles.SelectedPlate);
            set(handles.infoLabel,'String',['Sampling pool generated with ',num2str(size(CommonHandles.SamplingPool.Features,1)),' cell.']);
            CommonHandles = synchronizeTrainingAndSamplingSet(CommonHandles);
            
        else
            errordlg('Please give a positive number!');    
        end
    else
        errordlg('Be careful: the # of total cells are only estimated, you might trying to make a bigger sampling pool than the full set.');
    end
else       
    errordlg('Please give a number as the size of the sampling pool!');
end

%this function synchronizes the sampling pool and the training set. As
%we'll use the sampling pool for training and predicting we need to have
%all the training examples in the sampling pool.
function CommonHandles = synchronizeTrainingAndSamplingSet(CommonHandles)
    %first we generate a new field in the SamplingPool which will store the
    %classes for the training examples.
    CommonHandles.SamplingPool.ClassLabels = zeros(1,length(CommonHandles.SamplingPool.Mapping));
    for i=1:length(CommonHandles.TrainingSet.Features)
        idx = getIndexInSamplingPool(CommonHandles,CommonHandles.TrainingSet.ImageName{i},CommonHandles.TrainingSet.CellNumber{i});
        if (idx == -1)
            CommonHandles.SamplingPool.Features(end+1,:) = CommonHandles.TrainingSet.Features{i};
            CommonHandles.SamplingPool.Mapping{end+1}.ImageName = CommonHandles.TrainingSet.ImageName{i};
            CommonHandles.SamplingPool.Mapping{end}.CellNumberInImage = CommonHandles.TrainingSet.CellNumber{i};
            [~,classLabel] = max(CommonHandles.TrainingSet.Class{i});
            CommonHandles.SamplingPool.ClassLabels(end+1) = classLabel;
        else
            [~,classLabel] = max(CommonHandles.TrainingSet.Class{i});
            CommonHandles.SamplingPool.ClassLabels(idx) = classLabel;
        end
    end    

%callback function to start AL cyle. This will generate an ALController
%store it into CommonHandles. 
function startAL_Callback(hObject, ~, handles)

global CommonHandles;
regressorName = CommonHandles.RegressorNames(get(handles.selectedRegressor,'Value'));
activeLearnerName = CommonHandles.ActiveRegressorNames{get(handles.selectedActiveRegressor,'Value')};

paramarray = feval([activeLearnerName,'.getParameters']);
global Ghandles;
paramcell = {};
for i=1:length(Ghandles.ARParamEdits)
    if strcmp(paramarray{i}.type,'str')
        paramcell{i} = get(Ghandles.ARParamEdits{i},'string');
    elseif strcmp(paramarray{i}.type,'int')
        paramcell{i} = str2num(get(Ghandles.ARParamEdits{i},'string'));
    elseif strcmp(paramarray{i}.type,'enum')
        paramcell{i} = paramarray{i}.values{(get(Ghandles.ARParamEdits{i},'value'))};
    end
end

CommonHandles.AC = ALController(regressorName,0,activeLearnerName,paramcell);

%train a SALT classifier and store it in the AC field of CommonHandles.
CommonHandles.AC.classifierName = CommonHandles.ClassifierNames{get(handles.selectedClassifier,'Value')};
CommonHandles = trainSALTForAC(CommonHandles);

if (isfield(CommonHandles,'AC') && isfield(CommonHandles,'SamplingPool'))
    set(gcf,'Visible','off');
    uiresume(gcbf);
else
    errordlg('You cannot start Active learning without sampling pool!');
end

%callback function for changing the active regressos. This function
%refreshes the parameters panel.
function changeActiveRegressorCallback(~, ~, handles)

global Ghandles;

global CommonHandles;
%get the name of the selected regressor.
ARName = CommonHandles.ActiveRegressorNames{get(handles.selectedActiveRegressor,'value')};

%call the getParameter function of this class
paramarray = feval([ARName,'.getParameters']);

for i=1:length(Ghandles.ARParamNames)
    delete(Ghandles.ARParamNames{i});
    delete(Ghandles.ARParamEdits{i});
end

Ghandles.ARParamNames = [];
Ghandles.ARParamEdits = [];

for i=1:length(paramarray)
    Ghandles.ARParamNames{i} = uicontrol('Parent',handles.activeRegressorParameters,'Units','pixels','Style','Text','String',paramarray{i}.name,'Position',[10, 60-(i-1)*20, 180 20]);
    switch paramarray{i}.type
        case 'int'
            Ghandles.ARParamEdits{i} = uicontrol('Parent',handles.activeRegressorParameters,'Units','pixels','Style','edit','Position',[200, 60-(i-1)*20, 180 20]);
        case 'str'
            Ghandles.ARParamEdits{i} = uicontrol('Parent',handles.activeRegressorParameters,'Units','pixels','Style','edit','Position',[200, 60-(i-1)*20, 180 20]);
        case 'enum'        
            Ghandles.ARParamEdits{i} = uicontrol('Parent',handles.activeRegressorParameters,'Units','pixels','Style','popupmenu','Position',[200, 60-(i-1)*20, 180 20]);
            set(Ghandles.ARParamEdits{i},'String',paramarray{i}.values);
    end
end
pause(1);





    







