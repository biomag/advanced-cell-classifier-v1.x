function varargout = showTrainedCells_GUI(varargin)
% SHOWTRAINEDCELLS_GUI MATLAB code for showTrainedCells_GUI.fig
%      SHOWTRAINEDCELLS_GUI, by itself, creates a new SHOWTRAINEDCELLS_GUI or raises the existing
%      singleton*.
%
%      H = SHOWTRAINEDCELLS_GUI returns the handle to a new SHOWTRAINEDCELLS_GUI or the handle to
%      the existing singleton*.
%
%      SHOWTRAINEDCELLS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWTRAINEDCELLS_GUI.M with the given input arguments.
%
%      SHOWTRAINEDCELLS_GUI('Property','Value',...) creates a new SHOWTRAINEDCELLS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showTrainedCells_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showTrainedCells_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showTrainedCells_GUI

% Last Modified by GUIDE v2.5 04-Nov-2015 23:02:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showTrainedCells_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @showTrainedCells_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showTrainedCells_GUI is made visible.
function showTrainedCells_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showTrainedCells_GUI (see VARARGIN)

% Choose default command line output for showTrainedCells_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showTrainedCells_GUI wait for user response (see UIRESUME)
% uiwait(handles.showTrainedCellsFig);

counter = get(hObject, 'UserData');
[img,className,~] = createClassImage(counter);

hSP = imscrollpanel(hObject,imshow(img));
set(hSP,'Units','pixels','Position',[10 70 720 500],'Tag','scrollPanel');
api = iptgetapi(hSP);
api.setVisibleLocation([0 0]);
api.setImageButtonDownFcn({@scrollPanel_ButtonDownFcn,hObject});

set(handles.writeClassName, 'String', className);


% --- Outputs from this function are returned to the command line.
function varargout = showTrainedCells_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function scrollPanel_ButtonDownFcn(hObject,eventdata,fHandle)
%
%
%
global CommonHandles;

%x = eventdata.IntersectionPoint(1);
%y = eventdata.IntersectionPoint(2);
% dummy code needed for old matlab
curpoint = get(gca,'CurrentPoint');
x = curpoint(1,1,1);
y = curpoint(1,2,1);
counter = get(fHandle, 'UserData');

[~,~,mapping] = createClassImage(counter);
className = CommonHandles.Classes{counter}.Name;
[selimgInfo,hit,xcoord,ycoord] = selectCellFromClassImage(x,y,mapping);

if hit
    % Get axes object
    hSp = get(fHandle,'Children');
    axes = findobj(hSp,'Type','Axes');
    
    % Remove previous rectangle if exists
    classImgObj = CommonHandles.ClassImages(className);
    if ~isempty(fieldnames(classImgObj.selected))
        delete(CommonHandles.ClassImages(className).sHandle);
    end
    
    % Draw borders around the image
    lw = 3;
    imgxsize = CommonHandles.ClassImageInfo.ImageSize(1);
    imgysize = CommonHandles.ClassImageInfo.ImageSize(2);
    sepsize = CommonHandles.ClassImageInfo.SepSize;
    drawX = (xcoord-1) * imgxsize + (xcoord) * sepsize - lw;
    drawY = (ycoord-1) * imgysize + (ycoord) * sepsize - lw;
    hRec = rectangle('Position',[drawX, drawY, imgxsize + 2*lw - 1, imgysize + 2*lw - 1],'EdgeColor', 'red', 'LineWidth',lw, 'Parent', axes);
    classImgObj.sHandle = hRec;
    classImgObj.selected = selimgInfo;
    CommonHandles.ClassImages(className) = classImgObj;
end

% --- Executes on button press in unclassifierButton.
function unclassifierButton_Callback(hObject, eventdata, handles)
% hObject    handle to unclassifierButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

figHandle = get(hObject,'Parent');
counter = get(figHandle, 'UserData');
className = CommonHandles.Classes{counter}.Name;

if ~isempty(fieldnames(CommonHandles.ClassImages(className).selected))
    deleteFromClassImages(CommonHandles.ClassImages(className).selected.CellNumber, CommonHandles.ClassImages(className).selected.ImageName, counter);
    delete(CommonHandles.ClassImages(className).sHandle);
    classImgObj = CommonHandles.ClassImages(className);
    classImgObj.selected = struct;
    CommonHandles.ClassImages(className) = classImgObj;
else
    %errormsg;
end

sp = findobj(figHandle,'Tag','scrollPanel');
refreshClassImage(counter, sp);


function writeClassName_Callback(hObject, eventdata, handles)
% hObject    handle to writeClassName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of writeClassName as text
%        str2double(get(hObject,'String')) returns contents of writeClassName as a double


% --- Executes during object creation, after setting all properties.
function writeClassName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to writeClassName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in closeButton.
function closeButton_Callback(hObject, eventdata, handles)
% hObject    handle to closeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

figHandle = get(hObject,'Parent');
counter = get(figHandle, 'UserData');
className = CommonHandles.Classes{counter}.Name;
classImgObj = CommonHandles.ClassImages(className);
if ~isempty(fieldnames(classImgObj.selected))
    delete(classImgObj.sHandle);
    classImgObj.selected = struct();
    CommonHandles.ClassImages(className) = classImgObj;
end

uiresume(gcbf); 
close;
