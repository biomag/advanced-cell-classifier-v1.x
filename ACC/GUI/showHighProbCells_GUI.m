function varargout = showHighProbCells_GUI(varargin)
% SHOWHIGHPROBCELLS_GUI MATLAB code for showHighProbCells_GUI.fig
%      SHOWHIGHPROBCELLS_GUI, by itself, creates a new SHOWHIGHPROBCELLS_GUI or raises the existing
%      singleton*.
%
%      H = SHOWHIGHPROBCELLS_GUI returns the handle to a new SHOWHIGHPROBCELLS_GUI or the handle to
%      the existing singleton*.
%
%      SHOWHIGHPROBCELLS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWHIGHPROBCELLS_GUI.M with the given input arguments.
%
%      SHOWHIGHPROBCELLS_GUI('Property','Value',...) creates a new SHOWHIGHPROBCELLS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showHighProbCells_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showHighProbCells_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showHighProbCells_GUI

% Last Modified by GUIDE v2.5 11-Feb-2016 09:02:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showHighProbCells_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @showHighProbCells_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showHighProbCells_GUI is made visible.
function showHighProbCells_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showHighProbCells_GUI (see VARARGIN)

% Choose default command line output for showHighProbCells_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showHighProbCells_GUI wait for user response (see UIRESUME)
% uiwait(handles.showHighProbCells);

global CommonHandles;

everyCell = CommonHandles.AllFeatures;
mapping = CommonHandles.AllFeaturesMapping;
coords = CommonHandles.CellCoordinates;

mapping = mapping(~cellfun('isempty',mapping));

showStruct = get(hObject, 'UserData');

if CommonHandles.SALT.initialized    
    CommonHandles.SALT.trainingData.instances = everyCell;
    CommonHandles.SALT.trainingData.labels = ones(size(everyCell,1),1);
    if strcmp(CommonHandles.ClassifierNames{CommonHandles.SelectedClassifier},'SVM_Libsvm')
        [out, ~ ,props] = svmpredict(ones(size(features,1),1), CommonHandles.SALT.trainingData.instances, CommonHandles.SALT.model);
    else
        [out, props] = sacPredict(CommonHandles.SALT.model, CommonHandles.SALT.trainingData);
    end
else
    [out, props] = Predict(CommonHandles.SelectedClassifier , everyCell', CommonHandles);
end


[~,index] = sort(props(:,showStruct.classID));

sortedOut = out(index);

sortedClassIndices = sortedOut==showStruct.classID;

classIndices = index(sortedClassIndices);

classIndices = flip(classIndices);

% sortedIndex = sort(classIndices(1:showStruct.numOfCellsToShow));

sortedIndex = classIndices(1:showStruct.numOfCellsToShow);

uploadStructure.img = {};
uploadStructure.featureVector = [];
uploadStructure.imageName = {};
uploadStructure.selectedCell = {};

for i=1:showStruct.numOfCellsToShow
    
    currentCell = sortedIndex(i);
    cellImage = getCurrentCellImage(mapping{currentCell}.ImageName, coords(currentCell,:));
    
%     figure(10); imshow(cellImage); title([num2str(props(currentCell,showStruct.classID),'p=%f, ')]);
    num2str(props(currentCell,showStruct.classID),'p=%f, ')

    uploadStructure.img{i} = cellImage;
    uploadStructure.featureVector{i} = everyCell(currentCell,:);
    uploadStructure.imageName{i} = mapping{currentCell}.ImageName;
    uploadStructure.selectedCell{i} = mapping{currentCell}.CellNumberInImage;
    
end

% set(handles.showHighProbCells,'Position', [0 0 800 1000]);
% set(handles.showHighProbCells,'OuterPosition', [0 0 800 650]);
set(handles.showHighProbCells,'Name', sprintf('Cells with the highest probability of belonging to class %s', CommonHandles.Classes{showStruct.classID}.Name));

% parent = get(hObject,'parent');
imgs = imagesForLowProb(uploadStructure.img,showStruct.numOfCellsToShow);
% hSP = imscrollpanel(parent,imshow(imgs));
hSP = imscrollpanel(handles.showHighProbCells,imshow(imgs));

set(hSP,'Units','pixels','Position',[10 60 720 620],'Tag','scrollPanel');

set(handles.showHighProbCells,'Position', [0 0 750 700]);

api = iptgetapi(hSP);
api.setVisibleLocation([0 0]);

% --- Outputs from this function are returned to the command line.
function varargout = showHighProbCells_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function showHighProbCells_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showHighProbCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in selectAllButton.
function selectAllButton_Callback(hObject, eventdata, handles)
% hObject    handle to selectAllButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in deselectAllButton.
function deselectAllButton_Callback(hObject, eventdata, handles)
% hObject    handle to deselectAllButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
