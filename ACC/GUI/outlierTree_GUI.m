function varargout = outlierTree_GUI(varargin)
% OUTLIERTREE_GUI MATLAB code for outlierTree_GUI.fig
%      OUTLIERTREE_GUI, by itself, creates a new OUTLIERTREE_GUI or raises the existing
%      singleton*.
%
%      H = OUTLIERTREE_GUI returns the handle to a new OUTLIERTREE_GUI or the handle to
%      the existing singleton*.
%
%      OUTLIERTREE_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OUTLIERTREE_GUI.M with the given input arguments.
%
%      OUTLIERTREE_GUI('Property','Value',...) creates a new OUTLIERTREE_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before outlierTree_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to outlierTree_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help outlierTree_GUI

% Last Modified by GUIDE v2.5 03-Feb-2016 15:42:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @outlierTree_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @outlierTree_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before outlierTree_GUI is made visible.
function outlierTree_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to outlierTree_GUI (see VARARGIN)

% Choose default command line output for outlierTree_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes outlierTree_GUI wait for user response (see UIRESUME)
% uiwait(handles.outlierTreeWindow);

global CommonHandles;

CommonHandles.HC.mainFig = handles.outlierTreeWindow;
set(CommonHandles.HC.mainFig,'Position',[273 157 1615 821]);
CommonHandles.HC.ax = axes('Parent', handles.outlierTreeWindow, 'Units', 'pixel','Visible','on');
set(CommonHandles.HC.ax,'Position',[0 0 1615 821]);
set(CommonHandles.HC.ax,'XLim',[0 1615]);
set(CommonHandles.HC.ax,'YLim',[0 821]);
CommonHandles.HC.Node = {};
CommonHandles.HC.Node.createClass = 0;
CommonHandles.HC.Node.Images = {};
CommonHandles.HC.avgFeatures = {};
CommonHandles.HC.Node.ActualLevel = {};

for i=1:size(CommonHandles.OCC.LowProb.featureVector,1)
    selectedFeatures(i,:) = CommonHandles.OCC.LowProb.featureVector(i,:);
end

if ~isfield(CommonHandles.HC.Default,'Method')
    CommonHandles.HC.Default.Method = 'average';
end
if ~isfield(CommonHandles.HC.Default,'Metric')
    CommonHandles.HC.Default.Metric = 'euclidean';
end
tree = linkage(selectedFeatures,CommonHandles.HC.Default.Method,CommonHandles.HC.Default.Metric);

CommonHandles.HC.sampleSize = size(selectedFeatures,1);
treeStruct = zeros(CommonHandles.HC.sampleSize-1,3);
for i=1:CommonHandles.HC.sampleSize-1
    treeStruct(i,1) = tree(i,1);
    treeStruct(i,2) = tree(i,2);
    treeStruct(i,3) = i + CommonHandles.HC.sampleSize;
end

%img = imresize(CommonHandles.OCC.LowProb.img{1},[50 NaN]);
root = 2*size(treeStruct,1) + 1;
CommonHandles.HC.Node.Root = root;
img = CommonHandles.OCC.LowProb.img{1};
incW = 101;
CommonHandles.HC.incW=incW;
CommonHandles.HC.Node.Panel{root} = uipanel('Parent',  handles.outlierTreeWindow, 'Units', 'pixel');
CommonHandles.HC.Node.CreateClassButton{root} = uicontrol('Parent', CommonHandles.HC.Node.Panel{root} ,'Style', 'pushbutton', 'String', '','BackgroundColor', 'red', 'Position', [incW-18 2 12 12], 'Callback', {@createClasses_Callback,root,img});
CommonHandles.HC.Node.ShowCellsButton{root} = uicontrol('Parent', CommonHandles.HC.Node.Panel{root} ,'Style', 'pushbutton', 'String', '','BackgroundColor', 'green', 'Position', [incW-32 2 12 12], 'Callback',{@showNodeCells_Callback,root});
CommonHandles.HC.Node.CellText{root} = uicontrol('Parent', CommonHandles.HC.Node.Panel{root} ,'Style', 'text', 'String', 'Cells: ', 'Position', [0 1 35 15]);
CommonHandles.HC.Node.NumberOfCells{root} = CommonHandles.HC.sampleSize;
CommonHandles.HC.Node.CellNumbers{root} = uicontrol('Parent', CommonHandles.HC.Node.Panel{root} ,'Style', 'text', 'String', CommonHandles.HC.Node.NumberOfCells{root},'BackgroundColor', 'white', 'Position', [32 1 21 15]);
CommonHandles.HC.Node.ShowChilds{root} = 0;
CommonHandles.HC.Node.Childs{root}(1) = treeStruct(size(treeStruct,1),1);
CommonHandles.HC.Node.Childs{root}(2) = treeStruct(size(treeStruct,1),2);
CommonHandles.HC.Node.ImageOfCells{root} = img;
CommonHandles.HC.Node.Level{root} = 1;
CommonHandles.HC.Node.ChildLevel{root} = 2;


for i=1:(2*size(treeStruct,1))
    if i > CommonHandles.HC.sampleSize
        childPlace = find(i==treeStruct(:,3));
        parentPlace = find(i==treeStruct(:,2));
        if isempty(parentPlace)
            parentPlace = find(i==treeStruct(:,1));
        end
        CommonHandles.HC.Node.Panel{i} = uipanel('Parent',handles.outlierTreeWindow,'Units','pixel','Visible','off');        
        CommonHandles.HC.Node.Childs{i}(1) = treeStruct(childPlace,1);
        CommonHandles.HC.Node.Childs{i}(2) = treeStruct(childPlace,2);
        CommonHandles.HC.Node.NumberOfCells{i} = getChildCells(i);
        CommonHandles.HC.Node.ImageOfCells{i} = {};
        imgPos = getRepresentativeImage(i);
        CommonHandles.HC.Node.CellButton{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'pushbutton', 'String', '', 'CData', CommonHandles.HC.Node.ImageOfCells{imgPos}, 'Position', [1 16 incW incW],'Callback',{@showChilds_Callback,i});
        CommonHandles.HC.Node.CreateClassButton{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'pushbutton', 'String', '','BackgroundColor', 'red', 'Position', [incW-18 2 12 12], 'Callback', {@createClasses_Callback,i,CommonHandles.HC.Node.ImageOfCells{imgPos}});
        CommonHandles.HC.Node.ShowCellsButton{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'pushbutton', 'String', '','BackgroundColor', 'green', 'Position', [incW-32 2 12 12], 'Callback',{@showNodeCells_Callback,i});
        CommonHandles.HC.Node.CellText{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'text', 'String', 'Cells: ', 'Position', [0 1 35 15]);       
        CommonHandles.HC.Node.CellNumbers{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'text', 'String', CommonHandles.HC.Node.NumberOfCells{i},'BackgroundColor', 'white', 'Position', [32 1 21 15]);
        CommonHandles.HC.Node.ShowChilds{i} = 0;
        CommonHandles.HC.Node.Parent{i} = treeStruct(parentPlace,3);
    else
        img = CommonHandles.OCC.LowProb.img{i};
        CommonHandles.HC.Node.ImageOfCells{i} = img;
        parentPlace = find(i==treeStruct(:,2));
        if isempty(parentPlace)
            parentPlace = find(i==treeStruct(:,1));
        end
        CommonHandles.HC.Node.Parent{i} = treeStruct(parentPlace,3);
        CommonHandles.HC.Node.Panel{i} = uipanel('Parent',handles.outlierTreeWindow,'Units','pixel','Visible','off');
        CommonHandles.HC.Node.CellButton{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'pushbutton', 'String', '', 'CData', img, 'Position', [1 16 incW incW],'Callback',{@showChilds_Callback,i});
        CommonHandles.HC.Node.CreateClassButton{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'pushbutton', 'String', '','BackgroundColor', 'red', 'Position', [incW-18 2 12 12], 'Callback', {@createClasses_Callback,i,img});
        CommonHandles.HC.Node.ShowCellsButton{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'pushbutton', 'String', '','BackgroundColor', 'green', 'Position', [incW-32 2 12 12], 'Callback',{@showNodeCells_Callback,i});
        CommonHandles.HC.Node.CellText{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'text', 'String', 'Cells: ', 'Position', [0 1 35 15]);
        CommonHandles.HC.Node.NumberOfCells{i} = 1;
        CommonHandles.HC.Node.CellNumbers{i} = uicontrol('Parent', CommonHandles.HC.Node.Panel{i} ,'Style', 'text', 'String', CommonHandles.HC.Node.NumberOfCells{i},'BackgroundColor', 'white', 'Position', [32 1 21 15]);
        CommonHandles.HC.Node.LeafFeatures{i} = selectedFeatures(i,:);
    end
end

CommonHandles.HC.ShiftedNodes = zeros(CommonHandles.HC.Node.Root,1);

imgPos = getRepresentativeImage(root);
img = CommonHandles.HC.Node.ImageOfCells{imgPos};
CommonHandles.HC.Node.CellButton{root} = uicontrol('Parent', CommonHandles.HC.Node.Panel{root} ,'Style', 'pushbutton', 'String', '', 'CData', img, 'Position', [1 16 incW incW],'Callback',{@showChilds_Callback,root});

CommonHandles.HC.SliderY = uicontrol('Style','Slider','Parent', handles.outlierTreeWindow,'Position',[1600 5 10 810],'Callback',{@moveNodesOnYLabel_callback});
CommonHandles.HC.SliderX = uicontrol('Style','Slider','Parent', handles.outlierTreeWindow,'Position',[5 5 1590 10],'Callback',{@moveNodesOnXLabel_callback});

getRequiredFeatures(root);
depth = max([CommonHandles.HC.Node.Level{:}]);
CommonHandles.HC.FirstValueForSliderY = (depth + depth/5);
set(CommonHandles.HC.SliderY, 'Min', 1);
set(CommonHandles.HC.SliderY, 'Max', depth + depth/5 + 1);
set(CommonHandles.HC.SliderY, 'Value', CommonHandles.HC.FirstValueForSliderY);
set(CommonHandles.HC.SliderY, 'SliderStep', [1/depth , 10/depth]);

maxNumberOfImages = 2*size(treeStruct,1);
depth = getHeigthOfTree(maxNumberOfImages);
set(CommonHandles.HC.SliderX, 'Min', 1);
CommonHandles.HC.maxWidth = 2^(depth-1);
CommonHandles.HC.FirstValueForSliderX = CommonHandles.HC.maxWidth/2;
set(CommonHandles.HC.SliderX, 'Max', CommonHandles.HC.maxWidth);
set(CommonHandles.HC.SliderX, 'Value', CommonHandles.HC.FirstValueForSliderX);
set(CommonHandles.HC.SliderX, 'SliderStep', [1/CommonHandles.HC.maxWidth , 10/CommonHandles.HC.maxWidth]);

set(CommonHandles.HC.Node.Panel{root}, 'Position', [700 702 incW incW+15]);


% --- Outputs from this function are returned to the command line.
function varargout = outlierTree_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in closeButton.
function closeButton_Callback(hObject, eventdata, handles)
% hObject    handle to closeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(gcbf); 
close;

function createClasses_Callback(hObject, eventdata, counter, img)     
    
    global CommonHandles;
    CommonHandles.HC.Node.createClass = 1;
    data{1} = counter;
    data{2} = img;
    f = createClasses_GUI('UserData',data);
    
function showNodeCells_Callback(hObject, eventdata, counter)      

    leaf = recImg(hObject, eventdata, counter);    
    f = showCells_GUI('UserData',leaf);

function leaf = recImg(hObject, eventdata, counter)
    global CommonHandles;
    
    leaf = 0;
    
    if ~isempty(CommonHandles.HC.Node.Childs{counter})
        child_1 = CommonHandles.HC.Node.Childs{counter}(1);
        child_2 = CommonHandles.HC.Node.Childs{counter}(2);
        leaf = 1;

        if iscell(CommonHandles.HC.Node.ImageOfCells{child_1}) 
            recImg(hObject, eventdata, child_1);
        else
            CommonHandles.HC.Node.Images{end+1}.cellImg = CommonHandles.HC.Node.ImageOfCells{child_1};
            CommonHandles.HC.Node.Images{end}.cell = CommonHandles.OCC.LowProb.selectedCell{child_1};
            CommonHandles.HC.Node.Images{end}.image = CommonHandles.OCC.LowProb.imageName{child_1};
            CommonHandles.HC.Node.Images{end}.plate = CommonHandles.OCC.LowProb.plateName{child_1};
            CommonHandles.HC.Node.Images{end}.selected = 0;
        end

        if iscell(CommonHandles.HC.Node.ImageOfCells{child_2})   
            recImg(hObject, eventdata, child_2);
        else
            CommonHandles.HC.Node.Images{end+1}.cellImg = CommonHandles.HC.Node.ImageOfCells{child_2};
            CommonHandles.HC.Node.Images{end}.cell = CommonHandles.OCC.LowProb.selectedCell{child_2};
            CommonHandles.HC.Node.Images{end}.image = CommonHandles.OCC.LowProb.imageName{child_2};
            CommonHandles.HC.Node.Images{end}.plate = CommonHandles.OCC.LowProb.plateName{child_2};
            CommonHandles.HC.Node.Images{end}.selected = 0;
        end
    else
        CommonHandles.HC.Node.Images{end+1}.cellImg = CommonHandles.HC.Node.ImageOfCells{counter};
        CommonHandles.HC.Node.Images{end}.cell = CommonHandles.OCC.LowProb.selectedCell{counter};
        CommonHandles.HC.Node.Images{end}.image = CommonHandles.OCC.LowProb.imageName{counter};
        CommonHandles.HC.Node.Images{end}.plate = CommonHandles.OCC.LowProb.plateName{counter};
        CommonHandles.HC.Node.Images{end}.selected = 0;
    end
    
function showChilds_Callback(hObject, eventdata, counter)

    global CommonHandles;       
    if ~isempty(CommonHandles.HC.Node.Childs{counter})
        child_1 = CommonHandles.HC.Node.Childs{counter}(1);        
        child_2 = CommonHandles.HC.Node.Childs{counter}(2);
    end
    
    if CommonHandles.HC.Node.ShowChilds{counter} == 0 
        pos = CommonHandles.HC.Node.Panel{counter}.Position;
        
        %CommonHandles.HC.Node.ActualLevel{child_1} = CommonHandles.HC.Node.Level{child_1};
        %maxDepth = max([CommonHandles.HC.Node.ActualLevel{:}]);
        if ~isempty(CommonHandles.HC.Node.Childs{counter})
            %x = CommonHandles.HC.maxWidth/2^(maxDepth-1);
            %shift = x*CommonHandles.HC.incW;
            cellNumberOfParent = CommonHandles.HC.Node.CellNumbers{counter};
            depthChild1 = calculateDepth(child_1) - CommonHandles.HC.Node.Level{counter};
            depthChild2 = calculateDepth(child_2) - CommonHandles.HC.Node.Level{counter};
            [pos1_left,pos1_right] = spaceBetweenChilds(cellNumberOfParent,depthChild1,depthChild2);
            %set(CommonHandles.HC.Node.Panel{child_1},'Visible','on','Position',[pos(1)-shift/2 pos(2)-115 pos(3) pos(4)]);
            %set(CommonHandles.HC.Node.Panel{child_2},'Visible','on','Position',[pos(1)+shift/2 pos(2)-115 pos(3) pos(4)]);
            set(CommonHandles.HC.Node.Panel{child_1},'Visible','on','Position',[pos(1)-pos1_left pos(2)-(115 + 115/5) pos(3) pos(4)]);
            set(CommonHandles.HC.Node.Panel{child_2},'Visible','on','Position',[pos(1)+pos1_right pos(2)-(115 + 115/5) pos(3) pos(4)]);
            
            shiftNodes(counter);
            reDrawLines();
        end
        CommonHandles.HC.Node.ShowChilds{counter} = 1;
    else
        closeNodes(counter);
    end
    
function reDrawLines()
    global CommonHandles;
    delete(allchild(CommonHandles.HC.ax));
    for i=1:CommonHandles.HC.Node.Root               
        visibility = get(CommonHandles.HC.Node.Panel{i},'Visible');
        if ~isempty(CommonHandles.HC.Node.Childs{i})
            child_1 = CommonHandles.HC.Node.Childs{i}(1);
            childVisible = get(CommonHandles.HC.Node.Panel{child_1},'Visible');
            if strcmp(visibility,'on') && ~isempty(CommonHandles.HC.Node.Childs{i}) && strcmp(childVisible,'on')
                drawLines(i,CommonHandles.HC.Node.Childs{i}(1),CommonHandles.HC.Node.Childs{i}(2));
            end
        end                
    end
    
function closeNodes(counter)
%recursion for closing the opened nodes    
    global CommonHandles;
    if ~isempty(CommonHandles.HC.Node.Childs{counter})
        child_1 = CommonHandles.HC.Node.Childs{counter}(1);
        child_2 = CommonHandles.HC.Node.Childs{counter}(2);
    end

    if (~isempty(CommonHandles.HC.Node.Childs{counter}) && CommonHandles.HC.Node.ShowChilds{counter} == 1)
        set(CommonHandles.HC.Node.Panel{child_1},'Visible','off');
        CommonHandles.HC.ShiftedNodes(child_1) = 0;
        closeNodes(child_1);
        set(CommonHandles.HC.Node.Panel{child_2},'Visible','off');
        CommonHandles.HC.ShiftedNodes(child_2) = 0;
        closeNodes(child_2);        
    end
    reDrawLines();
    CommonHandles.HC.Node.ShowChilds{counter} = 0;

function count = getChildCells(counter)
    
    count = 0;    
    global CommonHandles;
    
    if ~isempty(CommonHandles.HC.Node.Childs{counter})
        child_1 = CommonHandles.HC.Node.Childs{counter}(1);
        child_2 = CommonHandles.HC.Node.Childs{counter}(2);
        
        count = count + CommonHandles.HC.Node.NumberOfCells{child_1};
        count = count + CommonHandles.HC.Node.NumberOfCells{child_2};
    end

function getRequiredFeatures(counter)

    global CommonHandles;
    
    if ~isempty(CommonHandles.HC.Node.Childs{counter})
        child_1 = CommonHandles.HC.Node.Childs{counter}(1);        
        CommonHandles.HC.Node.Level{child_1} = CommonHandles.HC.Node.ChildLevel{counter};
        CommonHandles.HC.Node.ChildLevel{child_1} = CommonHandles.HC.Node.Level{child_1} + 1;
        getRequiredFeatures(child_1);
        child_2 = CommonHandles.HC.Node.Childs{counter}(2);
        CommonHandles.HC.Node.Level{child_2} = CommonHandles.HC.Node.ChildLevel{counter};
        CommonHandles.HC.Node.ChildLevel{child_2} = CommonHandles.HC.Node.Level{child_2} + 1;
        getRequiredFeatures(child_2);
    else
        CommonHandles.HC.avgFeatures{end+1} = CommonHandles.HC.Node.LeafFeatures{counter};
    end
    
function counter = getRepresentativeImage(counter)

    global CommonHandles;
    CommonHandles.HC.avgFeatures = {};
    getRequiredFeatures(counter);
    for i=1:length(CommonHandles.HC.avgFeatures)
        allChildSamples(i,:) = CommonHandles.HC.avgFeatures{i};
    end
    for i=1:length(CommonHandles.HC.Node.LeafFeatures)
        allChilds(i,:) = CommonHandles.HC.Node.LeafFeatures{i};
    end
    summary = zeros(1,size(CommonHandles.HC.avgFeatures{1},2));    
    
    for i=1:size(CommonHandles.HC.avgFeatures,2)
        summary = summary + CommonHandles.HC.avgFeatures{i};
    end    
    
    summary = summary / size(CommonHandles.HC.avgFeatures,2);
    
    [~, minPosition] = min(pdist2(allChildSamples,summary,CommonHandles.HC.Default.Metric));
    
    %CommonHandles.HC.b{end+1}=[counter,minPosition];
    [~,indx]=ismember(allChildSamples(minPosition,:),allChilds,'rows');
    counter = indx;
    
function x = getHeigthOfTree(maxNumberOfImages)    
    x = floor(log2(maxNumberOfImages) + 1);
    
function moveNodesOnYLabel_callback(hObject, eventdata)
    
    global CommonHandles;    
    
    currentValue = floor(get(CommonHandles.HC.SliderY,'Value'));
    x = CommonHandles.HC.FirstValueForSliderY-currentValue;
    
    for i=1:size(CommonHandles.HC.Node.Panel,2)
       pos = get(CommonHandles.HC.Node.Panel{i},'Position');
       set(CommonHandles.HC.Node.Panel{i},'Position',[pos(1) pos(2)+CommonHandles.HC.incW*x pos(3) pos(4)]);
    end
    
    lines = allchild(CommonHandles.HC.ax);
    
    for i=1:length(lines)
        pos = get(lines(i),'YData');
        set(lines(i),'YData',[pos(1)+CommonHandles.HC.incW*x pos(2)+CommonHandles.HC.incW*x]);
    end
    
   CommonHandles.HC.FirstValueForSliderY = currentValue;
   
function moveNodesOnXLabel_callback(hObject, eventdata)
    
    global CommonHandles;
    
    currentValue = floor(get(CommonHandles.HC.SliderX,'Value'));
    x = CommonHandles.HC.FirstValueForSliderX-currentValue;
    
    for i=1:size(CommonHandles.HC.Node.Panel,2)
        pos = get(CommonHandles.HC.Node.Panel{i},'Position');
        set(CommonHandles.HC.Node.Panel{i},'Position',[pos(1)+CommonHandles.HC.incW*x pos(2) pos(3) pos(4)]);
    end
    
    lines = allchild(CommonHandles.HC.ax);
    
    for i=1:length(lines)
        pos = get(lines(i),'XData');
        set(lines(i),'XData',[pos(1)+CommonHandles.HC.incW*x pos(2)+CommonHandles.HC.incW*x]);
    end
    
    CommonHandles.HC.FirstValueForSliderX = currentValue;
    
function depth = calculateDepth(node)

    global CommonHandles;
    
    if ~isempty(CommonHandles.HC.Node.Childs{node})
        child_1 = CommonHandles.HC.Node.Childs{node}(1);
        child_2 = CommonHandles.HC.Node.Childs{node}(2);
    
        depth_left = calculateDepth(child_1);
        depth_right = calculateDepth(child_2);
        depth = max(depth_left,depth_right);
    else
        depth = CommonHandles.HC.Node.Level{node};
    end

function [pos1_left, pos1_right] = spaceBetweenChilds(cellNumberOfParent,depthChild1,depthChild2)

    global CommonHandles;
    
    imgW = CommonHandles.HC.incW;
    
    if(depthChild1==1 && depthChild2~=1)
        pos1_left = imgW;
        pos1_right = 0;
    elseif(depthChild1~=1 && depthChild2==1)
        pos1_left = 0;
        pos1_right = imgW;
    elseif(depthChild1==1 && depthChild2==1)
        pos1_left = imgW/2;
        pos1_right = imgW/2;
        %{
    elseif(depthChild1<depthChild2 && depthChild1~=1)
        pos1_left = imgW;
        pos1_right = imgW;
    elseif(depthChild1>depthChild2 && depthChild2~=1)   
        pos1_left = imgW;
        pos1_right = imgW;
    elseif(depthChild1==depthChild2 && depthChild1~=1)
        x = 2^depthChild1 / 2;
        pos1_left = x*imgW;
        pos1_right = x*imgW;
%}
    else
        pos1_left = imgW;
        pos1_right = imgW;
    end
    
function shiftNodes(counter)

    global CommonHandles;
    
    if counter < CommonHandles.HC.Node.Root
        parent = CommonHandles.HC.Node.Parent{counter};
        if CommonHandles.HC.Node.Childs{parent}(1) == counter
            brother = CommonHandles.HC.Node.Childs{parent}(2);
            left = CommonHandles.HC.Node.Childs{parent}(1);
            right = CommonHandles.HC.Node.Childs{parent}(2);
        else
            brother = CommonHandles.HC.Node.Childs{parent}(1);
            left = CommonHandles.HC.Node.Childs{parent}(1);
            right = CommonHandles.HC.Node.Childs{parent}(2);
        end

        if ~isempty(CommonHandles.HC.Node.Childs{brother})
            brothersChild_1 = CommonHandles.HC.Node.Childs{brother}(1);
            %brothersChild_2 = CommonHandles.HC.Node.Childs{brother}(2);
            brothersChildVisibility = get(CommonHandles.HC.Node.Panel{brothersChild_1},'Visible');

            if strcmp(brothersChildVisibility,'on') && ~(CommonHandles.HC.ShiftedNodes(left) == 1 && CommonHandles.HC.ShiftedNodes(right) == 1)

                leftPos = CommonHandles.HC.Node.Panel{left}.Position;
                set(CommonHandles.HC.Node.Panel{left},'Position',[leftPos(1)-(CommonHandles.HC.incW) leftPos(2) leftPos(3) leftPos(4)]);
                
                rightPos = CommonHandles.HC.Node.Panel{right}.Position;
                set(CommonHandles.HC.Node.Panel{right},'Position',[rightPos(1)+(CommonHandles.HC.incW) rightPos(2) rightPos(3) rightPos(4)]);
                
                shiftChildsRecursion(left, 1);
                shiftChildsRecursion(right, 2);
                
                CommonHandles.HC.ShiftedNodes(left) = 1;
                CommonHandles.HC.ShiftedNodes(right) = 1;
            end
        end
        
        if parent ~= CommonHandles.HC.Node.Root && CommonHandles.HC.Node.Parent{parent} <= CommonHandles.HC.Node.Root
            grandParent = CommonHandles.HC.Node.Parent{parent};
            if CommonHandles.HC.Node.Childs{grandParent}(1) == parent
                parentBro = CommonHandles.HC.Node.Childs{grandParent}(2);
            else
                parentBro = CommonHandles.HC.Node.Childs{grandParent}(1);
            end
            
            closeNodes(parentBro);
        end
    end
    
function shiftChildsRecursion(node, side)

    global CommonHandles;

    if side == 1
        if ~isempty(CommonHandles.HC.Node.Childs{node})
            leftChild_1 = CommonHandles.HC.Node.Childs{node}(1);
            leftChild_1Pos = CommonHandles.HC.Node.Panel{leftChild_1}.Position;
            set(CommonHandles.HC.Node.Panel{leftChild_1},'Position',[leftChild_1Pos(1)-(CommonHandles.HC.incW) leftChild_1Pos(2) leftChild_1Pos(3) leftChild_1Pos(4)]);
            leftChild_2 = CommonHandles.HC.Node.Childs{node}(2);
            leftChild_2Pos = CommonHandles.HC.Node.Panel{leftChild_2}.Position;
            set(CommonHandles.HC.Node.Panel{leftChild_2},'Position',[leftChild_2Pos(1)-(CommonHandles.HC.incW) leftChild_2Pos(2) leftChild_2Pos(3) leftChild_2Pos(4)]);
            shiftChildsRecursion(leftChild_1,1);
            shiftChildsRecursion(leftChild_2,1);
        end
    elseif side == 2
        if ~isempty(CommonHandles.HC.Node.Childs{node})
            leftChild_1 = CommonHandles.HC.Node.Childs{node}(1);
            leftChild_1Pos = CommonHandles.HC.Node.Panel{leftChild_1}.Position;
            set(CommonHandles.HC.Node.Panel{leftChild_1},'Position',[leftChild_1Pos(1)+(CommonHandles.HC.incW) leftChild_1Pos(2) leftChild_1Pos(3) leftChild_1Pos(4)]);
            leftChild_2 = CommonHandles.HC.Node.Childs{node}(2);
            leftChild_2Pos = CommonHandles.HC.Node.Panel{leftChild_2}.Position;
            set(CommonHandles.HC.Node.Panel{leftChild_2},'Position',[leftChild_2Pos(1)+(CommonHandles.HC.incW) leftChild_2Pos(2) leftChild_2Pos(3) leftChild_2Pos(4)]);
            shiftChildsRecursion(leftChild_1,2);
            shiftChildsRecursion(leftChild_2,2);
        end
    end
    
function drawLines(counter,child_1,child_2)

    global CommonHandles;
        
    posParent = CommonHandles.HC.Node.Panel{counter}.Position;
    posChild1 = CommonHandles.HC.Node.Panel{child_1}.Position;
    posChild2 = CommonHandles.HC.Node.Panel{child_2}.Position;
    line('xdata',[posParent(1)+(CommonHandles.HC.incW/2),posChild1(1)+(CommonHandles.HC.incW/2)],'ydata',[posParent(2),posChild1(2)+CommonHandles.HC.incW+15],'Parent',CommonHandles.HC.ax,'LineWidth',sqrt(CommonHandles.HC.Node.NumberOfCells{child_1}+1),'Color',[0.4,0.4,0.4]);
    line('xdata',[posParent(1)+(CommonHandles.HC.incW/2),posChild2(1)+(CommonHandles.HC.incW/2)],'ydata',[posParent(2),posChild2(2)+CommonHandles.HC.incW+15],'Parent',CommonHandles.HC.ax,'LineWidth',sqrt(CommonHandles.HC.Node.NumberOfCells{child_2}+1),'Color',[0.4,0.4,0.4]);
            


% --- Executes on button press in dendrogram.
function dendrogram_Callback(hObject, eventdata, handles)
% hObject    handle to dendrogram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
%CommonHandles.AllFeaturesReduced

for i=1:size(CommonHandles.TrainingSet.Features,1)    
    temp = CommonHandles.TrainingSet.Features{i};
    for j=1:length(CommonHandles.HC.Default.InfoGainAttributes)
        temp2(1,j) = temp(1,CommonHandles.HC.Default.InfoGainAttributes(j));
    end
    reducedFeatures{i,1} = temp2;
    temp = 0;
    temp2 = 0;
end

for i=1:size(reducedFeatures,1)
    trainingFeatures(i,:) = reducedFeatures{i,1};
end

[method,metric] = testingForBestCluster(CommonHandles.TrainingSet.Features,CommonHandles.TrainingSet.Class,0);
tree = linkage(trainingFeatures,method,metric);
%tree = linkage(trainingFeatures,'average','seuclidean');

for i=1:length(CommonHandles.TrainingSet.Class)
    for j=1:3
        if CommonHandles.TrainingSet.Class{i,1}(j) == 1;
            textData{i} = num2str(j);
        end
    end
end

figure()
%[h,nodes,orig] = dendrogram(tree,0,'Labels',textData,'ColorThreshold',4);
[h,nodes,orig] = dendrogram(tree,0,'ColorThreshold',4);

%set(h(59),'Color','red','ButtonDownFcn',{@myClck,h});

%function myClck(hObj, evt, h)
    %disp('handles');
