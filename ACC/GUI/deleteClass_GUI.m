function varargout = deleteClass_GUI(varargin)
% DELETECLASS MATLAB code for deleteClass.fig
%      DELETECLASS, by itself, creates a new DELETECLASS or raises the existing
%      singleton*.
%
%      H = DELETECLASS returns the handle to a new DELETECLASS or the handle to
%      the existing singleton*.
%
%      DELETECLASS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DELETECLASS.M with the given input arguments.
%
%      DELETECLASS('Property','Value',...) creates a new DELETECLASS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before deleteClass_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to deleteClass_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help deleteClass

% Last Modified by GUIDE v2.5 12-Oct-2015 17:24:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @deleteClass_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @deleteClass_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before deleteClass is made visible.
function deleteClass_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to deleteClass (see VARARGIN)

% Choose default command line output for deleteClass
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes deleteClass wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global CommonHandles;

% list the classes
for i=1:numel(CommonHandles.Classes)
    className{i} = CommonHandles.Classes{i}.Name;
end
set(handles.deletePopup, 'String', className);


% --- Outputs from this function are returned to the command line.
function varargout = deleteClass_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in selectDelete.
function selectDelete_Callback(hObject, eventdata, handles)
% hObject    handle to selectDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

% get the selected class
nameList = get(handles.deletePopup, 'String');
name = char(nameList(get(handles.deletePopup, 'Value')));

% if it has child(s), delete them too
i = length(CommonHandles.Classes);
while i > 1
    if(strcmp(CommonHandles.Classes{i-1}.Type,'Normal') && (strcmp(CommonHandles.Classes{i-1}.Name,name)) && (strcmp(CommonHandles.Classes{i}.Type,'Child')))
        childName = CommonHandles.Classes{i}.Name;
        deleteFromClasses(childName);
    else        
        i = i - 1;
    end
end

deleteFromClasses(name);
    
regressionClassChecker();
refreshClassesList();

uiresume(gcbf);
close;


% --- Executes on button press in cancelDelete.
function cancelDelete_Callback(hObject, eventdata, handles)
% hObject    handle to cancelDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(gcbf); 
close;


% --- Executes on selection change in deletePopup.
function deletePopup_Callback(hObject, eventdata, handles)
% hObject    handle to deletePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns deletePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from deletePopup


% --- Executes during object creation, after setting all properties.
function deletePopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to deletePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
