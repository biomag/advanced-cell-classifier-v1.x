function varargout = outlierTreeSettingsGUI(varargin)
% outlierTreeSettingsGUIGUI MATLAB code for outlierTreeSettingsGUIGUI.fig
%      outlierTreeSettingsGUIGUI, by itself, creates a new outlierTreeSettingsGUIGUI or raises the existing
%      singleton*.
%
%      H = outlierTreeSettingsGUIGUI returns the handle to a new outlierTreeSettingsGUIGUI or the handle to
%      the existing singleton*.
%
%      outlierTreeSettingsGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in outlierTreeSettingsGUI.M with the given input arguments.
%
%      outlierTreeSettingsGUI('Property','Value',...) creates a new outlierTreeSettingsGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before outlierTreeSettingsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to outlierTreeSettingsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help outlierTreeSettingsGUI

% Last Modified by GUIDE v2.5 03-Feb-2016 11:46:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @outlierTreeSettingsGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @outlierTreeSettingsGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before outlierTreeSettingsGUI is made visible.
function outlierTreeSettingsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to outlierTreeSettingsGUI (see VARARGIN)

% Choose default command line output for outlierTreeSettingsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes outlierTreeSettingsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

method = {'average','complete','single','weighted','centroid','median','ward'};
set(handles.methodPopup,'String',method);
metric = {'euclidean','seuclidean','cosine','correlation','chebychev','minkowski','spearman','cityblock'};
set(handles.metricPopup,'String',metric);
featureSelection = {'Attribute Reduction'};
set(handles.featureReductionPopup,'String',featureSelection);
set(handles.featReduct,'Visible','off', 'String', '20');


% --- Outputs from this function are returned to the command line.
function varargout = outlierTreeSettingsGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in confirmButton.
function confirmButton_Callback(hObject, eventdata, handles)
% hObject    handle to confirmButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

CommonHandles.HC.Default.TrainParams = get(handles.cmd,'String');
CommonHandles.HC.Default.NumberOfOutliers = str2double(get(handles.numberOfOutliers,'String'));
metricList = get(handles.metricPopup,'String');
CommonHandles.HC.Default.Metric = char(metricList(get(handles.metricPopup, 'Value')));
methodList = get(handles.methodPopup,'String');
CommonHandles.HC.Default.Method = char(methodList(get(handles.methodPopup, 'Value')));

if get(handles.metricMethodCB,'Value') == get(handles.metricMethodCB,'Max')
    [CommonHandles.HC.Default.Method,CommonHandles.HC.Default.Metric] = testingForBestCluster(CommonHandles.TrainingSet.Features,CommonHandles.TrainingSet.Class,0);
end

CommonHandles.HC.Default.SupportInformation = str2double(get(handles.featReduct,'String'));

uiresume(gcbf); 
close;

% --- Executes on button press in closeButton.
function closeButton_Callback(hObject, eventdata, handles)
% hObject    handle to closeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(gcbf); 
close;

function cmd_Callback(hObject, eventdata, handles)
% hObject    handle to cmd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cmd as text
%        str2double(get(hObject,'String')) returns contents of cmd as a double


% --- Executes during object creation, after setting all properties.
function cmd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cmd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function numberOfOutliers_Callback(hObject, eventdata, handles)
% hObject    handle to numberOfOutliers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numberOfOutliers as text
%        str2double(get(hObject,'String')) returns contents of numberOfOutliers as a double


% --- Executes during object creation, after setting all properties.
function numberOfOutliers_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numberOfOutliers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in metricMethodCB.
function metricMethodCB_Callback(hObject, eventdata, handles)
% hObject    handle to metricMethodCB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of metricMethodCB
if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.methodPopup ,'Visible','off');
    set(handles.metricPopup ,'Visible','off');
    set(handles.text6 ,'Visible','off');
    set(handles.text7 ,'Visible','off');
else
    set(handles.methodPopup ,'Visible','on');
    set(handles.metricPopup ,'Visible','on');
    set(handles.text6 ,'Visible','on');
    set(handles.text7 ,'Visible','on');
end

% --- Executes on selection change in metricPopup.
function metricPopup_Callback(hObject, eventdata, handles)
% hObject    handle to metricPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns metricPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from metricPopup


% --- Executes during object creation, after setting all properties.
function metricPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to metricPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in methodPopup.
function methodPopup_Callback(hObject, eventdata, handles)
% hObject    handle to methodPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns methodPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from methodPopup


% --- Executes during object creation, after setting all properties.
function methodPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to methodPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in featureReductionPopup.
function featureReductionPopup_Callback(hObject, eventdata, handles)
% hObject    handle to featureReductionPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns featureReductionPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from featureReductionPopup

type = get(handles.featureReductionPopup,'String');
if strcmp(type,'Attribute Reduction')
    set(handles.featReduct,'Visible','on');
end


% --- Executes during object creation, after setting all properties.
function featureReductionPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to featureReductionPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function featReduct_Callback(hObject, eventdata, handles)
% hObject    handle to featReduct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of featReduct as text
%        str2double(get(hObject,'String')) returns contents of featReduct as a double


% --- Executes during object creation, after setting all properties.
function featReduct_CreateFcn(hObject, eventdata, handles)
% hObject    handle to featReduct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
