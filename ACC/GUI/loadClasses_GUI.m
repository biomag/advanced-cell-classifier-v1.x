function varargout = loadClasses_GUI(varargin)
% LOADCLASSES_GUI MATLAB code for loadClasses_GUI.fig
%      LOADCLASSES_GUI, by itself, creates a new LOADCLASSES_GUI or raises the existing
%      singleton*.
%
%      H = LOADCLASSES_GUI returns the handle to a new LOADCLASSES_GUI or the handle to
%      the existing singleton*.
%
%      LOADCLASSES_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOADCLASSES_GUI.M with the given input arguments.
%
%      LOADCLASSES_GUI('Property','Value',...) creates a new LOADCLASSES_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before loadClasses_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to loadClasses_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help loadClasses_GUI

% Last Modified by GUIDE v2.5 13-Oct-2015 09:21:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loadClasses_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @loadClasses_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before loadClasses_GUI is made visible.
function loadClasses_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to loadClasses_GUI (see VARARGIN)

% Choose default command line output for loadClasses_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes loadClasses_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = loadClasses_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadFile.
function loadFile_Callback(hObject, eventdata, handles)
% hObject    handle to loadFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
addpath('Utils');

% if there is already existing Classes, delete them or skip loading new one
button = questdlg('Do You want to remove/replace all existing classes?','Loading classes','Yes','No','No');
if strcmp(button,'Yes')

    CommonHandles.Classes = {};
    CommonHandles.TrainingSet.MetaDataFilename = {};
    CommonHandles.TrainingSet.ImageName = {};
    CommonHandles.TrainingSet.OriginalImageName = {};
    CommonHandles.TrainingSet.CellNumber = {};
    CommonHandles.TrainingSet.Features = {};
    CommonHandles.TrainingSet.Class = {};
    
    classesFile = get(handles.classesPath, 'String');
    loadClasses(classesFile);    

else
    disp('Dont remove classes');
end

uiresume(gcbf); 
close;

% --- Executes on button press in cancelButton.
function cancelButton_Callback(hObject, eventdata, handles)
% hObject    handle to cancelButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(gcbf); 
close;


function classesPath_Callback(hObject, eventdata, handles)
% hObject    handle to classesPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of classesPath as text
%        str2double(get(hObject,'String')) returns contents of classesPath as a double


% --- Executes during object creation, after setting all properties.
function classesPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classesPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectFile.
function selectFile_Callback(hObject, eventdata, handles)
% hObject    handle to selectFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% select which file to be loaded
classesFile = uigetfile();
set(handles.classesPath, 'String', classesFile);
