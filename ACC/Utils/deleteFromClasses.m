function [] = deleteFromClasses(name)
% deletes a class/button from the list, with the given name
%   if the selected class type is Regression, we have to clear the CH.Regression too
%   we must delete data from the TrainingSet and from the Classes too
global CommonHandles;

i = length(CommonHandles.Classes);
while (i > 0)
    if(strcmp(CommonHandles.Classes{i}.Name,name))
        
        if(strcmp(CommonHandles.Classes{i}.Type,'Regression'))
            CommonHandles.Regression{CommonHandles.Classes{i}.ID} = {};
        end
        
        j = length(CommonHandles.TrainingSet.Class);
        while (j > 0)
            if (CommonHandles.TrainingSet.Class{j}(i) == 1)
                CommonHandles.TrainingSet.MetaDataFilename(j) = [];
                CommonHandles.TrainingSet.ImageName(j) = [];
                CommonHandles.TrainingSet.OriginalImageName(j) = [];
                CommonHandles.TrainingSet.CellNumber(j) = [];
                CommonHandles.TrainingSet.Features(j) = [];
                CommonHandles.TrainingSet.Class(j) = [];
            else
                CommonHandles.TrainingSet.Class{j}(i) = [];
            end
            j=j-1;
        end        
        CommonHandles.ClassImages.remove(CommonHandles.Classes{i}.Name);
        CommonHandles.Classes(i) = [];
    end
    i=i-1;
end

end

