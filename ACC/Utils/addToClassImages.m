function addToClassImages(counter)
%this function adds the currently selected cell to the ClassImages map.
%counter is the class index for the currently selected cell.

global CommonHandles;

classImg.Image = CommonHandles.CurrentSelectedCell;
classImg.ClassName = CommonHandles.Classes{counter}.Name;
classImg.CellNumber = CommonHandles.SelectedCell;
classImg.ImageName = CommonHandles.SelectedImageName;
if ~CommonHandles.ClassImages.isKey(classImg.ClassName)
    newclass = struct('Images',[],'selected',struct(),'sHandle',0);
    CommonHandles.ClassImages(classImg.ClassName) = newclass;
end
cimgs = CommonHandles.ClassImages(classImg.ClassName);
cimgs.Images{end+1} = classImg;
CommonHandles.ClassImages(classImg.ClassName) = cimgs;

end

