function [ ] = loadClasses( classesFile )
% load the data into CommonHandles.Classes from the file with the
% classesFile - path

    global CommonHandles;

    fileName = ['Classes' filesep classesFile];
    tmp = load(fileName,'-mat');
    CommonHandles.Classes = tmp.tmp_classes;

    for i=1:length(CommonHandles.Classes)
        CommonHandles.Classes{1,i}.LabelNumbers = 0;
    end
    
    CommonHandles.Regression = {};
    
    regressionClassChecker();
    refreshClassesList();

end

