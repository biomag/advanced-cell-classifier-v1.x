function CommonHandles = setImageIntensity(CommonHandles)

if CommonHandles.MainView.StrechImage == 1

     % Needs to be verifyed this saturation value
     tolr = 0.001;
     tolg = 0.001;
     tolb = 0.005;

     sl(:,1) = stretchlim(CommonHandles.CurrentOriginalImageNoTouch(:,:,1), tolr); %smalls = find(sl(2,:) < 0.05); sl(2,smalls) = 1;
     sl(:,2) = stretchlim(CommonHandles.CurrentOriginalImageNoTouch(:,:,2), tolg);
     sl(:,3) = stretchlim(CommonHandles.CurrentOriginalImageNoTouch(:,:,3), tolb);
     CommonHandles.CurrentOriginalImage = imadjust(CommonHandles.CurrentOriginalImageNoTouch,sl,[]);
 else
     sl(:, 1)  = [CommonHandles.MainView.StrechMinR/255, CommonHandles.MainView.StrechMaxR/255]';
     sl(:, 2)  = [CommonHandles.MainView.StrechMinG/255, CommonHandles.MainView.StrechMaxG/255]';
     sl(:, 3)  = [CommonHandles.MainView.StrechMinB/255, CommonHandles.MainView.StrechMaxB/255]';
     % check inversion     
     CommonHandles.CurrentOriginalImage = imadjust(CommonHandles.CurrentOriginalImageNoTouch,sl,[]);
end

% swap colors
if CommonHandles.MainView.SwapMap > 1
    tmpImg = CommonHandles.CurrentOriginalImage;
    switch CommonHandles.MainView.SwapMap
        case 2 %RBG
            CommonHandles.CurrentOriginalImage(:,:,2) = tmpImg(:,:,3);
            CommonHandles.CurrentOriginalImage(:,:,3) = tmpImg(:,:,2);
        case 3 %GRB
            CommonHandles.CurrentOriginalImage(:,:,1) = tmpImg(:,:,2);
            CommonHandles.CurrentOriginalImage(:,:,2) = tmpImg(:,:,1);
            CommonHandles.CurrentOriginalImage(:,:,3) = tmpImg(:,:,3);
        case 4 %GRB
            CommonHandles.CurrentOriginalImage(:,:,1) = tmpImg(:,:,2);
            CommonHandles.CurrentOriginalImage(:,:,2) = tmpImg(:,:,3);
            CommonHandles.CurrentOriginalImage(:,:,3) = tmpImg(:,:,1);
        case 5 %GRB
            CommonHandles.CurrentOriginalImage(:,:,1) = tmpImg(:,:,3);
            CommonHandles.CurrentOriginalImage(:,:,2) = tmpImg(:,:,1);
            CommonHandles.CurrentOriginalImage(:,:,3) = tmpImg(:,:,2);
        case 6 %GRB
            CommonHandles.CurrentOriginalImage(:,:,1) = tmpImg(:,:,3);
            CommonHandles.CurrentOriginalImage(:,:,2) = tmpImg(:,:,2);
            CommonHandles.CurrentOriginalImage(:,:,3) = tmpImg(:,:,1);
        otherwise
            disp('Wooops');            
    end
    
end
