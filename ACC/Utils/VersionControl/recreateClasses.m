function recreateClasses()
%This function tries to recover original classes based on the information
%stored in the training set. It is for rebuilding the Classes field.

global CommonHandles;

CommonHandles.Classes = cell(1,length(CommonHandles.TrainingSet.Class{1}));
reserveCH = CommonHandles;
for i=1:length(CommonHandles.Classes)            
    
    classMatrix = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.TrainingSet.Class{1}),length(CommonHandles.TrainingSet.Class))';
    firstIndex = find(classMatrix(:,i));
    if isempty(firstIndex)
        icon = ones(60,60,3).*128;        
    else
        %splitted image name
        SINbyfilesep = strsplit(CommonHandles.TrainingSet.ImageName{firstIndex(1)},{'/','\'});
        lastbyfilesep = SINbyfilesep{end};
        SINbyunderscore = strsplit(lastbyfilesep,'_');
        for j=1:length(CommonHandles.PlatesNames)
            if strcmp(CommonHandles.PlatesNames{j},SINbyunderscore{1})
                break;
            end
        end
        CommonHandles.SelectedImageName = CommonHandles.TrainingSet.ImageName{firstIndex(1)};
        CommonHandles.SelectedOriginalImageName = CommonHandles.TrainingSet.OriginalImageName{firstIndex(1)};
        CommonHandles.SelectedCell = CommonHandles.TrainingSet.CellNumber{firstIndex(1)};
        CommonHandles = loadImageForLowProb( CommonHandles, j );
        CommonHandles = showTheCell( CommonHandles );
        icon = CommonHandles.CurrentSelectedCell;        
    end
        
    if CommonHandles.RegressionClass(i)
        CommonHandles.Classes{i}.Type = 'reg';
    else
        CommonHandles.Classes{i}.Type = 'normal';
    end
    CommonHandles.Classes{i}.Icon = imresize(icon,[60,NaN]);
    CommonHandles.Classes{i}.ID = i;
    CommonHandles.Classes{i}.Name = num2str(i);
    CommonHandles.Classes{i}.LabelNumbers = sum(classMatrix(:,i));    
end
CommonHandles.SelectedImageName = reserveCH.SelectedImageName;
CommonHandles.SelectedOriginalImageName = reserveCH.SelectedOriginalImageName;
CommonHandles.SelectedCell = reserveCH.SelectedCell;

end
