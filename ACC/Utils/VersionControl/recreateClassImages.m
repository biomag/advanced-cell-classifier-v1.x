function recreateClassImages()
%This function tries to recover the class images from the training set data
% it is very similar to the recreateClasses.m Here we suppose that we the
% Classes array is already filled in.
global CommonHandles;

reserveCH = CommonHandles;
for i=1:length(CommonHandles.Classes)            
    
    classMatrix = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.TrainingSet.Class{1}),length(CommonHandles.TrainingSet.Class))';
    firstIndex = find(classMatrix(:,i));
    
    for k=1:length(firstIndex)
        %splitted image name
        SINbyfilesep = strsplit(CommonHandles.TrainingSet.ImageName{firstIndex(k)},{'/','\'});
        lastbyfilesep = SINbyfilesep{end};
        SINbyunderscore = strsplit(lastbyfilesep,'_');
        for j=1:length(CommonHandles.PlatesNames)
            if strcmp(CommonHandles.PlatesNames{j},SINbyunderscore{1})
                break;
            end
        end
        CommonHandles.SelectedImageName = CommonHandles.TrainingSet.ImageName{firstIndex(k)};
        CommonHandles.SelectedOriginalImageName = CommonHandles.TrainingSet.OriginalImageName{firstIndex(k)};
        CommonHandles.SelectedCell = CommonHandles.TrainingSet.CellNumber{firstIndex(k)};
        CommonHandles = loadImageForLowProb( CommonHandles, j );
        CommonHandles = showTheCell( CommonHandles );
        
        addToClassImages(i);
        
    end
            
end
CommonHandles.SelectedImageName = reserveCH.SelectedImageName;
CommonHandles.SelectedOriginalImageName = reserveCH.SelectedOriginalImageName;
CommonHandles.SelectedCell = reserveCH.SelectedCell;

end

