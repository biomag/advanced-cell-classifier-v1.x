function [] = deleteFromClassImages( cellNumber, imageName, counter )
%DELETEFROMCLASSIMAGES Summary of this function goes here
%   Detailed explanation goes here
global CommonHandles;

className = CommonHandles.Classes{counter}.Name;

for i = 1:size(CommonHandles.TrainingSet.CellNumber)
    if (CommonHandles.TrainingSet.CellNumber{i} == cellNumber) && (strcmp(CommonHandles.TrainingSet.ImageName{i},imageName))
        % Remove from TrainingSet
        CommonHandles.TrainingSet.MetaDataFilename(i) = [];
        CommonHandles.TrainingSet.ImageName(i) = [];
        CommonHandles.TrainingSet.OriginalImageName(i) = [];
        CommonHandles.TrainingSet.CellNumber(i) = [];
        CommonHandles.TrainingSet.Features(i) = [];
        CommonHandles.TrainingSet.Class(i) = [];
        
        % Search for classImage to remove
        classImgObj = CommonHandles.ClassImages(className);
        for j = 1:length(classImgObj.Images)
            if (classImgObj.Images{j}.CellNumber == cellNumber) && (strcmp(classImgObj.Images{j}.ImageName,imageName))
                classImgObj.Images(j) = [];
                CommonHandles.ClassImages(className) = classImgObj;
                break;
            end
        end
        
        CommonHandles.Classes{counter}.LabelNumbers = CommonHandles.Classes{counter}.LabelNumbers - 1;
        
        refreshClassesList();
        
        break;
    end
end

end
