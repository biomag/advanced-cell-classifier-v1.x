function [] = oneClassClassifier()
% ONECLASSCLASSIFIER
% the fully automated testing is controlled by this function
% sinFunc, squareRing_1, squareRing_2, ellipses_1, ellipses_2, bubbles
% generateSurface, generatePredictionImages, generateDatasetImage, generateNoisyImage
    testCase = 'bubbles';
    whatToDo = 'generateDatasetImage';
    useNoise = 0;
  
    if strcmp(whatToDo,'generateSurface')
        % j is the number of features
        % i is the number of training samples
        figure();
        hold on;
        feat = 5:15:125;
        train = 50:150:2000;
    
        for j = 1:length(feat)    
            testData = genTestData(5000, feat(j), testCase, useNoise);
            for i = 1:length(train)
                perc = testResultsForLibSVM(train(i), testData, feat(j), testCase, useNoise);
                fprintf('perc: %d \nFeatures: %i \nSamples: %i \n',perc,feat(j),train(i));

                %plot3(j,perc,i,'bo');           
                t(i,j) = perc;
            end
        end

        surf(feat,train,t);
        xlabel('features');
        ylabel('samples');
        zlabel('accuracy');
        view(20,20);
        shading('faceted');
        colormap('gray');
        caxis([20 100]);
    
    elseif(strcmp(whatToDo,'generatePredictionImages'))
    
        testData = genTestData(5000, 2, testCase, useNoise);
            for i = [50,100,200,500,1000]
                perc = testResultsForLibSVM(i, testData, 2, testCase, useNoise);            
                fprintf('perc: %d \nFeatures: %i \nSamples: %i \n',perc,2,i);
            end
            
    elseif(strcmp(whatToDo,'generateDatasetImage'))
    
        testData = genTestData(5000, 3, testCase, useNoise,0); 
        perc = testResultsForLibSVM(1000, testData, 3, testCase, useNoise,0);
    
    elseif(strcmp(whatToDo,'generateNoisyImage'))
        
        feat = 5:15:350;
        figure();hold on;
        for db = [10,15,20,25,30]
            for j = 1:length(feat)    
                testData = genTestData(5000, feat(j), testCase, useNoise, db);
                perc = testResultsForLibSVM(1000, testData, feat(j), testCase, useNoise, db);            
                
                t(j) = perc;
                
            end
            
            if db == 50
                col = 'r*';
            elseif db == 40
                col = 'b*';
            elseif db == 30
                col = 'g*';
            elseif db == 10
                col = 'ro';
            else
                col = 'bo';
            end
            
            plot(feat(:),t(:),'LineWidth',2);
            legend({'10dB','15dB','20dB','25dB','30dB'});
            
        end
        %testData = genTestData(5000, 5, testCase, 1, -5);
        %perc = testResultsForLibSVM(1000, testData, 5, testCase, 1, -5);
    end
    
    
    %if strcmp(testCase,'simpleGaussDist')
    %    results = simpleGauss(testCase);        
    %    visualizeResults(results);
    %end

    %[y, p, ~, ~] = sacCrossValidate(CommonHandles.ClassifierNames{CommonHandles.SelectedClassifier},CommonHandles.SALT.trainingData, 10, 1);

end