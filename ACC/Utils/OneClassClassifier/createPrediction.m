function perc = createPrediction(features, flags)
% creates a prediction from the loaded dataset
%   imports the flags and compares it to the prediction
% INPUT:
%   features - the features from the loaded dataset (usually from SALT.trainingdata
%               or from [OCC]LoadAllFeatures)
%   flags - specially for comparing to the prediction
% OUTPUT:
%   perc - percentage of the true prediction
%
% also creates and prints some statistical information
    global CommonHandles;

    if CommonHandles.SALT.initialized    
        CommonHandles.SALT.trainingData.instances = features;
        CommonHandles.SALT.trainingData.labels = ones(size(features, 1));
        [out,~] = sacPredict(CommonHandles.SALT.model, CommonHandles.SALT.trainingData);
    end;
    
    tn = 0;
    tp = 0;
    fp = 0;
    fn = 0;
    nofFlags = length(flags);
    
    for i=1:nofFlags
       if (flags(i) == out(i) && out(i) == 2)
           tp= tp + 1;
       elseif (flags(i) ~= out(i) && out(i) == 2)
           fp = fp + 1;
       elseif (flags(i) == out(i) && out(i) == 1)
           tn = tn + 1;
       elseif (flags(i) ~= out(i) && out(i) == 1)
           fn = fn + 1;
       end
    end
    
    perc = 100*(tp+tn)/nofFlags;
    fprintf('Number of labeled (synthetic) data: %i \nPercentage of good labels: %0.2f%% \n',nofFlags,perc);
    fprintf('FP: %i \nTP: %i \nTN: %i \nFN: %i \n',fp,tp,tn,fn);

end

