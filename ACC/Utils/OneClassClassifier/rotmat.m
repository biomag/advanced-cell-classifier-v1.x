function [a,b] = rotmat(x0,y0,x,y,theta)

    a = (cos(theta)*(x-x0) - sin(theta) * (y-y0)) + x0;
    b = (sin(theta)*(x-x0) + cos(theta) * (y-y0)) + y0;

end