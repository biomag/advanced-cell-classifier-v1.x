function data = generateClassifiedDataForSAC( dimension, nofCells, testCase )
% this function generates synthetic labeled data for SAC
%
% OUTPUT: 
%   data - is exactly the same like convertACC2SALT's output data
%           data.instances and data.labels must be set correctly

    counter = 1;
    data = [];

    data.instances = zeros(nofCells,dimension);
    data.labels = zeros(1,nofCells);
    data.classNames{1} = 'class1';
    data.classNames{2} = 'class2';

    %for i = 1:nofCells
    while counter <= nofCells
        data.name = 'default HCS project';
        data.featureName{counter} = ['feature' num2str(counter)];
        data.featureTypes{counter} = 'NUMERIC';
        
        if (strcmp(testCase,'simpleGaussDist'))
            %this is for the simple gaussian ditributions
            if counter <= nofCells/2
                for j=1:dimension
                    data.instances(counter,1) = rand();
                    data.instances(counter,j) = normrnd(5,2);
                end
            else
                for j=1:dimension
                    data.instances(counter,1) = rand();
                    data.instances(counter,j) = normrnd(5,2);
                end
            end
            counter = counter + 1;
        elseif (strcmp(testCase,'squareRing'))        
            x = 100 * rand();
            y = 100 * rand();

            if ~(x < 20 || x > 80 || y < 20 || y > 80) && ~((x > 30 && y > 30) && (x > 30 && y < 70) && (x < 70 && y > 30) && ( x < 70 && y < 70))
                data.instances(counter,1) = x;
                data.instances(counter,2) = y;
                data.labels(counter) = 1;
                counter = counter + 1;
            %elseif ((x > 30 && y > 30) && (x > 30 && y < 70) && (x < 70 && y > 30) && ( x < 70 && y < 70))
            %    data.instances(i,1) = x;
            %    data.instances(i,2) = y;
            %    data.labels(i) = 1;
            %else
            %    data.instances(i,1) = 25;
            %    data.instances(i,2) = 25;
            %    data.labels(i) = 1;
            end            
        elseif (strcmp(testCase,'sinFunc'))
            x = 100 * rand();
            y = 100 * rand();
            
            if (y < (10*sin(x/10)+50))
                data.instances(counter,1) = x;
                data.instances(counter,2) = y;
                data.labels(counter) = 1;
                counter = counter + 1;
            else
                data.instances(counter,1) = x;
                data.instances(counter,2) = y;
                data.labels(counter) = -1;
                counter = counter + 1;
            end
        elseif (strcmp(testCase,'ellipses'))
            
        end
    end

end