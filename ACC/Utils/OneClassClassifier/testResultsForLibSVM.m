function [perc] = testResultsForLibSVM( numberOfObjectsForTraining, testData , numberOfFeatures , testCase, useNoise, db )
%TESTRESULTSFORLIBSVM Summary of this function goes here
%   Detailed explanation goes here
    a = 30;
    counter = 1;
    
    while counter <= numberOfObjectsForTraining
        
        %this is for uniform-random noise
        z = (sqrt(28.87*28.87 / (10^(db/10)))) / 0.2887;
        
        if(useNoise == 1)
            for i=3:numberOfFeatures
                trainingData.instances(counter, i) = z * rand();
            end
        end
            
        if (strcmp(testCase,'squareRing_1'))        
            x = 100 * rand();
            y = 100 * rand();

            if ~(x < 20 || x > 80 || y < 20 || y > 80) && ~((x > 30 && y > 30) && (x > 30 && y < 70) && (x < 70 && y > 30) && ( x < 70 && y < 70))
                trainingData.instances(counter,1) = x;
                trainingData.instances(counter,2) = y;
                trainingData.labels(counter) = 1;
                counter = counter + 1;
            end
        elseif (strcmp(testCase,'squareRing_2'))        
            x = 100 * rand();
            y = 100 * rand();

            if (x < 20 || x > 80 || y < 20 || y > 80) || ((x > 30 && y > 30) && (x > 30 && y < 70) && (x < 70 && y > 30) && ( x < 70 && y < 70))
                trainingData.instances(counter,1) = x;
                trainingData.instances(counter,2) = y;
                trainingData.labels(counter) = 1;
                counter = counter + 1;
            end
        elseif(strcmp(testCase,'sinFunc'))
            x = 100 * rand();
            y = 100 * rand();
            
            if (y < (10*sin(x/10)+50))
                trainingData.instances(counter,1) = x;
                trainingData.instances(counter,2) = y;
                trainingData.labels(counter) = 1;
                counter = counter + 1;
            end
        elseif(strcmp(testCase,'ellipses_1'))
            if counter <= numberOfObjectsForTraining/2                
                x0 = 20;
                y0 = 45;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 6.3);
                [trainingData.instances(counter,1),trainingData.instances(counter,2)] = rotmat(x0,y0,x,y,theta);
            	trainingData.labels(counter) = 1;
                counter = counter + 1;
            else
                x0 = 40;
                y0 = 20;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 2.5);
                [trainingData.instances(counter,1),trainingData.instances(counter,2)] = rotmat(x0,y0,x,y,theta);
            	trainingData.labels(counter) = 1;
                counter = counter + 1;
            end
        elseif(strcmp(testCase,'ellipses_2'))            
            if counter <= numberOfObjectsForTraining/2                
                x0 = 20;
                y0 = 45;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 6.3);
                [trainingData.instances(counter,1),trainingData.instances(counter,2)] = rotmat(x0,y0,x,y,theta);
            	trainingData.labels(counter) = 1;
                counter = counter + 1;
            else
                x0 = 30;
                y0 = 35;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 4);
                [trainingData.instances(counter,1),trainingData.instances(counter,2)] = rotmat(x0,y0,x,y,theta);
            	trainingData.labels(counter) = 1;
                counter = counter + 1;
            end
        elseif(strcmp(testCase,'bubbles'))
            if counter <= numberOfObjectsForTraining/2
                for i=1:numberOfFeatures
                    trainingData.instances(counter,i) = normrnd(75,2);
                end
            	trainingData.labels(counter) = 1;
                counter = counter + 1;
            else
                for i=1:numberOfFeatures
                    trainingData.instances(counter,i) = normrnd(25,2);
                end
            	trainingData.labels(counter) = 1;
                counter = counter + 1;                
            end
        end
    end
    
    
   
    figure(10);
    maxX = max(testData.instances(:,1));
    maxY = max(testData.instances(:,2));
    minX = min(testData.instances(:,1));
    minY = min(testData.instances(:,2));
    axis([minX maxX minY maxY]);   
    set(gca, 'XTick', []);    
    set(gca, 'YTick', []);
    %set(gca,'visible','off');
    %axis EQUAL;
    hold on;
    for i=1:length(testData.instances)
        if (testData.labels(i) == 1)
            %plot(testData.instances(i,1),testData.instances(i,2),'r.');
            scatter3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),a,'MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0 0 0]);
            %scatter(testData.instances(i,1),testData.instances(i,2),a,'MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0 0 0]);
            %plot3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),'r.');
        else
            %plot(testData.instances(i,1),testData.instances(i,2),'b.');
            scatter3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),a,'MarkerEdgeColor',[0.517647 0.517647 0.517647],'MarkerFaceColor',[0.517647 0.517647 0.517647]);
            %scatter(testData.instances(i,1),testData.instances(i,2),a,'MarkerEdgeColor',[0.517647 0.517647 0.517647],'MarkerFaceColor',[0.517647 0.517647 0.517647]);
            %plot3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),'b.');
        end
    end

    %stdData = std(trainingData.instances);
    %meanData = mean(trainingData.instances);
    %trData = trainingData.instances - repmat(meanData, size(trainingData.instances,1),1);
    %trData = trData ./ repmat(stdData, size(trainingData.instances,1),1);     
    %tsData = testData.instances - repmat(meanData, size(testData.instances,1),1);
    %tsData = tsData ./ repmat(stdData, size(testData.instances,1),1);
    
    trData = trainingData.instances;
    tsData = testData.instances;
    
    if strcmp(testCase,'sinFunc')
        cmd = '-s 2 -t 2 -e 0.000001 -n 0.0001 -g 0.001';
    else
        cmd = '-s 2 -t 2 -e 0.000001 -n 0.0001 -g 0.008';
    end
    svmmodel = svmtrain(trainingData.labels', trData, cmd);
    [outL, ~, ~] = svmpredict(ones(size(tsData,1),1), tsData, svmmodel);
    
%{
    figure(11);
    axis([0 100 0 100]);
    hold on;
    for i=1:length(trainingData.labels);
        if (trainingData.labels(i) == 1)
            %plot(trainingData.instances(i,1),trainingData.instances(i,2),'r.');
            scatter(trainingData.instances(i,1),trainingData.instances(i,2),a,'MarkerEdgeColor',[1 1 0],'MarkerFaceColor',[1 0 0]);
        else
            %plot(trainingData.instances(i,1),trainingData.instances(i,2),'b.');
            scatter(trainingData.instances(i,1),trainingData.instances(i,2),a,'MarkerEdgeColor',[0 1 1],'MarkerFaceColor',[0 0 1]);
        end
    end
%}    
     
    nofFlags = length(testData.labels);
    f = zeros(nofFlags,1);

    for j=1:nofFlags
         f(j,:) = testData.labels(:,j);
    end

%{
    c = figure();
    set(c,'Visible','off');
    maxX = max(testData.instances(:,1));
    maxY = max(testData.instances(:,2));
    minX = min(testData.instances(:,1));
    minY = min(testData.instances(:,2));
    axis([minX maxX minY maxY]);
    set(gca, 'XTick', []);    
    set(gca, 'YTick', []);
    set(gca,'visible','off');
    hold on;
    for i=1:length(testData.instances)
        if(f(i) == outL(i))
            if (outL(i) == 1)
                %scatter(testData.instances(i,1),testData.instances(i,2),a,'MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0 0 0]);
                %plot3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),'bo');
                %scatter3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),a,'MarkerEdgeColor',[0 0 0],'MarkerFaceColor',[0 0 0]);
            else
                %scatter(testData.instances(i,1),testData.instances(i,2),a,'MarkerEdgeColor',[0.517647 0.517647 0.517647],'MarkerFaceColor',[0.517647 0.517647 0.517647]);
                %plot3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),'ro');
                %scatter3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),a,'MarkerEdgeColor',[0.517647 0.517647 0.517647],'MarkerFaceColor',[0.517647 0.517647 0.517647]);
            end
        else
            %scatter(testData.instances(i,1),testData.instances(i,2),a,'MarkerEdgeColor',[1 0 0],'MarkerFaceColor',[1 0 0]);
            %scatter3(testData.instances(i,1),testData.instances(i,2),testData.instances(i,3),a,'MarkerEdgeColor',[1 0 0],'MarkerFaceColor',[1 0 0]);
        end
    end
%} 
    tn = 0;
    tp = 0;
    fp = 0;
    fn = 0;

    for i=1:nofFlags
       if (f(i) == outL(i) && outL(i) == 1)
           tp= tp + 1;
       elseif (f(i) ~= outL(i) && outL(i) == 1)
           fp = fp + 1;
       elseif (f(i) == outL(i) && outL(i) == -1)
           tn = tn + 1;
       elseif (f(i) ~= outL(i) && outL(i) == -1)
           fn = fn + 1;
       end
    end

    perc = 100*(tp+tn)/nofFlags;
    precision = tp / (tp+fp);
    recall = tp / (tp+fn);
    fprintf('Number of labeled (synthetic) data: %i \nPercentage of good labels: %0.2f%% \n',nofFlags,perc);
    fprintf('FP: %i \nTP: %i \nTN: %i \nFN: %i \n',fp,tp,tn,fn);
    fprintf('Precision: %f \nRecall: %f\n',precision,recall);
    
    %export_fig([testCase '.eps']);
    %export_fig([testCase 'Tr' num2str(numberOfObjectsForTraining) 'Ft' num2str(numberOfFeatures) 'acc' num2str(perc) '.eps']);
    

end