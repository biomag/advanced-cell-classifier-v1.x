function [] = visualizeResults(matrix)
%VISUALIZERESULTS Summary of this function goes here
%   Detailed explanation goes here
    figure;
    hold on;
    imagesc(matrix);
    colormap(flipud(gray));  
    x = size(matrix,1);
    y = size(matrix,2);
    [a,b] = meshgrid(1:y,1:x);
        
    textStrings = num2str(matrix(:),'%0.2f');
    textStrings = strtrim(cellstr(textStrings));
    
    hStrings = text(a(:),b(:),textStrings(:), 'HorizontalAlignment','center');
    midValue = mean(get(gca,'CLim'));
    textColors = repmat(matrix(:) > midValue,1,3);
    
    set(hStrings,{'Color'},num2cell(textColors,2));

    set(gca,'XTick',1:y,'XTickLabel',{'1000','2000'},'YTick',1:x,'YTickLabel',{'10','20','30'},'TickLength',[0 0]);

end

