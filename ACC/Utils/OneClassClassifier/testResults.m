function [] = testResults( numberOfObjectsForTraining, numberOfGeneratedObjects , testCase )
%TESTINGRESULTS Summary of this function goes here
%   Detailed explanation goes here
    global CommonHandles;
    
    a = 30;
    
    CommonHandles.SALT.trainingData = [];
    data = generateClassifiedDataForSAC(2, numberOfGeneratedObjects, testCase);
    CommonHandles.SALT.trainingData.classNames = data.classNames;
    CommonHandles.SALT.trainingData.name = data.name;
    CommonHandles.SALT.trainingData.featureName = data.featureName;
    CommonHandles.SALT.trainingData.featureTypes = data.featureTypes;
    flags = data.labels;
    
    r = randi([1 numberOfGeneratedObjects],1,numberOfObjectsForTraining);
    
    for m = 1:numel(r)
       element = r(m);
       CommonHandles.SALT.trainingData.instances(m,:) = data.instances(element,:);
       CommonHandles.SALT.trainingData.labels(1,m) = data.labels(1,element);       
       CommonHandles.SALT.trainingData.featureName(1,m) = data.featureName(1,element);
       CommonHandles.SALT.trainingData.featureTypes(1,m) = data.featureTypes(1,element);
    end
    
    %CommonHandles.SALT.model = sacTrain(CommonHandles.ClassifierNames{CommonHandles.SelectedClassifier},CommonHandles.SALT.trainingData);
    %[out,~] = sacPredict(CommonHandles.SALT.model, data);
    %svmmodel = fitcsvm(CommonHandles.SALT.trainingData.instances,CommonHandles.SALT.trainingData.labels,'OutlierFraction',0.005,'KernelFunction','rbf');
    %[out,~] = predict(svmmodel, data.instances);
    
    
    
    figure(5);
    hold on;
    for i=1:length(testData.instances)
        if (data.labels(i) == 1)
            scatter(data.instances(i,1),data.instances(i,2),a,'MarkerEdgeColor',[1 1 0],'MarkerFaceColor',[1 0 0]);
        else
            scatter(data.instances(i,1),data.instances(i,2),a,'MarkerEdgeColor',[0 1 1],'MarkerFaceColor',[0 0 1]);
        end
    end
    
    
    %figure;
    %hold on;
    %scatter(data.instances(:,1),data.instances(:,2),a,'MarkerEdgeColor',[0 .5 .5],'MarkerFaceColor',[0 .7 .7]);

    figure(12);
    axis([0 100 0 100]);
    hold on;
    for i=1:length(CommonHandles.SALT.trainingData.labels);
        if (CommonHandles.SALT.trainingData.labels(i) == 1)
            scatter(CommonHandles.SALT.trainingData.instances(i,1),CommonHandles.SALT.trainingData.instances(i,2),a,'MarkerEdgeColor',[1 1 0],'MarkerFaceColor',[1 0 0]);
        else
            scatter(CommonHandles.SALT.trainingData.instances(i,1),CommonHandles.SALT.trainingData.instances(i,2),a,'MarkerEdgeColor',[0 1 1],'MarkerFaceColor',[0 0 1]);
        end
    end
    
    figure(16);
    axis([0 100 0 100]);
    hold on;
    for i=1:length(testData.instances)
        if (outL(i) == 1)
            scatter(testData.instances(i,1),testData.instances(i,2),a,'MarkerEdgeColor',[1 1 0],'MarkerFaceColor',[1 0 0]);
        else
            scatter(testData.instances(i,1),testData.instances(i,2),a,'MarkerEdgeColor',[0 1 1],'MarkerFaceColor',[0 0 1]);
        end
    end
    
    tn = 0;
    tp = 0;
    fp = 0;
    fn = 0; 
    nofFlags = length(flags);
    f = zeros(nofFlags,1);
    
    for j=1:nofFlags
         f(j,:) = flags(:,j);
    end
    
    for i=1:nofFlags
       if (f(i) == outL(i) && outL(i) == 1)
           tp= tp + 1;
       elseif (f(i) ~= outL(i) && outL(i) == 1)
           fp = fp + 1;
       elseif (f(i) == outL(i) && outL(i) == -1)
           tn = tn + 1;
       elseif (f(i) ~= outL(i) && outL(i) == -1)
           fn = fn + 1;
       end
    end
    
    perc = 100*(tp+tn)/nofFlags;
    precision = tp / (tp+fp);
    recall = tp / (tp+fn);
    fprintf('Number of labeled (synthetic) data: %i \nPercentage of good labels: %0.2f%% \n',nofFlags,perc);
    fprintf('FP: %i \nTP: %i \nTN: %i \nFN: %i \n',fp,tp,tn,fn);
    fprintf('Precision: %f \nRecall: %f\n',precision,recall);
    
end

