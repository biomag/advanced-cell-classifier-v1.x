function results = simpleGauss(testCase)
%SIMPLEGAUSS Summary of this function goes here
%   Detailed explanation goes here
    
        global CommonHandles;

        % these variables must be set/check before every run
        % and also generateClassifiedDataForSAC and OCCconvertDataToAcc
        maxDimension = 40;
        dimensionStepSize = 10;
        maxObjects = 2000;
        objectsStepSize = 1000;

        maxDimensionForResults = maxDimension / dimensionStepSize;
        maxObjectsForResults = maxObjects / objectsStepSize;
        results = zeros(maxDimensionForResults, maxObjectsForResults);

        AD = OCCArtificialCellsData(999);

        for i=10:dimensionStepSize:maxDimension
            for j=1000:objectsStepSize:maxObjects
                numberOfObjectsForTraining = j;
                AD.OCCconvertDataToACC(9,'myPlate',i,j);
                cd(['Utils' filesep 'OneClassClassifier' filesep 'SyntheticData' filesep 'datasets' filesep 'myPlate-fd' num2str(i) 'trObj' num2str(j)]);
                features = OCCLoadAllFeatures(CommonHandles,'myPlate');        
                flags = importdata([pwd filesep 'myPlate' filesep 'anal2' filesep 'flags']);
                cd(['..' filesep '..' filesep '..' filesep '..' filesep '..']);

                CommonHandles.SALT.trainingData = [];
                CommonHandles.SALT.trainingData = generateClassifiedDataForSAC(i, numberOfObjectsForTraining, testCase);
                CommonHandles.SALT.model = sacTrain(CommonHandles.ClassifierNames{CommonHandles.SelectedClassifier},CommonHandles.SALT.trainingData);

                disp('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*');
                fprintf('Feature Dimension: %i \n',i);
                percentageOfHit = createPrediction(features, flags);
                fprintf('Training objects numbers (only target objects): %i \n',numberOfObjectsForTraining);
                disp('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*');
                flags = 0;
                features = 0;

                results(i/dimensionStepSize,j/objectsStepSize) = percentageOfHit;
            end
        end

end

