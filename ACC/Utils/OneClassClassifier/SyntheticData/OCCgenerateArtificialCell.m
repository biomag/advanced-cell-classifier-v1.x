function [ cells, image ] = OCCgenerateArtificialCell( nofSamples )
% SPECIFIC MODIFICATION OF generateArtificialCell
% especially for OneClassClassifier
% INPUT:
%   nofSamples - number of artificial cells/circles
%
% OUTPUT:
%   cells - size and color of cells
%   image - image

    cells = cell(1,nofSamples);
    image = cell(1,nofSamples);

    for i=1:nofSamples
        a = rand();
        b = rand();

        cColorR = round(rand);
        cColorG = round(rand); 
        cColorB = 1;
        
        alfa = linspace(0,2*pi,300);        
        h = fill(a*cos(alfa),b*sin(alfa),[cColorR, cColorG, cColorB]);
        
        hold on;

        F = getframe(gcf);
        image{i} = frame2im(F);    

        hold off;

        if a ~= 0
            cells{i}.cytoA = a;
        else
            cells{i}.cytoA = 0.5;
        end
        cells{i}.cytoB = b;
        cells{i}.cytoColorR = cColorR;
        cells{i}.cytoColorG = cColorG;
        cells{i}.cytoColorB = cColorB;

    end

end

