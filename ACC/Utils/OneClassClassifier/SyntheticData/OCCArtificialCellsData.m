classdef OCCArtificialCellsData < Data
% SPECIFIC MODIFICATION OF ArtificialCellsData
% especially for OneClassClassifier
%
% calcFeatures - this func generates the attributes and the fixed flags
%                 with labels
%
% OCCconvertDataToACC - converts the generated data to separated files and
%                       folders which can be useable by the ACC


    properties
        %cellData is a huge cell Array every cell in it contains a
        %structure which describe one artificial cell, such as the minor
        %and major axes, positions, colors etc.
        cellData

        %stores the targets for our cells. If there are multiple targets,
        %then they are stored as columns.
        targets

        %The constructor function also stores images of the generated
        %cells.
        images                
    end

    methods
        %Constructor
        function obj = OCCArtificialCellsData(nofSamples)
            [obj.cellData, obj.images] = OCCgenerateArtificialCell(nofSamples);            

            obj.sizeOfLabelingPool = nofSamples;
        end

        %We put the new information into the data.
        function obj = label(obj,actualCell, target)

            obj.targets(:,actualCell)=target; 
            if (~ismember(actualCell,obj.realLabels))
                obj.realLabels = [obj.realLabels, actualCell];  
            end
        end

        %Getting labels
        function [inputs, targets] = getLabels(obj, specialization)            
            if nargin>1
                if isequal(sort(convert2RowVector(intersect(specialization,obj.realLabels))),sort(convert2RowVector(specialization)))
                    selectedCells = convert2ColumnVector(specialization);                
                else
                    baseException = MException('MYEXC:nolabeledRequsted','Sorry not all the requested cells are labeled. Please check realLabels property.');
                    throw(baseException);   
                end
            elseif ~isequal(obj.realLabels,[])
                selectedCells = convert2ColumnVector(obj.realLabels);
            else
                baseException = MException('MYEXC:nolabeledany','Sorry there is no any labeled cells yet, so we cannot provide the any labels.');
                throw(baseException);
            end

            [inputs, ~] = obj.calcFeatures(selectedCells);
            targets = obj.targets(:,selectedCells);

        end

        %getData
        function [input, img] = getData(obj,specialization)
            if nargin>1
                specialization = convert2ColumnVector(specialization);
            else
                specialization = convert2ColumnVector(1:obj.sizeOfLabelingPool);
            end

            input = obj.calcFeatures(specialization);

            for i=1:length(specialization)
                img{i} = obj.images{i};
            end
        end

        % ************************************             
        % here we can MODIFY what to consider as the TARGET, and
        % also what we consider as input.

        % **************************************     
        function [inputs, targets, flags] = calcFeatures(obj, selectedCells, featureDimension)
            inputs = zeros(featureDimension,length(selectedCells));
            targets = zeros(2,length(selectedCells));
            flags = zeros(length(selectedCells),1);

            for i=1:length(selectedCells)
                if (obj.cellData{selectedCells(i)}.cytoColorR && obj.cellData{selectedCells(i)}.cytoColorG)
                    for j=2:featureDimension
                        inputs(j,i) = rand();                        
                        inputs(1,i) = normrnd(5,2);
                        %inputs(10,i) = normrnd(5,2);%rand();
                        flags(i) = 1;
                    end                
                elseif (obj.cellData{selectedCells(i)}.cytoColorR && ~obj.cellData{selectedCells(i)}.cytoColorG)
                    for j=2:featureDimension
                        inputs(j,i) = rand();                         
                        inputs(1,i) = normrnd(15,2);                       
                        %inputs(10,i) = normrnd(15,2);%rand();
                        flags(i) = -1;
                    end
                elseif (~obj.cellData{selectedCells(i)}.cytoColorR && obj.cellData{selectedCells(i)}.cytoColorG)
                    for j=2:featureDimension
                        inputs(j,i) = rand();                        
                        inputs(1,i) = normrnd(25,2);
                        %inputs(10,i) = normrnd(25,2);%rand();
                        flags(i) = 1;
                    end
                elseif (~obj.cellData{selectedCells(i)}.cytoColorR && ~obj.cellData{selectedCells(i)}.cytoColorG)
                    for j=2:featureDimension
                        inputs(j,i) = rand();                       
                        inputs(1,i) = normrnd(35,2);
                        %inputs(10,i) = normrnd(35,2);%rand();
                        flags(i) = -1;
                    end
                end
            end
        end


        %plot data to regression plane
        %this function helps to visualize the results of the regression
        %process. It gets the inputs, and targets and based on that it
        %draws out the regression plane.
        function plotDataToRegressionPlane(obj,indeces, targets)
            alfa = linspace(0,2*pi,300);
            zooming = 30;            
            for i=1:length(indeces)
                a = 0.5;
                b = 0.5;
                cColorR = obj.cellData{indeces(i)}.cytoColorR;
                cColorG = obj.cellData{indeces(i)}.cytoColorG;
                cColorB = obj.cellData{indeces(i)}.cytoColorB;
                fill(zooming.*a*cos(alfa)+targets(1,i),zooming.*b*sin(alfa)+targets(2,i),[cColorR, cColorG, cColorB]);
                hold on;
            end            
        end

        %We convert our artificial data to ACC format.
        % ACC format:
        %   - the data is found in 3 folders usually called anal1 anal2 and
        %   anal3.
        %       In anal1 there are the segmented images.
        %       In anal3 there are the raw images.
        %       In anal2 there are text files describing the features for
        %       each image.
        %     This function does not make difference between the raw and
        %     segmented images. We use again the input calculator function.
        % In ACC format on one images usually you can find more than one
        % object. We'll put 9 objects on each image.
        % 
        % INPUTS:
        %        - nofObjectsOnOnePicture should be a square number!
        %        - plateName - how you call your plate - string
        function OCCconvertDataToACC(obj,nofObjectsOnOnePicture,plateName,featureDimension, numberOfTrainingObjects)

            %some index arrays for plot
            [A,B] = meshgrid(50:100:50+(sqrt(nofObjectsOnOnePicture)-1)*100);
            A = reshape(A,1,nofObjectsOnOnePicture);
            B = reshape(B,1,nofObjectsOnOnePicture);
            cd(['Utils' filesep 'OneClassClassifier' filesep 'SyntheticData']);
            mkdir('datasets');
            cd('datasets');
            mkdir(['myPlate-fd' num2str(featureDimension) 'trObj' num2str(numberOfTrainingObjects)]);
            cd(['myPlate-fd' num2str(featureDimension) 'trObj' num2str(numberOfTrainingObjects)]);
            mkdir(plateName);
            cd(plateName);
            mkdir('anal1');
            mkdir('anal2');
            mkdir('anal3');
            whitebg('k');
            dataTotal = [];


           for i=1:ceil(obj.sizeOfLabelingPool/nofObjectsOnOnePicture)               
                file = fopen(['anal2' filesep plateName '-' num2str(i,'%04d') '.txt'],'w');hold off;
                h = gcf();
                f = get(h,'CurrentAxes');
                F = getframe(f);
                I = frame2im(F);
                imwrite(I,['anal1' filesep plateName '-' num2str(i,'%04d') '.png']);
                imwrite(I,['anal3' filesep plateName '-' num2str(i,'%04d') '.png']);
                [inputs,~,flags] = obj.calcFeatures([(i-1)*nofObjectsOnOnePicture+1 : min(i*nofObjectsOnOnePicture,obj.sizeOfLabelingPool)],featureDimension);
                for j=1:size(inputs,2)
                   fprintf(file,'%f %f ',A(j)/300*435,(B(size(inputs,2)-j+1))/300*344);
                   for k=1:size(inputs,1)
                       fprintf(file,'%f ', inputs(k,j));
                   end
                   fprintf(file,'\n');
                end
                fclose(file);                
                dataTotal = [dataTotal; flags];
                csvwrite(['anal2' filesep 'flags'],dataTotal);
           end
           flags = 0;
           cd('..');       
           cd('..');       
           cd('..');           
           cd('..');       
           cd('..');       
           cd('..');
        end

    end

end

