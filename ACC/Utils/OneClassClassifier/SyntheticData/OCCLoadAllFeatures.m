function features = OCCLoadAllFeatures( CommonHandles, selectedPlate )
% SPECIFIC MODIFICATION OF LoadAllFeatures
% especially for OneClassClassifier
% INPUT:
%   CommonHandles - as usual
%   selectedPlate - the plate name what is given to OCCConvertDataToAcc
%                   in oneClassClassifier.m (usually 'myPlate)'
%
% OUTPUT:
%   features - every feature (rows = objects, columns = attributes)
    k=1;    
    ImageList = dir([pwd filesep selectedPlate filesep CommonHandles.ImageFolder filesep '*' CommonHandles.ImageExtension]);                        
    possibleImages = 1:length(ImageList);
     for i=1:ceil(length(ImageList))
       currentImageName = ImageList(possibleImages(i)).name;
       SelectedImageName = [pwd filesep selectedPlate filesep CommonHandles.ImageFolder filesep currentImageName];
       [~, fileNameexEx, ~, ~] = filepartsCustom(SelectedImageName);
       SelectedMetaDataFileName = [pwd filesep selectedPlate filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.txt'];        
       SelectedMetaData = load(SelectedMetaDataFileName);           
       metaData = SelectedMetaData;
       if (~exist('features','var')) && size(metaData,2)>1
            %preallocate space
            features = zeros(ceil(length(ImageList)*size(metaData,1)),size(metaData,2)-2);
        end
        for j=1:size(metaData,1)                        
            if size(metaData,2)>1
                features(k,:) = metaData(j,3:end);                           
                k=k+1;
            end
        end
     end

    features(k:end,:) = [];        
end



