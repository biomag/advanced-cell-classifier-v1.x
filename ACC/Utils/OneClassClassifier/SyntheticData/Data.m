classdef (Abstract) Data
% the abstract of OCCArtificialCellsData

    %DATA Interface for all data source we use in our AL strategies
    %   The Data class is abstract, it much more like an interface in fact.
    %   All the methods in it must be implemented for different data
    %   sources just like: synthetic data, or CommonHandles
    %       What we require from 'data' is:
    %          1. It must have a large labeling pool, from which we can
    %          choose objects to predict or to show it to the oracle.
    %          2. All the objects must be identified with a unique number
    %          with which we can make references to each objects.
    
    properties
        sizeOfLabelingPool
        % RealLabels. The already labeled cells. This is an array with the
        % indeces of the cells for which we have labels.    
        realLabels                
    end
    
    methods (Abstract)
        % This function is for labeling. This is with which we extend our
        % information, we put new labels into the data. Index is the index
        % of the cell you intend to label and targets are the corresponding
        % targets.
        obj = label(obj,index,targets)
        %Get the already labeled objects data: the input and the targets.
        %We can filter the result data with specialization array which
        %contains indeces. We only give back the labeled objects which are
        %in the specialization array. Only labeled data can be queried
        %here.
        [input, target] = getLabels(obj,specialization)
        %Get input data from the labeling pool. With specialization you can
        %filter from it.
        [input, img] = getData(obj, specialization)
    end
    
end

