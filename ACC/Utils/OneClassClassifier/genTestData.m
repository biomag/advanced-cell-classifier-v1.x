function testData = genTestData( numberOfGeneratedObjects, numberOfFeatures , testCase, useNoise, db )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    testData.instances = zeros(numberOfGeneratedObjects,2);
    testData.labels = zeros(1,numberOfGeneratedObjects);
    for i=1:numberOfGeneratedObjects
        
        if ~strcmp(testCase,'bubbles')
            testData.instances(i,1) = 100 * rand();
            testData.instances(i,2) = 100 * rand();
        end
            
        %this is for uniform-random noise
        
        z = (sqrt(28.87^2 / (10^(db/10)))) / 0.2887;
        
        if(useNoise == 1)
            for j=3:numberOfFeatures
                testData.instances(i, j) = z * rand();
            end
        end
           
        if(strcmp(testCase,'squareRing_1'))
            if (testData.instances(i,1) < 20 || testData.instances(i,1) > 80 || testData.instances(i,2) < 20 || testData.instances(i,2) > 80)
                testData.labels(i) = -1;
            elseif ((testData.instances(i,1) > 30 && testData.instances(i,2) > 30) && (testData.instances(i,1) > 30 && testData.instances(i,2) < 70) && (testData.instances(i,1) < 70 && testData.instances(i,2) > 30) && ( testData.instances(i,1) < 70 && testData.instances(i,2) < 70))
                testData.labels(i) = -1;
            else
                testData.labels(i) = 1;
            end
        elseif(strcmp(testCase,'squareRing_2'))
            if (testData.instances(i,1) < 20 || testData.instances(i,1) > 80 || testData.instances(i,2) < 20 || testData.instances(i,2) > 80)
                testData.labels(i) = 1;
            elseif ((testData.instances(i,1) > 30 && testData.instances(i,2) > 30) && (testData.instances(i,1) > 30 && testData.instances(i,2) < 70) && (testData.instances(i,1) < 70 && testData.instances(i,2) > 30) && ( testData.instances(i,1) < 70 && testData.instances(i,2) < 70))
                testData.labels(i) = 1;
            else
                testData.labels(i) = -1;
            end
        elseif(strcmp(testCase,'sinFunc'))
            if (testData.instances(i,2) < (10*sin(testData.instances(i,1)/10)+50))
                testData.labels(i) = 1;
            else
                testData.labels(i) = -1;
            end
        elseif(strcmp(testCase,'ellipses_1'))
            if i <= numberOfGeneratedObjects/3
                x0 = 20;
                y0 = 45;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 6.3);
                [testData.instances(i,1), testData.instances(i,2)] = rotmat(x0,y0,x,y,theta);
                testData.labels(i) = 1;
                
            elseif (i>numberOfGeneratedObjects/3 && i <= numberOfGeneratedObjects*2/3)
                x0 = 30;
                y0 = 35;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 4);
                [testData.instances(i,1), testData.instances(i,2)] = rotmat(x0,y0,x,y,theta);
                testData.labels(i) = -1;
            else
                x0 = 40;
                y0 = 20;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 2.5);
                [testData.instances(i,1), testData.instances(i,2)] = rotmat(x0,y0,x,y,theta);
                testData.labels(i) = 1;
            end
        elseif(strcmp(testCase,'ellipses_2'))
                        
             if i <= numberOfGeneratedObjects/3
                x0 = 20;
                y0 = 45;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 6.3);
                [testData.instances(i,1), testData.instances(i,2)] = rotmat(x0,y0,x,y,theta);
                testData.labels(i) = 1;
                
            elseif (i>numberOfGeneratedObjects/3 && i <= numberOfGeneratedObjects*2/3)
                x0 = 30;
                y0 = 35;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 4);
                [testData.instances(i,1), testData.instances(i,2)] = rotmat(x0,y0,x,y,theta);
                testData.labels(i) = 1;
            else
                x0 = 40;
                y0 = 20;
                x = normrnd(x0,2);
                y = normrnd(y0,8);
                theta = -(pi / 2.5);
                [testData.instances(i,1), testData.instances(i,2)] = rotmat(x0,y0,x,y,theta);
                testData.labels(i) = -1;
            end
        elseif(strcmp(testCase,'bubbles'))
            if i <= numberOfGeneratedObjects/3
                for j=1:numberOfFeatures
                    testData.instances(i,j) = normrnd(75,2);
                end
                testData.labels(i) = 1;
            elseif (i>numberOfGeneratedObjects/3 && i <= numberOfGeneratedObjects*2/3)
                for j=1:numberOfFeatures
                    testData.instances(i,j) = normrnd(50,2);
                end
                testData.labels(i) = -1;
            else
                for j=1:numberOfFeatures
                    testData.instances(i,j) = normrnd(25,2);
                end
                testData.labels(i) = 1;
            end
        end
    end
    

end

