function [] = regressionClassChecker()
% checks if a class type is Regression

global CommonHandles;

actualClass = length(CommonHandles.Classes);
for i=1:actualClass
    type = CommonHandles.Classes{i}.Type;
    if (strcmp(type,'Regression'))            
        CommonHandles.RegressionClass(actualClass) = 1;                   
        CommonHandles.LastRegressionPlane{actualClass} = zeros(1100, 1100, 3);
    else            
        CommonHandles.RegressionClass(actualClass) = 0;            
    end
end

end

