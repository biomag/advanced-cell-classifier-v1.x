%jumpGuiToSelectedCell. This function refreshes the ACC_main_GUI according
%to the selected image name and cell number in CommonHandles.
%   INPUTS: CommonHandles.
%           flag: which information of the selected image do we want to use
%           for the jump. Possible values:
%               'NAME': we use CommonHandles.SelectedImageName for the jump
%               'NUMBER': we use CommonHandles.SelectedImage: this is the
%               index for the selected image in the image list.
%TODO: It does not handle the plate changes. (This function is used after
%AC.proposeNextCell function, which changes the selectedImageName field of
%CommonHandles, but the selectedImage is not necessarily in the
%selectedPlate)
function CommonHandles = jumpGuiToSelectedCell(CommonHandles,flag)
    ImageList = get(CommonHandles.ImageListHandle,'String');          
    if strcmp(flag,'NUMBER')
        CommonHandles.SelectedImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
        splitted = strsplit(CommonHandles.SelectedImageName,filesep);
        %splittedSelectedImageName
        SSIN = splitted{end};
    elseif strcmp(flag,'NAME')
        splitted = strsplit(CommonHandles.SelectedImageName,filesep);
        %splittedSelectedImageName
        SSIN = splitted{end};
        for i=1:length(ImageList)
            if strcmp(ImageList{i},SSIN)
                CommonHandles.SelectedImage = i;
                break;
            end
        end
    end                            
    
    %reset the image list selection
    set(CommonHandles.ImageListHandle, 'Value', CommonHandles.SelectedImage);
    %select original image too
    CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep SSIN];
    CommonHandles = LoadSelectedImage(CommonHandles);