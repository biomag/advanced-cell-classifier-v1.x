function [ selectedMethod, selectedMetric ] = testingForBestCluster( features, classes, showResultTable )
%TESTINGFORBESTCLUSTER Summary of this function goes here
%   this fcn tests which is the best linkage set for the current dataset
%   showResultTable [0,1] - for showing the table at the end


    % TODO
    % remove warnings!
    method = {'average','complete','single','weighted','centroid','median','ward'};
    metric = {'euclidean','seuclidean','cosine','correlation','chebychev','minkowski','spearman','cityblock'};

    y1 = length(method);
    y2 = length(metric);
    matrix = zeros(y1,y2);
    min = 9999;
    
    for i=1:length(features)
        trainingFeatures(i,:) = features{i,1};
    end
    
    for k=1:length(classes)
        for l=1:length(classes{1,1})
            if classes{k,1}(l) == 1;
                textData{k} = num2str(l);
            end
        end
    end
   
    for i=1:length(method)
        for j=1:length(metric)
            x = 0;
            tree = linkage(trainingFeatures,method(i),metric(j));
            
            %if i==5 && j==8
            %    fig = figure();
            %else
            fig = figure('visible','off');%end
            %[~,~,orig] = dendrogram(tree,0,'Labels',textData);
            [~,~,orig] = dendrogram(tree,0);
            
            for k=1:length(orig)
                t = orig(k);
                labelOrder(k) = textData(t);
            end
            
            for k=1:(length(labelOrder)-1)
                if (labelOrder{k} ~= labelOrder{k+1})
                    matrix(i,j) = matrix(i,j) + 1;
                end
            end
        end
    end
       
    for i=1:size(matrix,1)
        for j=1:size(matrix,2)
            if matrix(i,j) < min
                min = matrix(i,j);
                selectedMethod = method{i};
                selectedMetric = metric{j};
            else 
            end
        end
    end
    
    if showResultTable
        figure();
        hold on;
        imagesc(matrix);
        colormap(flipud(gray));
        [a,b] = meshgrid(1:y2,1:y1);

        textStrings = num2str(matrix(:),'%0.f');
        textStrings = strtrim(cellstr(textStrings));

        hStrings = text(a(:),b(:),textStrings(:), 'HorizontalAlignment','center');
        midValue = mean(get(gca,'CLim'));
        textColors = repmat(matrix(:) > midValue,1,3);

        set(hStrings,{'Color'},num2cell(textColors,2));
        set(gca,'XTick',1:y2,'XTickLabel',metric,'YTick',1:y1,'YTickLabel',method,'TickLength',[0 0]);
    end
end

