function [outimg, className, mapping] = createClassImage( counter )
%CREATECLASSIMAGE Summary of this function goes here
%   Detailed explanation goes here
    global CommonHandles;

    imgsize = CommonHandles.ClassImageInfo.ImageSize;
    sepsize = CommonHandles.ClassImageInfo.SepSize;
    cols = CommonHandles.ClassImageInfo.Cols;
    width = cols * imgsize(1) + (cols+1) * sepsize;

    className = CommonHandles.Classes{counter}.Name;
    classCells = CommonHandles.ClassImages(className).Images;
    
    numimgs = size(classCells,2);
    rows = ceil(numimgs / cols);
    height = rows * imgsize(2) + (rows+1) * sepsize;
    outimg = uint8(zeros(height,width,3));
    outimg(:,:,:) = 255;
    
    mapping = {};
    x = 1;
    y = 1;
    for i = 1:numimgs
        classImg = classCells{i};
        resImg = imresize(classImg.Image, imgsize);
        resImg = setIconImageIntensity(CommonHandles,resImg);
        if x > cols
           x = 1;
           y = y + 1;
        end
        offsetw = imgsize(1) * (x-1) + sepsize * x;
        offseth = imgsize(2) * (y-1) + sepsize * y;
        outimg(offseth:offseth+imgsize(2)-1,offsetw:offsetw+imgsize(1)-1,:) = resImg;
        subimgmeta.CellNumber = classImg.CellNumber;
        subimgmeta.ImageName = classImg.ImageName;
        mapping{y,x} = subimgmeta;
        x = x + 1;
    end    
end