function recoverGUI( handles,filename)
%recoverGUI The CommonHandles contains all the neccessary data to reload a
%previous state of ACC. However just by loading in CommonHandles it won't
%set back the GUI elements to their states. (like state of Pushtools, size
%of window etc.) This function does these state resets.

    global CommonHandles;

    if (CommonHandles.ActiveLearning)
        set(handles.cleverGuessToggletool,'State','on');
    else
        set(handles.cleverGuessToggletool,'State','off');
    end
    if (CommonHandles.RandomTrain)
        set(handles.uitoggletool2,'State','on');
    else 
        set(handles.uitoggletool2,'State','off');
    end
    set(handles.checkbox1, 'Value', CommonHandles.ColorView);
    set(handles.checkbox2, 'Value', CommonHandles.ShowContour);
    set(handles.figure1, 'Name', ['Advanced Cell Classifier - ' filename] );
    
end

