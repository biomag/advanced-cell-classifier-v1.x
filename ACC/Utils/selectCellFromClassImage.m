function [ selimgInfo, hit, xcoord, ycoord ] = selectCellFromClassImage( x, y, mapping )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
global CommonHandles;

imgxsize = CommonHandles.ClassImageInfo.ImageSize(1);
imgysize = CommonHandles.ClassImageInfo.ImageSize(2);
sepsize = CommonHandles.ClassImageInfo.SepSize;
cols = CommonHandles.ClassImageInfo.Cols;
xcoord = x - sepsize;
ycoord = y - sepsize;

xcoord = floor(xcoord / (imgxsize+sepsize)) + 1;
ycoord = floor(ycoord / (imgysize+sepsize)) + 1;

% Check that click is over some cell image
if x >= ((xcoord-1) * imgxsize + xcoord * sepsize) && x <= ((xcoord) * imgxsize + xcoord * sepsize) && ...
   y >= ((ycoord-1) * imgysize + ycoord * sepsize) && y <= ((ycoord) * imgysize + ycoord * sepsize) && ...
   size(mapping,1) >= ycoord && size(mapping,2) >= xcoord
    selimgInfo = mapping{ycoord,xcoord};
    hit = 1;
else
    selimgInfo = {};
    hit = 0;
end

end
