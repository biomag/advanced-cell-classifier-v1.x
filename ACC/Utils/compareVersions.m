%returns false if the second string is alphabetically smaller than the
%first one. It is true if V1 =< V2.
function b = compareVersions(V1,V2)
    S = {V1,V2};
    S = sort(S);
    b = strcmp(S{1},V1);
end

