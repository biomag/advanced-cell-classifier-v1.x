function [] = refreshClassImage( counter, scrollpanel )
%REFRESHCLASSIMAGE Summary of this function goes here
%   Detailed explanation goes here

    [img,~,~] = createClassImage(counter);
    api = iptgetapi(scrollpanel);
    api.replaceImage(img);
    api.setVisibleLocation([0 0]);
end

