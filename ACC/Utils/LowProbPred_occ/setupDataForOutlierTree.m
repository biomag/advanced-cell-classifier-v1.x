function reducedSamples = setupDataForOutlierTree()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
global CommonHandles;

CommonHandles.OCC.LowProb = {};
cutting = {};

data = convertACC2SALT(CommonHandles);
convertInstances(data);

everyCell = CommonHandles.AllFeatures;
mapping = CommonHandles.AllFeaturesMapping;
coords = CommonHandles.AllCoordinates;

j = size(everyCell,2);
cutSample = zeros(1,j);
%{
for i=1:size(everyCell,1)
    indx = find(everyCell(i,:)==cutSample);
    if indx>0
        cutting{end+1} = i;
    end
end

for i=length(cutting):-1:1
   everyCell(cutting{i},:) = [];
   mapping{cutting{i}} = [];
end

mapping = mapping(~cellfun('isempty',mapping));
%}
everyCellTemp = zeros(size(everyCell,1),size(everyCell,2));
coordsTemp = zeros(size(coords,1),size(coords,2));
k=1;
for i=1:size(everyCell,1)
    if everyCell(i,:) ~= cutSample
        everyCellTemp(k,:) = everyCell(i,:);
        mappingTemp{k} = mapping{i};
        coordsTemp(k,:) = coords(i,:);
        k=k+1;
    end
end
everyCellTemp(k:end,:) = [];
mappingTemp(k:end) = [];
coordsTemp(k:end,:) = [];

reducedSamples = everyCellTemp(:,CommonHandles.HC.Default.InfoGainAttributes);
CommonHandles.AllFeatures = everyCellTemp;
CommonHandles.AllFeaturesMapping = mappingTemp;
CommonHandles.AllCoordinates = coordsTemp;

for i=1:size(data.instances,1)
    for j=1:length(CommonHandles.HC.Default.InfoGainAttributes)
       reducedTrainingSamples(i,j) = data.instances(i,CommonHandles.HC.Default.InfoGainAttributes(j));
    end
end

CommonHandles.SALT.model = svmtrain(data.labels', reducedTrainingSamples, CommonHandles.HC.Default.TrainParams);

[~, ~ ,props] = svmpredict(ones(size(reducedSamples,1),1), reducedSamples, CommonHandles.SALT.model);

[~,index] = sort(props);

if CommonHandles.HC.Default.NumberOfOutliers == 0
    CommonHandles.HC.Default.NumberOfOutliers = size(reducedSamples,1);
end

CommonHandles.OCC.Dendogram.selectedFeatures = zeros(CommonHandles.HC.Default.NumberOfOutliers,size(CommonHandles.SelectedMetaData,2)-2);

sortedIndex = sort(index(1:CommonHandles.HC.Default.NumberOfOutliers));

for i=1:CommonHandles.HC.Default.NumberOfOutliers
    currentCell = sortedIndex(i);
    if i~=1 && i<=CommonHandles.HC.Default.NumberOfOutliers-1 && ~strcmp(mapping{currentCell}.ImageName,mapping{sortedIndex(i+1)}.ImageName)
        CommonHandles.SelectedCell = mapping{currentCell}.CellNumberInImage;
        CommonHandles = showTheCell(CommonHandles);
        CommonHandles.OCC.LowProb.img{i} = CommonHandles.CurrentSelectedCell;
        CommonHandles.OCC.LowProb.featureVector(i,:) = everyCell(currentCell,:);
        CommonHandles.OCC.LowProb.imageName{i} = mapping{currentCell}.ImageName;
        CommonHandles.OCC.LowProb.selectedCell{i} = mapping{currentCell}.CellNumberInImage;
        CommonHandles.OCC.LowProb.plateName{i} = mapping{currentCell}.CurrentPlateNumber;
        CommonHandles.OCC.Dendogram.selectedFeatures(i,:) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end);
        
        CommonHandles.SelectedImageName = mapping{sortedIndex(i+1)}.ImageName;
        CommonHandles.SelectedOriginalImageName = mapping{sortedIndex(i+1)}.ImageName;
        CommonHandles = loadImageForLowProb(CommonHandles, mapping{sortedIndex(i+1)}.CurrentPlateNumber);            
        [~, fileNameexEx, ~, ~] = filepartsCustom(CommonHandles.SelectedImageName);
        [~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, mapping{sortedIndex(i+1)}.CurrentPlateNumber);
    elseif i==CommonHandles.HC.Default.NumberOfOutliers || (i<=CommonHandles.HC.Default.NumberOfOutliers-1 && i>1 && strcmp(mapping{currentCell}.ImageName,mapping{sortedIndex(i+1)}.ImageName))
        CommonHandles.SelectedCell = mapping{currentCell}.CellNumberInImage;
        CommonHandles = showTheCell(CommonHandles);
        CommonHandles.OCC.LowProb.img{i} = CommonHandles.CurrentSelectedCell;
        CommonHandles.OCC.LowProb.featureVector(i,:) = everyCell(currentCell,:);
        CommonHandles.OCC.LowProb.imageName{i} = mapping{currentCell}.ImageName;
        CommonHandles.OCC.LowProb.selectedCell{i} = mapping{currentCell}.CellNumberInImage;
        CommonHandles.OCC.LowProb.plateName{i} = mapping{currentCell}.CurrentPlateNumber;
        CommonHandles.OCC.Dendogram.selectedFeatures(i,:) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end);
    elseif i==1
        CommonHandles.SelectedImageName = mapping{currentCell}.ImageName;
        CommonHandles.SelectedOriginalImageName = mapping{currentCell}.ImageName;
        CommonHandles = loadImageForLowProb(CommonHandles, mapping{currentCell}.CurrentPlateNumber);            
        [~, fileNameexEx, ~, ~] = filepartsCustom(CommonHandles.SelectedImageName);
        [~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, mapping{currentCell}.CurrentPlateNumber);
        CommonHandles.SelectedCell = mapping{currentCell}.CellNumberInImage;
        CommonHandles = showTheCell(CommonHandles);
        CommonHandles.OCC.LowProb.img{i} = CommonHandles.CurrentSelectedCell;
        CommonHandles.OCC.LowProb.featureVector(i,:) = everyCell(currentCell,:);
        CommonHandles.OCC.LowProb.imageName{i} = mapping{currentCell}.ImageName;
        CommonHandles.OCC.LowProb.selectedCell{i} = mapping{currentCell}.CellNumberInImage;
        CommonHandles.OCC.LowProb.plateName{i} = mapping{currentCell}.CurrentPlateNumber;
        CommonHandles.OCC.Dendogram.selectedFeatures(i,:) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end);
        
        if ~strcmp(mapping{currentCell}.ImageName,mapping{sortedIndex(2)}.ImageName)
            CommonHandles.SelectedImageName = mapping{sortedIndex(i+1)}.ImageName;
            CommonHandles.SelectedOriginalImageName = mapping{sortedIndex(i+1)}.ImageName;
            CommonHandles = loadImageForLowProb(CommonHandles, mapping{currentCell}.CurrentPlateNumber);            
            [~, fileNameexEx, ~, ~] = filepartsCustom(CommonHandles.SelectedImageName);
            [~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, mapping{sortedIndex(i+1)}.CurrentPlateNumber);
        end
    else
        disp('Something is wrong, please check it!');
    end
end

end

