function CommonHandles = showTheCell( CommonHandles )
%SHOWTHECELL Summary of this function goes here
%   Detailed explanation goes here

if (CommonHandles.ShowContour)
    imageToShow = CommonHandles.CurrentImage;
else
    imageToShow = CommonHandles.CurrentOriginalImage;
end;

cutSize = 50;

imageToShow(:, :, 1) = imageToShow(:, :, 1) * CommonHandles.MainView.Red;
imageToShow(:, :, 2) = imageToShow(:, :, 2) * CommonHandles.MainView.Green;
imageToShow(:, :, 3) = imageToShow(:, :, 3) * CommonHandles.MainView.Blue;

if ~CommonHandles.ColorView
    
    imageToShow = max(imageToShow,[], 3);
    
end

% cut and display visible part of the original image after zooming
%showedImage = imageToShow(CommonHandles.CurrentImageSizeX-int16(round(CommonHandles.MainView.YBounds(2)))+1:CommonHandles.CurrentImageSizeX-int16(round(CommonHandles.MainView.YBounds(1)+1)), int16(round(CommonHandles.MainView.XBounds(1))):int16(round(CommonHandles.MainView.XBounds(2))), :);

% TODO we should not change small icon in case of zooming, only by clicking

% boundaries of the part of original image containing current cell for small view
if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1) < cutSize+1
    cutx1=1;
else
    cutx1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)-cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2) < cutSize+1
    cuty1=1;
else
    cuty1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)-cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1) > (CommonHandles.CurrentImageSizeY -cutSize)
    cutx2=CommonHandles.CurrentImageSizeY;
else
    cutx2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)+cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2) > (CommonHandles.CurrentImageSizeX-cutSize)
    cuty2=CommonHandles.CurrentImageSizeX;
else
    cuty2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)+cutSize);
end

if ~(CommonHandles.ShowContour)
    ViewImage = CommonHandles.CurrentOriginalImage(cuty1:cuty2, cutx1:cutx2, :);
else
    ViewImage = CommonHandles.CurrentImage(cuty1:cuty2, cutx1:cutx2, :);
end

CommonHandles.CurrentSelectedCell = CommonHandles.CurrentOriginalImage(cuty1:cuty2, cutx1:cutx2, :);

rViewImage = ViewImage;
gViewImage = ViewImage;
bViewImage = ViewImage;

if CommonHandles.ColorView
    rViewImage(:,:,2:3) = 0;
    gViewImage(:,:,[1 3]) = 0;
    bViewImage(:,:,1:2) = 0;
else
    rViewImage(:,:,2) = rViewImage(:,:,1); rViewImage(:,:,3) = rViewImage(:,:,1);
    gViewImage(:,:,1) = gViewImage(:,:,2); gViewImage(:,:,3) = gViewImage(:,:,2);
    bViewImage(:,:,1) = bViewImage(:,:,3); bViewImage(:,:,2) = bViewImage(:,:,3);
end;

end

