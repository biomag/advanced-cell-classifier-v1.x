function CommonHandles = loadImageForLowProb( CommonHandles, currPlateNumber )
%LOADIMAGEFORLOWPROB Summary of this function goes here
%   Detailed explanation goes here
[pathstr, fileNameexEx, ext, versn] = filepartsCustom(CommonHandles.SelectedImageName);

[CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, currPlateNumber);

if exist([CommonHandles.DirName filesep CommonHandles.SelectedImageName],'file');
    CommonHandles.CurrentImage = imread([CommonHandles.DirName filesep CommonHandles.SelectedImageName]);
else
    errordlg(['Altough the project folder exists on your computer, the image names might have changed. We cannot find: ',CommonHandles.DirName filesep CommonHandles.SelectedImageName]);
    ME = MException('loadImage:noSuchImage', ...
        'Image %s not found',[CommonHandles.DirName filesep CommonHandles.SelectedImageName]);
    throw(ME)
end
CommonHandles.CurrentImageSizeX = size(CommonHandles.CurrentImage,1);
CommonHandles.CurrentImageSizeY = size(CommonHandles.CurrentImage,2);
CommonHandles = PlotCurrentHistogram(CommonHandles);

% Storing bounds of visible part of the current image
CommonHandles.MainView.XBounds = [1 CommonHandles.CurrentImageSizeY];
CommonHandles.MainView.YBounds = [1 CommonHandles.CurrentImageSizeX];

CommonHandles.CurrentOriginalImageNoTouch = imread([CommonHandles.DirName filesep CommonHandles.SelectedOriginalImageName]);
 
 I = imread([CommonHandles.DirName filesep CommonHandles.SelectedOriginalImageName]);
 if CommonHandles.MainView.StrechImage == 1

     % Needs to be verifyed this saturation value
     tolr = 0.0;
     tolg = 0.001;
     tolb = 0.005;

     sl(:,1) = [0,1]';%stretchlim(I(:,:,1), tolr); %smalls = find(sl(2,:) < 0.05); sl(2,smalls) = 1;
     sl(:,2) = stretchlim(I(:,:,2), tolg);
     sl(:,3) = stretchlim(I(:,:,3), tolb);
     CommonHandles.CurrentOriginalImage = imadjust(I,sl,[]);
 else
     sl(:, 1)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
     sl(:, 2)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
     sl(:, 3)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
     CommonHandles.CurrentOriginalImage = imadjust(I,sl,[]);
 end;


CommonHandles.CurrentPrediction = {};

CommonHandles = setImageIntensity(CommonHandles);

end

