function [] = refreshClassesList()
% this function refreshes the classes
%{   
    * it's called every time when something changes around the buttons
    * Classfields are the number of labels per class
    * 

    TODO
    manualTrain_callback and addSampleToTrainingSet should be separated
%}
global CommonHandles;
offset = 10;
incW = 0;

for i=1:length(CommonHandles.Classbuttons)
    delete(CommonHandles.Classbuttons{1})
    delete(CommonHandles.ClassTextFields{1})
    delete(CommonHandles.ShowTrainedCells{1})
    CommonHandles.Classbuttons(1) = [];
    CommonHandles.ClassTextFields(1) = [];
    CommonHandles.ShowTrainedCells(1) = [];
end

for i=1:size(CommonHandles.Classes,2)
    incW = size(CommonHandles.Classes{i}.Icon,1);
    CommonHandles.Classbuttons{i} = uicontrol('Parent', CommonHandles.MainWindow ,'Style', 'pushbutton', 'String', '', 'CData', CommonHandles.Classes{i}.Icon, 'Position', [offset 15 (incW+5) (incW+5)], 'Callback', {@manualTrain_Callback, i, CommonHandles.MainView}, 'TooltipString', sprintf('Hotkey: %d',i));
    CommonHandles.ClassTextFields{i} = uicontrol('Parent', CommonHandles.MainWindow ,'Style', 'text', 'String', CommonHandles.Classes{i}.LabelNumbers, 'Position', [offset 1 incW+5 12]);
    CommonHandles.ShowTrainedCells{i} = uicontrol('Parent', CommonHandles.MainWindow, 'Style', 'pushbutton', 'String', '', 'Position', [offset+1 3 14 12], 'BackgroundColor', 'blue', 'Callback', {@showCells_Callback, i});
    offset = offset + 5 + incW;
    
    if(strcmp(CommonHandles.Classes{i}.Type,'Child') && strcmp(CommonHandles.Classes{i-1}.Type,'Normal'))
       set(CommonHandles.Classbuttons{i-1},'Enable','off');
       set(CommonHandles.ShowTrainedCells{i-1},'Enable','off');
    end
end

end


function manualTrain_Callback(hObject, eventdata, counter, handles)
% here user clicked to teach
global CommonHandles;

CommonHandles.MaxCluster = length(CommonHandles.Classes); 

% if database is open
if (CommonHandles.Success == 1)
        
        addSampleToTrainingSet(counter);
        
        % skip to the next image
        if CommonHandles.RandomTrain == 0
            
            %if we use active learning
            if (isfield(CommonHandles,'ActiveLearning') && CommonHandles.ActiveLearning)    
                CommonHandles = CommonHandles.AC.proposeNextCellACC(CommonHandles);
                CommonHandles = jumpGuiToSelectedCell(CommonHandles,'NAME');
                
            else            
            %we use passive learning and we jump to the next cell
                %if the next cell is on the selected picture
                if CommonHandles.SelectedCell < size(CommonHandles.SelectedMetaData, 1)
                    CommonHandles.SelectedCell = CommonHandles.SelectedCell + 1;
                    CommonHandles = ShowSelectedCell(CommonHandles);
                else
                %the next cell is on the next picture
                    MaxImage = length(get(CommonHandles.ImageListHandle, 'String'));
                    MaxPlate = length(get(CommonHandles.PlateListHandle, 'String'));
                    if MaxImage > CommonHandles.SelectedImage
                        %jump to the first cell of the next image
                        CommonHandles.SelectedImage = CommonHandles.SelectedImage + 1;
                        CommonHandles = jumpGuiToSelectedCell(CommonHandles,'NUMBER');
                    else
                        % jump next plate, later...
                    end;
                end;
            end;
        elseif CommonHandles.RandomTrain == 1
            %random train
            MaxImage = length(get(CommonHandles.ImageListHandle, 'String'));
            CommonHandles.SelectedImage = ceil(MaxImage*rand);
            set(CommonHandles.ImageListHandle, 'Value', CommonHandles.SelectedImage);
            ImageList = get(CommonHandles.ImageListHandle,'String');
            CommonHandles.SelectedImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
            CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
            % showimage function call
            CommonHandles = LoadSelectedImage(CommonHandles);
        elseif CommonHandles.RandomTrain == 2
            if CommonHandles.RandomData.Counter == length(CommonHandles.RandomData.RandomData.Image)
                rmfield(CommonHandles, 'RandomData');
                msgbox('Randomized training is done. Do not forget to save your data.', 'Training', 'help');
                uipushtool8_ClickedCallback(hObject, eventdata, handles)
                CommonHandles.RandomTrain = 0;
            else
                % jump to the next image and show it
%                set(CommonHandles.MainWindow, 'Name', ['Random training: ' num2str(CommonHandles.RandomData.Counter) '/' num2str(length(CommonHandles.RandomData.RandomData.Image)) ' done...'] );
                disp(['Random training: ' num2str(CommonHandles.RandomData.Counter) '/' num2str(length(CommonHandles.RandomData.RandomData.Image)) ' done...']);
                CommonHandles.SelectedCell = CommonHandles.RandomData.RandomData.Cell(CommonHandles.RandomData.Counter);
                CommonHandles.SelectedImage = CommonHandles.RandomData.RandomData.Image(CommonHandles.RandomData.Counter);
                CommonHandles.SelectedImageName = CommonHandles.RandomData.RandomData.ImageName{CommonHandles.RandomData.Counter};
                CommonHandles.SelectedOriginalImageName = CommonHandles.RandomData.RandomData.OriginalImageName{CommonHandles.RandomData.Counter};                
                CommonHandles.SelectedPlate = CommonHandles.RandomData.RandomData.Plate(CommonHandles.RandomData.Counter);
                set(CommonHandles.PlateListHandle,'Value', CommonHandles.SelectedPlate);
                CommonHandles.RandomData.Counter = CommonHandles.RandomData.Counter + 1;
                CommonHandles = LoadSelectedImage(CommonHandles);
            end;

        end;
    %end;
    
     % display the size of the data set 
    CommonHandles.LabeledInstances = sum(cell2mat(CommonHandles.TrainingSet.Class'), 2)';
    for i = 1: length(CommonHandles.LabeledInstances)
        CommonHandles.Classes{i}.LabelNumbers = CommonHandles.LabeledInstances(i);        
    end

    refreshClassesList();
end;
end

function addSampleToTrainingSet(counter)

    global CommonHandles;

    alreadyInTrainingSet = 0;
    for i=1:length(CommonHandles.TrainingSet.ImageName)
        if (strcmp(CommonHandles.SelectedImageName,CommonHandles.TrainingSet.ImageName{i}) && CommonHandles.SelectedCell == CommonHandles.TrainingSet.CellNumber{i})
            alreadyInTrainingSet = 1;
        end
    end
    
    if ~alreadyInTrainingSet

        CommonHandles.TrainingSet.MetaDataFilename = [CommonHandles.TrainingSet.MetaDataFilename; CommonHandles.SelectedMetaDataFileName];
        CommonHandles.TrainingSet.ImageName = [CommonHandles.TrainingSet.ImageName; CommonHandles.SelectedImageName];
        CommonHandles.TrainingSet.OriginalImageName = [CommonHandles.TrainingSet.ImageName; CommonHandles.SelectedOriginalImageName];
        CommonHandles.TrainingSet.CellNumber = [CommonHandles.TrainingSet.CellNumber; CommonHandles.SelectedCell];
        ClassVector = zeros(CommonHandles.MaxCluster,1);
        ClassVector(counter) = 1;
        CommonHandles.TrainingSet.Class = [CommonHandles.TrainingSet.Class; ClassVector];
        FeatureVector = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 3: size(CommonHandles.SelectedMetaData, 2));
        CommonHandles.TrainingSet.Features = [CommonHandles.TrainingSet.Features; FeatureVector];

        % if regression is assigned to the current image
        if strcmp(CommonHandles.Classes{counter}.Type,'Regression')
            regressionCounter = CommonHandles.Classes{counter}.ID;
            % put the new image to the queue with the features.
            if ((length(CommonHandles.Regression) >= regressionCounter) && (isfield(CommonHandles.Regression{regressionCounter},'Images')))
                CommonHandles.Regression{regressionCounter}.Images{end+1} = CommonHandles.CurrentSelectedCell;                
                CommonHandles.Regression{regressionCounter}.Features{end+1} = FeatureVector;                
            else
                CommonHandles.Regression{regressionCounter}.Images{1} = CommonHandles.CurrentSelectedCell;
                CommonHandles.Regression{regressionCounter}.Features{1} = FeatureVector;                
            end;
            CommonHandles.RegressionLastClass = regressionCounter;            
            uiwait(regressionPlane);
        end;
        
        %check for the existance of Sampling pool
        if isfield(CommonHandles,'SamplingPool')
            idx = getIndexInSamplingPool(CommonHandles, CommonHandles.SelectedImageName,CommonHandles.SelectedCell);
            if idx~=-1
                [~,classLabel] = max(CommonHandles.TrainingSet.Class{end});
                CommonHandles.SamplingPool.ClassLabels(idx) = classLabel;
            else
                CommonHandles.SamplingPool.Features(end+1,:) = CommonHandles.TrainingSet.Features{end};
                CommonHandles.SamplingPool.Mapping{end+1}.ImageName = CommonHandles.TrainingSet.ImageName{end};
                CommonHandles.SamplingPool.Mapping{end}.CellNumberInImage = CommonHandles.TrainingSet.CellNumber{end};
                [~,classLabel] = max(CommonHandles.TrainingSet.Class{end});
                CommonHandles.SamplingPool.ClassLabels(end+1) = classLabel;
            end
        end
                
        addToClassImages(counter);
        
    else
        button = questdlg('Would You like to change the class of this cell?','Unclassify?','Yes','No','No');
        
        if strcmp(button, 'Yes')
            
            moveClassImage(CommonHandles.SelectedCell, CommonHandles.SelectedImageName, counter);
            
            classImg.Image = CommonHandles.CurrentSelectedCell;
            classImg.ClassName = CommonHandles.Classes{counter}.Name;
            classImg.CellNumber = CommonHandles.SelectedCell;
            classImg.ImageName = CommonHandles.SelectedImageName;
            if ~CommonHandles.ClassImages.isKey(classImg.ClassName)
                newclass = struct('Images',[],'selected',struct(),'sHandle',0);
                CommonHandles.ClassImages(classImg.ClassName) = newclass;
            end
            cimgs = CommonHandles.ClassImages(classImg.ClassName);
            cimgs.Images{end+1} = classImg;
            CommonHandles.ClassImages(classImg.ClassName) = cimgs;
            CommonHandles.Classes{counter}.LabelNumbers = CommonHandles.Classes{counter}.LabelNumbers + 1;
            
            refreshClassesList();
        end
    end
end

function showCells_Callback(hObject, eventdata, counter)     
    
    f = showTrainedCells_GUI('UserData',counter);
    %uiwait(f);
end