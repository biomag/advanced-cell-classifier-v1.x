function [] = addHCSamplesToTrainingSet(featureMatrix)

    global CommonHandles;
    
    for i=1:size(featureMatrix,1)        
        [~,indx] = ismember(featureMatrix(i,:),CommonHandles.OCC.LowProb.featureVector,'rows');
        currentCellNumberInImage = CommonHandles.OCC.LowProb.selectedCell{indx};
        currentImageName = CommonHandles.OCC.LowProb.imageName{indx};  
    
        CommonHandles.TrainingSet.MetaDataFilename = [CommonHandles.TrainingSet.MetaDataFilename; CommonHandles.SelectedMetaDataFileName];
        CommonHandles.TrainingSet.ImageName = [CommonHandles.TrainingSet.ImageName; currentImageName];
        CommonHandles.TrainingSet.OriginalImageName = [CommonHandles.TrainingSet.ImageName; currentImageName];
        CommonHandles.TrainingSet.CellNumber = [CommonHandles.TrainingSet.CellNumber; currentCellNumberInImage];
        CommonHandles.TrainingSet.ClassVector = zeros(CommonHandles.MaxCluster,1);
        CommonHandles.TrainingSet.ClassVector(CommonHandles.MaxCluster) = 1;
        CommonHandles.TrainingSet.Class = [CommonHandles.TrainingSet.Class; CommonHandles.TrainingSet.ClassVector];
        CommonHandles.TrainingSet.Features = [CommonHandles.TrainingSet.Features; featureMatrix(i,:)];
        
        classImg.Image = CommonHandles.OCC.LowProb.img{indx};
        classImg.ImageName = currentImageName;
        classImg.CellNumber = currentCellNumberInImage;
        classImg.ClassName = CommonHandles.Classes{CommonHandles.MaxCluster}.Name;
        if ~CommonHandles.ClassImages.isKey(classImg.ClassName)
            newclass = struct('Images',[],'selected',struct(),'sHandle',0);
            CommonHandles.ClassImages(classImg.ClassName) = newclass;
        end
        cimgs = CommonHandles.ClassImages(classImg.ClassName);
        cimgs.Images{end+1} = classImg;
        CommonHandles.ClassImages(classImg.ClassName) = cimgs;
    end
    
    CommonHandles.LabeledInstances = sum(cell2mat(CommonHandles.TrainingSet.Class'), 2)';
    for i = 1: length(CommonHandles.LabeledInstances)
        CommonHandles.Classes{i}.LabelNumbers = CommonHandles.LabeledInstances(i);        
    end
    
    refreshClassesList();
end

