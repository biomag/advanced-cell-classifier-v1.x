function out = createWekaFile(CommonHandles, outputFileName, mapClasses);
%% Weka dataset creator

fid = fopen([outputFileName '.arff'], 'w');

fprintf(fid, '@relation ''HTS data set''\n');

for i=1:(length(CommonHandles.TrainingSet.Features{1}))

    fprintf(fid, '@attribute ''feature%d'' real\n', i);
    
end;

fprintf(fid, '@attribute class {');
for i=1:max(mapClasses)
    fprintf(fid, '%d ', i);
end;
fprintf(fid, '}\n');

fprintf(fid, '@data\n');

for i=1:length(CommonHandles.TrainingSet.Features)
    
    fv = CommonHandles.TrainingSet.Features{i};
    
    for j=1:length(fv)

            fprintf(fid, '%f,', fv(j));
            
    end;

    cv = CommonHandles.TrainingSet.Class{i};    
    
    [maxi, maxv] = max(cv);
    
%     if maxv == 3 || maxv == 4
%         maxv = 2;
%     elseif maxv == 7 || maxv == 8
%         maxv = 6;
%     end;
    
    fprintf(fid, '%d\n', mapClasses(maxv));
    
    i/length(CommonHandles.TrainingSet.Features)
    
end;

fclose(fid);

out = 0;

