%this function train a SALT model which is stored in the CommmonHandles.AC
%(Active Controller).
function [ CommonHandles ] = trainSALTForAC( CommonHandles )

if CommonHandles.SALT.initialized
    CommonHandles.AC.SALT = CommonHandles.SALT;
    CommonHandles.SALT.trainingData = convertACC2SALT(CommonHandles);    
    CommonHandles.AC.SALT.model = sacTrain(CommonHandles.AC.classifierName, CommonHandles.SALT.trainingData);  
    disp('ACs SALT is trained');
else 
    errordlg('Active learning cannot be used currently without SALT');
    %CommonHandles = Train(CommonHandles);
end;


end

