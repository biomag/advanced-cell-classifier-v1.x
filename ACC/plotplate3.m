function plotplate3(in1, in2);

spaceBetween = 5;
maxDataPoint = 20;
[xs, ys] = size(in1);

in1Orig = in1;
in2Orig = in2;

% find -1 values and set to the average of the rest
minusOne = find(in1 == -1); restVals = find(in1~=-1); meanRest = mean(in1(restVals)); in1(minusOne) = meanRest;
minusOne = find(in2 == -1); restVals = find(in2~=-1); meanRest = mean(in2(restVals)); in2(minusOne) = meanRest;

plateOverlay = ones(xs*(spaceBetween+maxDataPoint) + spaceBetween, ys*(spaceBetween+maxDataPoint) + spaceBetween, 3)*255;

in1 = ((in1 - (min(min(in1))))/(max(max(in1))))*128;

in2 = max(max(in2)) - in2;
in2 = int16(((in2 - (min(min(in2))))/(max(max(in2))))*5);

for i=1:xs
    for j=1:ys
        if (in1Orig(i, j) == -1) || (in2Orig(i, j) == -1)
            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+5:i*(maxDataPoint+spaceBetween)-5, (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+5:j*(maxDataPoint+spaceBetween)-5, :) = 64;
            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+6:i*(maxDataPoint+spaceBetween)-6, (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+6:j*(maxDataPoint+spaceBetween)-6, :) = 200;            

            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+8:i*(maxDataPoint+spaceBetween)-8, (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+8:j*(maxDataPoint+spaceBetween)-8, 1) = 255;            
            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+8:i*(maxDataPoint+spaceBetween)-8, (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+8:j*(maxDataPoint+spaceBetween)-8, [2 3]) = 0;            
            
            
        else
            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+in2(i, j):i*(maxDataPoint+spaceBetween)-in2(i, j), (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+in2(i, j):j*(maxDataPoint+spaceBetween)-in2(i, j), :) = 64;
            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+1+in2(i, j):i*(maxDataPoint+spaceBetween)-1-in2(i, j), (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+1+in2(i, j):j*(maxDataPoint+spaceBetween)-1-in2(i, j), 1) = 200 - in1(i, j);
            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+1+in2(i, j):i*(maxDataPoint+spaceBetween)-1-in2(i, j), (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+1+in2(i, j):j*(maxDataPoint+spaceBetween)-1-in2(i, j), 2) = 180 - in1(i, j);
            plateOverlay((i-1)*(maxDataPoint+spaceBetween)+spaceBetween+1+in2(i, j):i*(maxDataPoint+spaceBetween)-1-in2(i, j), (j-1)*(maxDataPoint+spaceBetween)+spaceBetween+1+in2(i, j):j*(maxDataPoint+spaceBetween)-1-in2(i, j), 3) = 220 - in1(i, j);        
        end;
    end;
end;

plateOverlay = uint8(plateOverlay);
imshow(plateOverlay);
imwrite(plateOverlay, 'hitmap.bmp');
