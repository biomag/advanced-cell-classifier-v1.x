function CommonHandles = PlotCurrentHistogram(CommonHandles);

r = imhist(CommonHandles.CurrentImage(:,:,1), 128);
g = imhist(CommonHandles.CurrentImage(:,:,2), 128);
b = imhist(CommonHandles.CurrentImage(:,:,3), 128);

HistImage = zeros(64,128,3)+.81;

r=log(r+1); r = r/max(r) * 63 + 1; r = uint8(r); 
g=log(g+1); g = g/max(g) * 63 + 1; g = uint8(g); 
b=log(b+1); b = b/max(b) * 63 + 1; b = uint8(b); 

for i=2:128 
    HistImage((65-r(i-1)):(65-r(i)), i, 1)=1; 
    HistImage((65-r(i)):(65-r(i-1)), i, 1)=1;     
    HistImage((65-r(i-1)):(65-r(i)), i, [2 3])=0; 
    HistImage((65-r(i)):(65-r(i-1)), i, [2 3])=0; 
    
    HistImage((65-g(i-1)):(65-g(i)), i, 2)=1; 
    HistImage((65-g(i)):(65-g(i-1)), i, 2)=1;     
    HistImage((65-g(i-1)):(65-g(i)), i, [1 3])=0; 
    HistImage((65-g(i)):(65-g(i-1)), i, [1 3])=0;     

    HistImage((65-b(i-1)):(65-b(i)), i, 3)=1; 
    HistImage((65-b(i)):(65-b(i-1)), i, 3)=1;     
    HistImage((65-b(i-1)):(65-b(i)), i, [1 2])=0; 
    HistImage((65-b(i)):(65-b(i-1)), i, [1 2])=0;         
end;

%put lines
% white
HistImage(1:64, uint8(CommonHandles.MainView.StrechMin/2)+1, [1 2 3]) = 1;
%yellow
HistImage(1:64, uint8(CommonHandles.MainView.StrechMax/2)+1, [1 2]) = 1;
HistImage(1:64, uint8(CommonHandles.MainView.StrechMax/2)+1, [3]) = 0;

imshow(HistImage, 'Parent', CommonHandles.HistogramViewHandle);

