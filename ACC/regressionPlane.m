function varargout = regressionPlane(varargin)
% REGRESSIONPLANE M-file for regressionPlane.fig
%      REGRESSIONPLANE, by itself, creates a new REGRESSIONPLANE or raises the existing
%      singleton*.
%
%      H = REGRESSIONPLANE returns the handle to a new REGRESSIONPLANE or the handle to
%      the existing singleton*.
%
%      REGRESSIONPLANE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REGRESSIONPLANE.M with the given input arguments.
%
%      REGRESSIONPLANE('Property','Value',...) creates a new REGRESSIONPLANE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before regressionPlane_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to regressionPlane_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help regressionPlane

% Last Modified by GUIDE v2.5 11-Nov-2015 09:03:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @regressionPlane_OpeningFcn, ...
                   'gui_OutputFcn',  @regressionPlane_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global initialApparentPictureSize;
global apparentPictureSize;
initialApparentPictureSize = 100;
apparentPictureSize = initialApparentPictureSize;
global adaptiveCellSize;
adaptiveCellSize = 0;
%our full figure will have black background
whitebg('k');



% --- Executes just before regressionPlane is made visible.
function regressionPlane_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to regressionPlane (see VARARGIN)

% Choose default command line output for regressionPlane
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes regressionPlane wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% GLOBAL variables and global 'constants'.
global CommonHandles;
%The currently displayed picture size
%bounds stores the current boundaries of the seen regression plane. They
%can be only sub-intervals of [0 1000]. (these are now the total regression
%plane boundaries).
global XBounds;
global YBounds;
global ghandles;
ghandles = handles;
% newObject is a boolean to show whether we're putting down now a cell to
% distinguish between selecting cell and putting down new cell with right
% click.
global newObject;
global selectedObject;
XBounds = [0 1000];
YBounds = [0 1000];

global showGrid;
global gridOnTheTop;
global gridDistance;
gridDistance = 5;


selectedObject = length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images);
newObject = 1;

if get(handles.checkbox1,'Value')
    showGrid = 1;
else
    showGrid = -1;
end
if get(handles.checkbox3,'Value')
    gridOnTheTop = 1;
else
    gridOnTheTop = -1;
end


placeItemsOnGUI(handles);

refreshSelectedObjectPictures();
plotNavigationMap();

%set(handles.axes11,'Ydir','reverse');
drawRegressionPlane(handles);


% --- Outputs from this function are returned to the command line.
function varargout = regressionPlane_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- drawRegressionPlane function draws out the required part of the
% regression plane.
function drawRegressionPlane(handles)
% XBounds is a 2 element array with lower (1st element) and upper (2nd
% element) bound to the x coordinates.
% YBounds is the same for y.
% CommonHandles and handles are used for communication.

%global variables
global apparentPictureSize;
global CommonHandles;
global XBounds;
global YBounds;
% This matrix is the same sized as the full regression plane which is the
% displayed regPlane. This whichObjectThere matrix contains the numbers of
% the object which is the most upper right now at the given pixel position.
% This helps us to determine if a right click was made that which cell is
% selected.
global whichObjectThere;
% If an object will be selected then it'll be stored here.
global selectedObject;
% Stored handle to the currently selected cell's axes
global selectedAxes;
%Boolean storing the grid switch's status
global showGrid;
%real storing the grid unit
global gridDistance;
%boolean indicating whether the grid is on the top of every cell or below
%them
global gridOnTheTop;

%regPlaneBoundaries: the pixel positions for the regression plane in the
%GUI. 1-2 coordinates are the x and y position for the left bottom corner,
%the 3-4 coordinates are the size in x and y direction.
regPlaneBoundaries = getpixelposition(handles.axes1);

plotNavigationMap();

%setting the scales to the right place
xlim(handles.axes11,[XBounds(1) XBounds(2)]);
ylim(handles.axes11,[YBounds(1) YBounds(2)]);
%push the scale axes to the back.
uistack(handles.axes11,'bottom');

%detecting whether there are any images to display (at the start there
%isn't any)
if ~isfield(CommonHandles.Regression{CommonHandles.RegressionLastClass},'ImagePosition')
    numOfDisplayedImages = 0;
else
   numOfDisplayedImages = length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition);
end

%If bacause of some reason not all the coordinates were given then we
%delete this cell from the regression plane (it is an error). The toDelete
%array stores the indeces which has to be deleted.
toDelete = [];
%the full DISPLAYED regression plane which can be seen on the screen.
fullRegPlane = zeros(regPlaneBoundaries(4),regPlaneBoundaries(3),3);
%According to the current bounds we fill the regression plane with a grid.
if (((showGrid+1)==2) && ((gridOnTheTop+1)~=2))

    XBU = ceil(XBounds(1)/gridDistance)*gridDistance;
    XBL = floor(XBounds(2)/gridDistance)*gridDistance;
    XLines = XBU:gridDistance:XBL;
    XLines = round( (XLines-XBounds(1))./ (XBounds(2)-XBounds(1)).*regPlaneBoundaries(3));
    fullRegPlane(:,XLines+1,:) = 100;

    YBU = ceil(YBounds(1)/gridDistance)*gridDistance;
    YBL = floor(YBounds(2)/gridDistance)*gridDistance;
    YLines = YBU:gridDistance:YBL;
    YLines = round( (YLines-YBounds(1))./ (YBounds(2)-YBounds(1)).*regPlaneBoundaries(4));
    fullRegPlane(regPlaneBoundaries(4)-YLines+1,:,:) = 100;
end

%whichObjectThere stores for the seen regression plane the upper most one.
whichObjectThere = zeros(regPlaneBoundaries(4),regPlaneBoundaries(3));

%Try to delete the selectedAxes. If there wasn't any selected axes then we
%catch the error if there were then we'll plot it again in the big for loop
try cla(selectedAxes); catch; end

%For every cell which has it's position on the regression plane.
for i=1:numOfDisplayedImages    
    %There can be cases when there is an image in the
    %CommonHandles.Regression{x}.Images field but there is no corresponding
    %image position. To handle these we write the next few line of code.
    if length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{i})<2
        CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{i} = [-200 -200];
        toDelete = [toDelete i]; %#ok<AGROW>
    end

    %Getting out the regression position of the currentImage from
    %CommonHandles.
    centerX = CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{i}(1);
    centerY = CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{i}(2);

    %calculating the relative position to the displayed part of the regPlan     
    distFromLowerXInRegPlane = centerX - XBounds(1);    
    distFromLowerYInRegPlane = centerY - YBounds(1);

    %Transformation of the coordinates to the zoomed in space.
    centerX = regPlaneBoundaries(3)*distFromLowerXInRegPlane/(XBounds(2)-XBounds(1));
    centerY = regPlaneBoundaries(4)*distFromLowerYInRegPlane/(YBounds(2)-YBounds(1));
    %Flip of Y because of different image and matrix indexing.
    centerY = regPlaneBoundaries(4) - centerY;
    
    %Get the real size of picture which we'd like to display
    pictureDataXSize = size(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{i},2);
    pictureDataYSize = size(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{i},1);
    %The displayed image size initialization
    apparentPictureXSize = apparentPictureSize;
    apparentPictureYSize = apparentPictureSize;
    
    %Calculating the left bottem corner position and round because we need
    %integer coordinates for the fullRegPlane image matrix
    leftUpperX = round(centerX - (apparentPictureXSize / 2));
    leftUpperY = round(centerY - (apparentPictureYSize / 2));
    
    %Transform to the GUI's coordinate system.    

    %If the current picture is in the required area then:
    if ((leftUpperX+apparentPictureXSize >= 0) && (leftUpperX <= regPlaneBoundaries(3)) && (leftUpperY+apparentPictureYSize >= 0) && (leftUpperY <= regPlaneBoundaries(4)))        
        %Handle the cases when the picture is out of the regression bound.
        
        %Initializing shown image boundaries.
        apparentStartX = leftUpperX;    
        apparentStartY = leftUpperY;    
        apparentEndX = floor(leftUpperX+apparentPictureXSize-1);    
        apparentEndY = floor(leftUpperY+apparentPictureYSize-1);    
        
        %Do the cuttings on the edge.
        if apparentStartX<1
           apparentStartX=1;
        end
        if apparentStartY<1
           apparentStartY=1;
        end
        if apparentEndX>regPlaneBoundaries(3)
           apparentEndX=regPlaneBoundaries(3);
        end
        if apparentEndY>regPlaneBoundaries(4)
           apparentEndY=regPlaneBoundaries(4);
        end
        
        %Calculating the start and end position for the picture pixels.
        XStartOfPicture = floor ( 1 + ((apparentStartX-leftUpperX)/apparentPictureXSize)*(pictureDataXSize-1) );
        YStartOfPicture = floor ( 1 + ((apparentStartY-leftUpperY)/apparentPictureYSize)*(pictureDataYSize-1) );
        XEndOfPicture = floor ( 1 + ((apparentEndX-leftUpperX)/apparentPictureXSize)*(pictureDataXSize-1)  );
        YEndOfPicture = floor ( 1 + ((apparentEndY-leftUpperY)/apparentPictureYSize)*(pictureDataYSize-1)  );
        
        %Calculate the real amount of elements which should be displayed
        %from the picture matrix
        pictureSizeX = XEndOfPicture - XStartOfPicture + 1;
        pictureSizeY = YEndOfPicture - YStartOfPicture + 1;        
        
        %Final check        
        if pictureSizeX < 0, pictureSizeX = 0; end
        if pictureSizeY < 0, pictureSizeY = 0; end        
        if XStartOfPicture < 1, XStartOfPicture = 1; end
        if YStartOfPicture < 1, YStartOfPicture = 1; end       
        try                     
            %Do the interpolation
            %Calculate the rescale rate
            tempXMult = (XEndOfPicture-XStartOfPicture)/(apparentEndX-apparentStartX);
            tempYMult = (YEndOfPicture-YStartOfPicture)/(apparentEndY-apparentStartY);
            %The offset which is between the picture's coordinate system
            %and the regression plane's coordinate system
            tempXOffset = (-apparentStartX*tempXMult) + XStartOfPicture;
            tempYOffset = (-apparentStartY*tempYMult) + YStartOfPicture;
            
            tempy=[apparentStartY:apparentEndY];
            tempPictureY=ceil(tempy*tempYMult + tempYOffset);                
            tempx=[apparentStartX:apparentEndX];
            tempPictureX=ceil(tempx*tempXMult + tempXOffset);
            if i~=selectedObject                            
                fullRegPlane(tempy,tempx,:) = CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{i}(tempPictureY,tempPictureX,:);       
                whichObjectThere(tempy,tempx) = i;
            else                
                %Transform back leftBottomY as the axes' position is
                %counted as a picture.
                leftBottomY = regPlaneBoundaries(4) - apparentEndY;
                selectedAxes = axes('Units','pixels','Position',[apparentStartX+regPlaneBoundaries(1) leftBottomY+regPlaneBoundaries(2) (apparentEndX-apparentStartX) (apparentEndY-apparentStartY)]); %#ok<LAXES>
                selectedObjectPicture = CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{i}(tempPictureY,tempPictureX,:);
                selectedObjectPicture([1,end],:,1) = 255;
                selectedObjectPicture(:,[1,end],1) = 255;
                selectedObjectPicture([1,end],[1,end],[2 3]) = 0;
                im = imshow(selectedObjectPicture,'Parent',selectedAxes);
                axis image;

                set(selectedAxes,'ButtonDownFcn',@dragObject);
                %the button doen fcn will not work until the image hit test is off
                set(im,'HitTest','off');

                %now set an image button doen fcn
                set(im,'ButtonDownFcn',@dragObject);

                %the image funtion will not fire until hit test is turned on
                set(im,'HitTest','on'); %now image button function will work
            end
        catch %exc
            %disp(getReport(exc));
            %NOTE: Sometimes a dimension mismatch problem occurs, but it
            %cannot be seen on the screen that's why we postpone to solve
            %this problem until we get some bad experience.
        end    
    end      
end
   
if (((showGrid+1)==2) && ((gridOnTheTop+1)==2))

    XBU = ceil(XBounds(1)/gridDistance)*gridDistance;
    XBL = floor(XBounds(2)/gridDistance)*gridDistance;
    XLines = XBU:gridDistance:XBL;
    XLines = round( (XLines-XBounds(1))./ (XBounds(2)-XBounds(1)).*regPlaneBoundaries(3));
    fullRegPlane(:,XLines+1,:) = 100;

    YBU = ceil(YBounds(1)/gridDistance)*gridDistance;
    YBL = floor(YBounds(2)/gridDistance)*gridDistance;
    YLines = YBU:gridDistance:YBL;
    YLines = round( (YLines-YBounds(1))./ (YBounds(2)-YBounds(1)).*regPlaneBoundaries(4));
    fullRegPlane(regPlaneBoundaries(4)-YLines+1,:,:) = 100;
end

im = imshow(uint8(fullRegPlane),'Parent',handles.axes1);

%the button doen fcn will not work until the image hit test is off
set(im,'HitTest','off');

%now set an image button doen fcn
set(im,'ButtonDownFcn',@axes1_ButtonDownFcn);

%the image funtion will not fire until hit test is turned on
set(im,'HitTest','on'); %now image button function will work
        
%delete unpositioned images;

for i=toDelete
    CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition(i) = [];
    CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images(i) = [];
    CommonHandles.Regression{CommonHandles.RegressionLastClass}.Features(i) = [];
end


function dragObject(hObject,eventdata)
global dragging;
global orPos;
          dragging = hObject;
          orPos = get(gcf,'CurrentPoint');      
          set(gcf,'Pointer','hand');


% --- Executes on button press in pushbutton1, which is the Cross position
% button.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
global selectedObject;
global selectedAxes;
global apparentPictureSize;
global newObject;


% ask for 1 mouse position
newPos = ginput(1);
regPlaneBoundaries = getpixelposition(handles.axes1);
newPos(1) = newPos(1) + regPlaneBoundaries(1);
newPos(2) = regPlaneBoundaries(2) + regPlaneBoundaries(4)- newPos(2);

if isInRegPlane(newPos, regPlaneBoundaries);   
   newPos = GUI2regPlane(newPos);
   if newObject       
       CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images)} = [newPos(1) newPos(2)];
       newObject = 0;
   else       
       CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{selectedObject} = newPos;
       set(selectedAxes,'Position',[newPos(1) newPos(2) apparentPictureSize apparentPictureSize]);
   end
   drawRegressionPlane(handles);
   refreshSelectedObjectInfoData;
end

% --- Executes on button press in pushbutton2. Regression errors. Not
% implemented yet.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% prediction errors
global CommonHandles;

CommonHandles = TrainRegression(CommonHandles, CommonHandles.RegressionLastClass);    

% collect all features 
for i=1:length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition)
    features(i,:) = CommonHandles.Regression{CommonHandles.RegressionLastClass}.Features{i}; 
end;    
    
%try    
  [predictedValues, props] = PredictRegression2D(features', CommonHandles, CommonHandles.RegressionLastClass);    
%end;

CommonHandles.Regression{CommonHandles.RegressionLastClass}.LastPrediction = predictedValues;

CommonHandles = CreatePlaneImage(CommonHandles);
imshow(CommonHandles.LastRegressionPlane{CommonHandles.RegressionLastClass}(51:1050, 51:1050, :), 'Parent', handles.axes1);


% --- Executes on scroll wheel click while the figure is in focus.
function figure1_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

global XBounds;
global YBounds;
global adaptiveCellSize;
global apparentPictureSize;
global startOfAdaptionCellSize;

regPlaneBoundaries = getpixelposition(handles.axes1);
pos = get(gcf,'CurrentPoint');

if pos(1)>regPlaneBoundaries(1) && pos(1)<regPlaneBoundaries(1)+regPlaneBoundaries(3) && pos(2)> regPlaneBoundaries(2) && pos(2) < regPlaneBoundaries(2) + regPlaneBoundaries(4)        
    if eventdata.VerticalScrollCount>0 % we're zooming out        
        newSizeX = (XBounds(2) - XBounds(1))*1.2;
        newSizeY = (YBounds(2) - YBounds(1))*1.2;                
        if newSizeX>1000 newSizeX = 1000; end
        if newSizeY>1000 newSizeY = 1000; end 
        if (adaptiveCellSize && apparentPictureSize > startOfAdaptionCellSize)
            apparentPictureSize = apparentPictureSize.*0.8;                        
        end              
    else % we're zooming in
        newSizeX = (XBounds(2) - XBounds(1))*0.9;
        newSizeY = (YBounds(2) - YBounds(1))*0.9;                
        if adaptiveCellSize
            apparentPictureSize = apparentPictureSize.*1.1;
        end
    end    
    calcXYBounds(newSizeX, newSizeY, pos, regPlaneBoundaries);
    drawRegressionPlane(handles);
        
end

%This util function modifies the global X and YBounds variables according to the
%new XY sizes and the center of the view which is stored in pos. We also
%need the regPlaneBoundaries variable which stores the position of the
%regression plane.
function calcXYBounds(newSizeX, newSizeY, pos, regPlaneBoundaries)
    global XBounds;
    global YBounds;
    
    posXInRegPlane = (pos(1) - regPlaneBoundaries(1))/regPlaneBoundaries(3)*(XBounds(2) - XBounds(1))+XBounds(1);
    posYInRegPlane = (pos(2) - regPlaneBoundaries(2))/regPlaneBoundaries(4)*(YBounds(2) - YBounds(1))+YBounds(1);            
    
    XNewLower = posXInRegPlane - (pos(1)-regPlaneBoundaries(1))*newSizeX./regPlaneBoundaries(3);
    if XNewLower<0 XNewLower = 0; end    
    XNewUpper = XNewLower + newSizeX;
    if XNewUpper>1000 XNewUpper = 1000; end    
    YNewLower = posYInRegPlane - (pos(2)-regPlaneBoundaries(2))*newSizeY./regPlaneBoundaries(4);
    if YNewLower<0 YNewLower = 0; end    
    YNewUpper = YNewLower + newSizeY;        
    if YNewUpper>1000 YNewUpper = 1000; end
    
    XBounds = [ XNewLower XNewUpper ];
    YBounds = [ YNewLower YNewUpper ];

%This util function helps to convert back the points from the GUI to the
%regression plane.
function [newPos] = GUI2regPlane(newPos)
global ghandles;
global XBounds;
global YBounds;
handles = ghandles;

regPlaneBoundaries = getpixelposition(handles.axes1); 

%Transform back from the displayed coordinate system to the stored one.
distFromXLowerInGUI = newPos(1) - regPlaneBoundaries(1);
distFromYLowerInGUI = newPos(2) - regPlaneBoundaries(2);

newPos(1) = distFromXLowerInGUI*(XBounds(2)-XBounds(1))/regPlaneBoundaries(3) + XBounds(1);
newPos(2) = distFromYLowerInGUI*(YBounds(2)-YBounds(1))/regPlaneBoundaries(4) +YBounds(1);

%This util function refreshes the pictures of the selectedCells.
function refreshSelectedObjectPictures()
global selectedObject;
global CommonHandles;
global ghandles;
handles = ghandles;

if selectedObject ~=0
    imshow(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{selectedObject},'Parent',handles.axes4);
    greenImage = zeros(size(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{selectedObject},1),size(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{selectedObject},2),3);
    greenImage(:,:,2) = CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images{selectedObject}(:,:,2);
    imshow(uint8(greenImage),'Parent',handles.axes5);        
else
    imshow(ones(100,100,3),'Parent',handles.axes4);
    imshow(ones(100,100,3),'Parent',handles.axes5);
end

%This util function refreshes the selectedObject's data like coordinates
%and cell number.
function refreshSelectedObjectInfoData()
global ghandles;
global CommonHandles;
global selectedObject;
%global selectedAxes;

if (selectedObject == 0)
    set(ghandles.selectedCellText,'String', 'None');                
    set(ghandles.editXCoord,'String', 'N');                
    set(ghandles.editYCoord,'String', 'N');                
else                
    set(ghandles.selectedCellText,'String', num2str(selectedObject));
    set(ghandles.editXCoord,'String', num2str(CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{selectedObject}(1)));                
    set(ghandles.editYCoord,'String', num2str(CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{selectedObject}(2)));                
end


%This util function will do the layout management for the regression plane.
%It should get the information from the regPlane main plot about
%resizements and then adjust the layout to that. We wish to make greater
%only the regression plane all the others should be maintained.
function placeItemsOnGUI(handles)

dimensions = get(gcf,'Position');
sizeX = dimensions(3);
sizeY = dimensions(4);

set(handles.axes1,'Units','pixels');
set(handles.axes1,'Position',[300, 50, sizeX-350, sizeY-200]);

set(handles.axes11,'Units','pixels');
set(handles.axes11,'Position',[300, 50, sizeX-350, sizeY-200]);


%Drawing the 2 red scale picture to the 'axes'
scale = ones(50, sizeX, 3)*255;
scale2 = ones(sizeY, 50, 3)*255;

for i=1:round(sizeX / 20)    
    scale(51-round(i*50 / sizeX *20):50, (i-1)*20+1:i*20, 1) = 255;
    scale(51-round(i*50 / sizeX *20):50, (i-1)*20+1:i*20, 2) = 0;
    scale(51-round(i*50 / sizeX *20):50, (i-1)*20+1:i*20, 3) = 0;    
end
for i=1:round(sizeY / 20);
    scale2((i-1)*20+1:i*20, 51-round(i*50 / sizeX *20):50, 1) = 255;
    scale2((i-1)*20+1:i*20, 51-round(i*50 / sizeX *20):50, 2) = 0;
    scale2((i-1)*20+1:i*20, 51-round(i*50 / sizeX *20):50, 3) = 0;    
      
end

imshow(scale, 'Parent', handles.axes2);
imshow(scale2, 'Parent', handles.axes3);


set(handles.axes2,'Units','pixels');
set(handles.axes2,'Position',[300, sizeY-125, sizeX-350, 50]);

set(handles.axes3,'Units','pixels');
set(handles.axes3,'Position',[225, 50, 50, sizeY-200]);

set(handles.axes4,'Position', [25, sizeY-125, 100, 100]);
set(handles.axes5,'Position', [150, sizeY-125, 100, 100]);
set(handles.axes10,'Position',[50, sizeY-300, 150, 150]);
set(handles.pushbutton1,'Position',[50, sizeY-425, 100, 25]);
set(handles.pushbutton2,'Position',[50, sizeY-475, 100, 25]);
set(handles.uipanel5,'Position',[50 sizeY-650, 150, 150]);
set(handles.text1,'Position',[300, sizeY-50, 300, 25]);
set(handles.increaseImageSize,'Position',[650 sizeY-50, 75, 25]);
set(handles.resetImageSize,'Position',[750 sizeY-50, 75, 25]);
set(handles.decreaseImageSize,'Position',[850 sizeY-50, 75, 25]);
set(handles.infoTextSelectedCell,'Position',[50 sizeY-350, 100, 25]);
set(handles.selectedCellText,'Position',[150 sizeY-350, 50, 25]);
set(handles.posInfo,'Position',[50 sizeY-375, 50, 25]);
set(handles.editXCoord,'Position',[100 sizeY-375, 50, 25]);
set(handles.editYCoord,'Position',[150 sizeY-375, 50, 25]);


%This util function is plotting the navigation on which we can see where we
%are currently on the regression plane. (how much we're zoomed in etc.)
function plotNavigationMap
global ghandles;
global XBounds;
global YBounds;

axes(ghandles.axes10);
plot([0 0 1000 1000 0],[0 1000 1000 0 0],'k');
hold on;
plot([XBounds(1) XBounds(1) XBounds(2) XBounds(2) XBounds(1)],[YBounds(1) YBounds(2) YBounds(2) YBounds(1) YBounds(1)],'w');
hold off;
%set(ghandles.axes10,'Ydir','reverse');

%This util function is more like a macro, it is used for shortening long
%conditions when we're examing whether a point is in the regression plane
%or not.
function bool = isInRegPlane(newPos, regPlaneBoundaries)
bool = newPos(1)>regPlaneBoundaries(1) && newPos(1)<regPlaneBoundaries(1)+regPlaneBoundaries(3) && newPos(2)> regPlaneBoundaries(2) && newPos(2) < regPlaneBoundaries(2) + regPlaneBoundaries(4);

    
% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

placeItemsOnGUI(handles);
drawRegressionPlane(handles);

% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%global variables
global CommonHandles;
global ghandles;
global mapDragged;
global orPos;
global newObject;
global whichObjectThere;
global selectedObject;
global selectedAxes;

mapDragged = 0;

selType = get(gcf,'SelectionType');

switch selType
    case 'alt'                
        pos = get(gcf,'CurrentPoint');        

        if ~exist('handles','var')        
            handles = ghandles;
            regPlaneBoundaries = getpixelposition(handles.axes1);    
        else
            %Get the bounds of the axes1 which is the regression plane.
            regPlaneBoundaries = getpixelposition(handles.axes1);    
        end

        %Handle the out of bounds cases.
        if pos(1)<regPlaneBoundaries(1) pos(1) = regPlaneBoundaries(1); end
        if pos(1)>regPlaneBoundaries(1)+regPlaneBoundaries(3) pos(1) = regPlaneBoundaries(1)+regPlaneBoundaries(3); end
        if pos(2)<regPlaneBoundaries(2) pos(2) = regPlaneBoundaries(2); end
        if pos(2)>regPlaneBoundaries(2)+regPlaneBoundaries(4) pos(2) = regPlaneBoundaries(2)+regPlaneBoundaries(4); end

        distFromXLowerInGUI = pos(1) - regPlaneBoundaries(1);
        distFromYLowerInGUI = pos(2) - regPlaneBoundaries(2);

        
        pos = GUI2regPlane(pos);
        
        if newObject % if we're placing an object to the plane.
            CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Images)} = [pos(1) pos(2)];
            newObject = 0;    
            prevGUI = gcbf;
            drawRegressionPlane(handles);                
            pause(0.1);                      
            divisor = '8.4.0.150.421 (R2014b)';
            if (compareVersions(divisor,CommonHandles.MatlabVersion))
                set(CommonHandles.MainWindow,'Visible','off');                       
            end
            set(CommonHandles.MainWindow,'Visible','on');                                    
            uiresume(prevGUI);            
        else % if we want to select an object            
            selectedObject = whichObjectThere(regPlaneBoundaries(4)-distFromYLowerInGUI,distFromXLowerInGUI);                       
            refreshSelectedObjectPictures();
            if selectedObject == 0
                try cla(selectedAxes);
                catch                    
                end                
            end
            refreshSelectedObjectInfoData();
            drawRegressionPlane(handles);                           
        end        
    case 'normal'
        mapDragged = 1;
        orPos = get(gcf,'CurrentPoint');
        set(gcf,'Pointer','fleur');
end


% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global dragging;
    global orPos;       
    global mapDragged;
    global XBounds;
    global YBounds;
    newPos = get(gcf,'CurrentPoint');    
    regPlaneBoundaries = getpixelposition(handles.axes1);    
        
    if mapDragged        
        posDiff = newPos - orPos;
        %the magic number 10 is needed for unknown reason. Without this the
        %regression plane moves too fast.
        posXDiffRegPlane = posDiff(1)*(XBounds(2)-XBounds(1))./regPlaneBoundaries(3)./10;
        posYDiffRegPlane = posDiff(2)*(YBounds(2)-YBounds(1))./regPlaneBoundaries(4)./10;
        
        if max(XBounds - posXDiffRegPlane)<1000 && min(XBounds - posXDiffRegPlane)>0
            XBounds = XBounds - posXDiffRegPlane;        
        end
        if max(YBounds - posYDiffRegPlane)<1000 && min(YBounds - posYDiffRegPlane)>0
            YBounds = YBounds - posYDiffRegPlane;
        end                
        drawRegressionPlane(handles);
    end
       
    if ~isempty(dragging)
        if newPos(1)>regPlaneBoundaries(1) && newPos(1)<regPlaneBoundaries(1)+regPlaneBoundaries(3) && newPos(2)> regPlaneBoundaries(2) && newPos(2) < regPlaneBoundaries(2) + regPlaneBoundaries(4)
              posDiff = newPos - orPos;
              orPos = newPos;              
              set(get(dragging,'Parent'),'Position',get(get(dragging,'Parent'),'Position') + [posDiff(1) posDiff(2) 0 0]);
        end
    end
        

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

regPlaneBoundaries = getpixelposition(handles.axes1);    

global mapDragged;
global dragging;
global orPos;
global CommonHandles;
global selectedObject;

mapDragged = 0;
set(gcf,'Pointer','arrow');

if ~isempty(dragging)
    newPos = get(gcf,'CurrentPoint');
    %if we are on a valid point
    if isInRegPlane(newPos, regPlaneBoundaries);
        posDiff = newPos - orPos;
        set(get(dragging,'Parent'),'Position',get(get(dragging,'Parent'),'Position') + [posDiff(1) posDiff(2)  0 0]);
        
        newPos = GUI2regPlane(newPos);
                                    
        CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{selectedObject} = [newPos(1) newPos(2)];       
        refreshSelectedObjectInfoData();
    else
        drawRegressionPlane(handles);
    end
    dragging = [];
end

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkbox1
global showGrid;
get(hObject,'Value')

showGrid = showGrid * -1;
drawRegressionPlane(handles);

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global gridDistance;
gridDistance = get(hObject, 'Value');
set(handles.edit1,'String',num2str(get(hObject, 'Value')));
drawRegressionPlane(handles);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Min',1);
set(hObject,'Max',100);
set(hObject,'Value',10);


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
global gridOnTheTop
gridOnTheTop = gridOnTheTop*-1;
drawRegressionPlane(handles);



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

global gridDistance;
gridDistance = str2double(get(hObject,'String'));
set(handles.slider2,'Value',gridDistance);
drawRegressionPlane(handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function uipushtool3_ClickedCallback(hObject, eventdata, handles)
%Delete pushtool. It deletes the selected object
% hObject    handle to uipushtool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global selectedObject;
global CommonHandles;
global selectedAxes;

CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{selectedObject} = 1;
try cla(selectedAxes); catch end
selectedObject = 0;
refreshSelectedObjectPictures;
drawRegressionPlane(handles);


% --------------------------------------------------------------------
function uipushtool4_ClickedCallback(~, ~, handles)
% hObject    handle to uipushtool4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% zoom out to overview tool

global XBounds;
global YBounds;
global adaptiveCellSize;
global startOfAdaptionCellSize;
global initialApparentPictureSize;
global apparentPictureSize;
XBounds = [0 1000];
YBounds = [0 1000];

if adaptiveCellSize
    apparentPictureSize = startOfAdaptionCellSize;
else
end
    


drawRegressionPlane(handles);


% --- Executes on button press in increaseImageSize.
function increaseImageSize_Callback(hObject, eventdata, handles)
% hObject    handle to increaseImageSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global apparentPictureSize;
apparentPictureSize = apparentPictureSize*1.1;
drawRegressionPlane(handles);
%disp(apparentPictureSize);


% --- Executes on button press in decreaseImageSize.
function decreaseImageSize_Callback(hObject, eventdata, handles)
% hObject    handle to decreaseImageSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global apparentPictureSize;
apparentPictureSize = apparentPictureSize/1.1;
drawRegressionPlane(handles);

% --- Executes on button press in resetImageSize.
function resetImageSize_Callback(hObject, eventdata, handles)
% hObject    handle to resetImageSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global apparentPictureSize;
global initialApparentPictureSize;
apparentPictureSize = initialApparentPictureSize;
drawRegressionPlane(handles);


%Callback function for the X coordinate modification. We check the validity
%of the coordinate and move the selected object to the desired position.
function editXCoord_Callback(hObject, eventdata, handles)
% hObject    handle to editXCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editXCoord as text
%        str2double(get(hObject,'String')) returns contents of editXCoord as a double

global selectedObject;
global CommonHandles;

if selectedObject ~=0
    XCoord = str2double(get(hObject,'String'));
    if XCoord<=1000 && XCoord >=0
        CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{selectedObject}(1) = XCoord;
        drawRegressionPlane(handles);
    end
end


% --- Executes during object creation, after setting all properties.
function editXCoord_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editXCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editYCoord_Callback(hObject, eventdata, handles)
% hObject    handle to editYCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editYCoord as text
%        str2double(get(hObject,'String')) returns contents of editYCoord as a double
global selectedObject;
global CommonHandles;

if selectedObject ~=0
    YCoord = str2double(get(hObject,'String'));
    if YCoord<=1000 && YCoord >=0
        CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition{selectedObject}(2) = YCoord;
        drawRegressionPlane(handles);
    end
end



% --- Executes during object creation, after setting all properties.
function editYCoord_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editYCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function plot_well_distribution_Callback(hObject, eventdata, handles)
% hObject    handle to plot_well_distribution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
%This function creates a regression prediction for one plate which is the
%selected one on the select_plate_well_GUI and stored
%in CommonHandles.SelectedPlate field.
% The generated 
function predict_all_pictures_Callback(hObject, eventdata, handles)
% hObject    handle to predict_all_pictures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

TrainTargets = reshape(cell2mat(CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition),[2 length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.ImagePosition)]);
TrainFeatures = reshape(cell2mat(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Features),[length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Features{1}),length(CommonHandles.Regression{CommonHandles.RegressionLastClass}.Features)]);

predictor = NNPredictor(20,0.7,0.2,0.1);
predictor = predictor.train(TrainFeatures,TrainTargets);

% This part now considers all the cells to be placed in the regression
% plane. This has to be modified to work as it works in samplingPool, that
% we put only the cells to the heatmap which belongs to the
% regressionClass.
% TODO plus: if we have more than one image from a well this function
% considers only the last one!
[inputs,mapping,~] = LoadAllFeatures(CommonHandles,1,0,CommonHandles.SelectedPlate);
predictions = predictor.predict(inputs');
results = cell(CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row,CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col);


disp('The predictions will be saved to the actual folder into the PredictionResult subfolder.');
if ~exist('PredictionResult','dir')
    mkdir('PredictionResult'); 
    chdir('PredictionResult');
else
    chdir('PredictionResult');
end

mkdir([CommonHandles.PlatesNames{CommonHandles.SelectedPlate}]);
mkdir([CommonHandles.PlatesNames{CommonHandles.SelectedPlate} filesep 'images']);
mkdir([CommonHandles.PlatesNames{CommonHandles.SelectedPlate} filesep 'thumbs']);

inputFolder = [CommonHandles.DirName filesep CommonHandles.PlatesNames{CommonHandles.SelectedPlate} filesep 'anal1' filesep];
outputFolder = [pwd filesep CommonHandles.PlatesNames{CommonHandles.SelectedPlate} filesep 'images' filesep];

inputFolder2 = [pwd filesep CommonHandles.PlatesNames{CommonHandles.SelectedPlate} filesep 'thumbs' filesep];

fileList = dir([inputFolder '*' CommonHandles.ImageExtension]);

for i=1:numel(fileList)

    im = imread([inputFolder fileList(i).name]);
            
    out = im;
    
    SSIN = strsplit(fileList(i).name(1:end-4),'_');
    
    imwrite(out, [outputFolder SSIN{1} '_' SSIN{2} '_' SSIN{3} '.jpg']);
    
    out = createWhiteBgImg(im);
    
    out = imresize(out, .2);
    
    imwrite(out, [inputFolder2 SSIN{1} '_' SSIN{2} '_' SSIN{3} '.jpg']);
    
end

for i=1:length(mapping)   
    fileName = strsplit(mapping{i}.ImageName,filesep);
    well = strsplit(fileName{end},'_');    
    results{(well{2}(2)-'A'+1),str2num(well{2}(3:4))}(:,end+1) = predictions(:,i);
end
 
createHtml(results',CommonHandles.PlatesNames{CommonHandles.SelectedPlate});
save([CommonHandles.PlatesNames{CommonHandles.SelectedPlate} '_Results'],'results');





%CreateHTML function makes an HTML file on which you can see the results of
%the regression. The results is a huge cell array representing the plate
%and in every cell in it there is a long 2xn array which has all the
%predicted regression coordinates for that well.
function createHtml(results, plateName)
global CommonHandles

mkdir([pwd filesep plateName filesep 'icons'])

% generate small images
binSize = 5;
[xs, ys] = size(results);

for i=1:ys
    for j=1:xs         
        if ~isempty(results{j, i})
            iconImg = plotCurrentRegression2D(results{j, i}, binSize);            
        else
            iconImg = zeros(binSize+2, binSize+2, 3);
            iconImg(:,:,:) = .25;
            iconImg(2:end-1,2:end-1,:) = 0;
        end
        
        iconImg = imresize(iconImg, 5, 'nearest');
        
        fileName = sprintf('dist_%c%0.2d.bmp', i + 'A' - 1, j);
        
        imwrite(uint8(iconImg*255), [plateName '\icons\' fileName]);
    end    
end


% write the html

htmlOut = fopen([plateName '\' plateName '.html'], 'w');

% header
fprintf(htmlOut, '<html>\n<head>\n<link rel="stylesheet" media="all" type="text/css" href="plate.css" />\n</head>\n');

% write col numbers
fprintf(htmlOut, '<table border="0">\n<tr>\n<td> </td>');
for j=1:xs 
    fprintf(htmlOut,'\t<td>%d</td>\n', j); 
end
fprintf(htmlOut, '</tr>\n');

% put the icons
for i=1:ys
    fprintf(htmlOut, '<tr>\n');
    for j=0:xs         
        if j==0
            fprintf(htmlOut,'\t<td>%c</td>\n', i + 'A' - 1); 
        else
            fileName = sprintf('dist_%c%0.2d.bmp', i + 'A' - 1, j);
            fileName2 = sprintf('%s_w%c%0.2d_s05.jpg',plateName, i + 'A' - 1, j); 
            fprintf(htmlOut,'\t<td><div id="pic"><a class="thumbnail"  href="images/%s" target="_blank" ><img src="icons/%s" /><span><img  src="thumbs/%s" /><br></span></a></div></td>\n', fileName2, fileName, fileName2);
            %<td><div id="pic"><a class="thumbnail"  href="bKB01-1O_A01.html" target="_blank" ><img src="bKB01-1O_A01.jpg" /><span><img  src=" bKB01-1O_A01montage.jpg" /><br></span></a></div></td>
        end
    end
    fprintf(htmlOut, '</tr>\n');
end


% footer
fprintf(htmlOut, '<! --MATLAB--> \n</body>\n</html>');

fclose(htmlOut);

function out = plotCurrentRegression2D(f, bins)
% f: 2 x n matrix

% bins = 10;

%normalize data:
f = f./1000;

% format data
f(f<0) = 0;
f(f>1) = 1;

hist2d = zeros(bins);

stepS = 1/bins;

for i=1:size(f, 2)

    b1 = floor(f(1, i) / stepS) + 1;
    b2 = floor(f(2, i) / stepS) + 1;
    if b1 > bins, b1 = bins; end
    if b2 > bins, b2 = bins; end
    
    hist2d(b1, bins-b2+1) = hist2d(b1, bins-b2+1) + 1;
    
end

hist2d = hist2d/max(hist2d(:));

%hist2d = hist2d/80;

out = zeros(bins+2, bins+2, 3);
out(:,:,:) = .25;
 
cmap = (hot(32));

for i=1:bins
    for j=1:bins
        colI = floor(hist2d(i, j) * 32)+1;
        if colI > 32, colI = 32; end
    
        out(i+1, j+1, 1) = cmap(colI, 1);
        out(i+1, j+1, 2) = cmap(colI, 2);
        out(i+1, j+1, 3) = cmap(colI, 3);
    end
end


% --------------------------------------------------------------------
function adaptiveCellSize_OnCallback(hObject, eventdata, handles)
% hObject    handle to adaptiveCellSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global adaptiveCellSize;
global startOfAdaptionCellSize;
global apparentPictureSize;
adaptiveCellSize = 1;
startOfAdaptionCellSize = apparentPictureSize;



% --------------------------------------------------------------------
function adaptiveCellSize_OffCallback(hObject, eventdata, handles)
% hObject    handle to adaptiveCellSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global adaptiveCellSize;
adaptiveCellSize = 0;
