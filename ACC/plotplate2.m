function plotplate2(in);

a=zeros(50*8 + 40, 50*12 + 40, 3);

%% create a circle template
b = zeros(50, 50) * 2000;

[xs, ys] = size(in);


maxi = max(max(in));
mini = min(min(in));
in = (in - mini)./(maxi - mini);

for x=1:50
    
    for y=1:50
    
        dist = (sqrt((x-25)^2 + (y-25)^2));
        
        if (dist < 20)
        
            b(x, y) = 20^3 - dist^3 + 50;
        
        end;
            
    end;

end;

m = max(max(b));

b = (b/m) * 255;

c = colormap('Jet');

for i=1:xs

    for j=1:ys
        
        a((i-1)*50+21:i*50+20, (j-1)*50+21:j*50+20, 1) = b * c(floor(in(i, j)*63)+1, 1);
        a((i-1)*50+21:i*50+20, (j-1)*50+21:j*50+20, 2) = b * c(floor(in(i, j)*63)+1, 2);
        a((i-1)*50+21:i*50+20, (j-1)*50+21:j*50+20, 3) = b * c(floor(in(i, j)*63)+1, 3);
        
    end;

end;

b = find(a == 0);

a(b) = 32;

% figure(1);
% imshow(uint8(a));

imwrite(uint8(a), 'hitmap.bmp');
