function [ idx ] = getIndexInSamplingPool(CommonHandles, imageName, cellNumber )
%getIndexInSamplingPool
%this function gives back the index of a cell in the sampling pool if it is
%in it. The cell is identified by the image name and it's cellNumber
%inside. If we give back -1 it means that the cell is not in the sampling
%pool.
%This search can be changed to a map.

idx = -1;
for i=1:length(CommonHandles.SamplingPool.Mapping)
    if (strcmp(CommonHandles.SamplingPool.Mapping{i}.ImageName,imageName) && cellNumber == CommonHandles.SamplingPool.Mapping{i}.CellNumberInImage)
        idx = i;
    end   
end    
