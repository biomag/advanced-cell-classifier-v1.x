%% Create QC from classified data
%%

clear all;

load('kinome2_md3.mat');

%plates = [1 3 6 9];  
%plates = [2 5 8 11];  
%plates = [4 7 10];  

plates = 1:14;


minCellnum = 100;

hitrate = 0.2;

%hits = [];

for rows = 1:24
    for cols = 1:16
       isHit = 0;        
       hitRates = zeros(11, 1);
        for i = 1:length(plates)            
%            numHit = sum(CommonHandles.Report.ClassResult{plates(i)}(rows, cols, [7 8 9]));
%            numNorm = sum(CommonHandles.Report.ClassResult{plates(i)}(rows, cols, [1 2 3 4 5 7 8 9 10 11 12]));            
%            hits = [hits (numHit/numNorm)];
            if (CommonHandles.Report.CellNumber{plates(i)}(rows, cols) > minCellnum )
                numHit = sum(CommonHandles.Report.ClassResult{plates(i)}(rows, cols, [2]));
                numNorm = sum(CommonHandles.Report.ClassResult{plates(i)}(rows, cols, [1 2]));            
                hitRates(i) = numHit/numNorm;
                if (numHit/numNorm > hitrate)
                    isHit = isHit+1;
                end;

            end;
        end;
        if (isHit >= 0)
            disp([num2str(rows)  ', ' num2str(cols) ', ' num2str(isHit) ', ' num2str(hitRates(1))  ', ' num2str(hitRates(2)) ', ' num2str(hitRates(3)) ', ' num2str(hitRates(4)) ', ' num2str(hitRates(5))  ', ' num2str(hitRates(6)) ', ' num2str(hitRates(7)) ', ' num2str(hitRates(8)) ', ' num2str(hitRates(9))  ', ' num2str(hitRates(10)) ', ' num2str(hitRates(11)) ]);
        end;
    end;
end;