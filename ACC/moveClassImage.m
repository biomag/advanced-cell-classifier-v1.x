function [] = movceClassImage( cellNumber, imageName, clickedClass )
%DELETEFROMCLASSIMAGES Summary of this function goes here
%   Detailed explanation goes here
global CommonHandles;



for i = 1:size(CommonHandles.TrainingSet.CellNumber)
    if (CommonHandles.TrainingSet.CellNumber{i} == cellNumber) && (strcmp(CommonHandles.TrainingSet.ImageName{i},imageName))
        [~,counter] = ismember(1,CommonHandles.TrainingSet.Class{i},'rows');
        className = CommonHandles.Classes{counter}.Name;
        ClassVector = zeros(CommonHandles.MaxCluster,1);
        ClassVector(clickedClass) = 1;
        CommonHandles.TrainingSet.Class{i} = ClassVector;
        
        % Search for classImage to remove
        classImgObj = CommonHandles.ClassImages(className);
        for j = 1:length(classImgObj.Images)
            if (classImgObj.Images{j}.CellNumber == cellNumber) && (strcmp(classImgObj.Images{j}.ImageName,imageName))
                classImgObj.Images(j) = [];
                CommonHandles.ClassImages(className) = classImgObj;
                break;
            end
        end
        
        CommonHandles.Classes{counter}.LabelNumbers = CommonHandles.Classes{counter}.LabelNumbers - 1;
        
        refreshClassesList();
        
        break;
    end
end

end
