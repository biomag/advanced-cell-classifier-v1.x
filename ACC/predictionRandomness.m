function E = predictionRandomness(p)
% this function calculates the mean of the function responses of
% (2x+1)^2+1, which give low valuse for certain and high for uncertain
% decisions

E = zeros(size(p, 1), 1);
for i=1:size(p, 1)
    E(i) = mean( -(2*p(i, :)-1).^2+1  );
end;

% normalize scores
E = E ./ (-(2*(1/size(p, 2))-1).^2+1);