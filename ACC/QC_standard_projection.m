%% Create QC from classified data
%%

clear all;

load('genome_b1_t1.mat');

figure(1);

map = [1, 2, 23, 24];


% cellnumber
figure(1);
for i = 1:length(map)
    for j=1:16
        subplot(4, 16, (i-1)*16+j);
        for k=1:length(CommonHandles.Report.CellNumber)
            cellNum(k) = sum(CommonHandles.Report.ClassResult{k}(map(i), j, [1:2]));
        end;
        plot(cellNum);  
        set(gca,'FontSize',5)        
        ylim([0 10000]);
    end;
end;
max(cellNum(:))
% statistics

% HIT rate
figure(2);
for i = 1:length(map)
    for j=1:16
        subplot(4, 16, (i-1)*16+j);
        for k=1:length(CommonHandles.Report.CellNumber)
            numHit = sum(CommonHandles.Report.ClassResult{k}(map(i), j, [2]));
            numNorm = sum(CommonHandles.Report.ClassResult{k}(map(i), j, [1 2]));            
            cellNum(k) = numHit/numNorm;
        end;
        plot(cellNum);  
        set(gca,'FontSize',5)        
        ylim([0 1]);
    end;
end;


% cellnumber imahe
figure(3);
for i = 1:24
    for j=1:16
        for k=1:length(CommonHandles.Report.CellNumber)
            cellNum(k) = sum(CommonHandles.Report.ClassResult{k}(i, j, [1:4]));
        end;
        gcn(i, j) = median(cellNum);  
    end;
end;
imagesc(gcn);