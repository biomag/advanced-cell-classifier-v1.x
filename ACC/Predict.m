function [out, props] = Predict(method, features, CommonHandles);

if method == 1
    [out, accuracy, props] = svmpredict(ones(1, size(features,2))', features', CommonHandles.TrainingSet.svm);
elseif method == 2
    [props, out]=max(sim(CommonHandles.TrainingSet.nn,features));
elseif method > 2

    [FS N] = size(features);
    CS = length(CommonHandles.TrainingSet.Class{1});
    attributes = weka.core.FastVector(FS+1);
    for i=1:FS
       attributes.addElement(weka.core.Attribute(['feature' num2str(i)]));
    end;

    classvalues = weka.core.FastVector(CS);    
    for i=1:CS
         classvalues.addElement(['class' num2str(i)]);
    end;
    attributes.addElement(weka.core.Attribute('Class', classvalues));

    trainingdata = weka.core.Instances('eval_data', attributes, N);
    trainingdata.setClassIndex(trainingdata.numAttributes() - 1);

    w = 1;
    for i=1:1:N
        observation = weka.core.Instance(w, [features(:, i)' 0]);
        observation.setDataset(trainingdata);                
        trainingdata.add(observation);
    end;
%    CommonHandles.TrainingSet.clRandForest  = classifierRandForest;
    evaluator = weka.classifiers.misc.monotone.InstancesUtil;

    evaluator.classifyInstances(trainingdata, CommonHandles.TrainingSet.clRandForest);
    
    for i=0:1:N-1    
        out(i+1) = trainingdata.instance(i).classValue+1; 
    end;
  
    props = ones(N, 1);    
end;
