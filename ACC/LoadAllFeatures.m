function [ features, mapping, coords ] = LoadAllFeatures( CommonHandles, samplingRatio, originalImages, selectedPlate )
%LoadAllFeatures This is a higher abstraction level to load features from
%an experiment than the readMetaData. It reads all the available features
%and gives back them in a 2 dimensional array. The rows represents the
%objects and the columns the different features.
%   INPUT:  CommonHandles as usual
%           samplingRatio: the rate of cells which we wish to load. To
%           avoid cases when we have to load huge amount of data you can
%           set this to a low value and then a subsampling will be made.
%           selectedPlate: this argument is optional, if you leave it out
%           then we'll load features from all plates. If you provide it
%           then we'll select cells only from that plate.
%           originalImages: set to 1 if mapping should contains the path of
%           the original images, or set to 0 for the segmented images
%   OUTPUT: features the 2D array which contains the object's features. It
%           does not contain the first 2 features which is the location of the
%           cell inside the image.
%           mapping: this is a cell array from which you can read out the
%           necessary data to indentify the cell of which features are
%           located in the features array. The features array has as many
%           rows as many elements the mapping cellarray has. Every cell in
%           mapping contains a structure with 3 fields: the CurrentPlateNumber,
%           the ImageName and the CellNumberInImage which 3 data uniquely
%           identifies a cell
%           coords contains the x,y image coordinated of segmented cells

ft = {};
crds = {};
mapping = {};

if nargin < 4
    selectedPlates = 1:length(CommonHandles.PlatesNames);
else
    selectedPlates = selectedPlate;
end

if ~originalImages
    imageFolder = CommonHandles.ImageFolder;
else
    imageFolder = CommonHandles.OriginalImageFolder;
end

for currentPlateNumber = selectedPlates
    ImageList = dir([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(currentPlateNumber)) filesep imageFolder filesep '*_w*' CommonHandles.ImageExtension]);  %*_wA
    possibleImages = 1:length(ImageList);
    if samplingRatio < 1
        for i=1:ceil(length(ImageList).*samplingRatio)
            %this array contains the image indeces which can be selected to label.
            r = rand;
            while r==0 r=rand; end
            j = ceil(r*length(possibleImages));
            currentImageName = ImageList(possibleImages(j)).name;
            possibleImages(j) = [];
            SelectedImageName = [char(CommonHandles.PlatesNames(currentPlateNumber)) filesep imageFolder filesep currentImageName];
            [~, fileNameexEx, ~, ~] = filepartsCustom(SelectedImageName);
            [~,metaData] = readMetaData(CommonHandles,fileNameexEx,currentPlateNumber);
            disp(fileNameexEx);
            
            featuresTmp = zeros(size(metaData,1), size(metaData, 2)-2);
            coordsTmp = zeros(size(metaData,1), 2);
            l = 1;
            for j=1:size(metaData,1)
                if size(metaData,2)>1
                    featuresTmp(l, :) = metaData(j,3:end);
                    mapping{end+1}.ImageName = SelectedImageName;
                    mapping{end}.CellNumberInImage = j;
                    mapping{end}.CurrentPlateNumber = currentPlateNumber;
                    coordsTmp(l, :) = metaData(j,1:2);
                    l=l+1;
                end
            end
            ft{end+1,1} = featuresTmp;
            crds{end+1,1} = coordsTmp;
        end
    else
        for i=1:ceil(length(ImageList))
            currentImageName = ImageList(possibleImages(i)).name;
            SelectedImageName = [char(CommonHandles.PlatesNames(currentPlateNumber)) filesep imageFolder filesep currentImageName];
            [~, fileNameexEx, ~, ~] = filepartsCustom(SelectedImageName);
            [~,metaData] = readMetaData(CommonHandles,fileNameexEx,currentPlateNumber);
            disp(fileNameexEx);
            if size(metaData,2) == 1
                metaData = metaData';
            end                           
            featuresTmp = zeros(size(metaData,1), size(metaData, 2)-2);            
            coordsTmp = zeros(size(metaData,1), 2);
            l = 1;
            for j=1:size(metaData,1)
                if size(metaData,2)>1
                    featuresTmp(l, :) = metaData(j,3:end);
                    mapping{end+1}.ImageName = SelectedImageName;
                    mapping{end}.CellNumberInImage = j;
                    mapping{end}.CurrentPlateNumber = currentPlateNumber;
                    coordsTmp(l, :) = metaData(j,1:2);
                    l=l+1;
                end
            end                        
            ft{end+1,1} = featuresTmp;
            crds{end+1,1} = coordsTmp;
        end
    end   
    %{
    j=1;
    while j<=length(ft)
        if isempty(ft{j})
            ft(j) = [];
        else
            j=j+1;
        end
    end
    %}
    features = cell2mat(ft);
    coords = cell2mat(crds);
end
end