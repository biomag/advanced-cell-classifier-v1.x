function net = edu_createnn(P,T, CommonHandles)

% if 0 %isfield(CommonHandles.TrainingSet, 'nn')
%     net = CommonHandles.TrainingSet.nn;
%     net.LW{2,1} = net.LW{2,1}*0.1;
%    % net.b{2} = net.b{2}*0.1;
% else
%     alphabet = P;
%     targets = T;
% 
%     [R,Q] = size(alphabet);
%     [S2,Q] = size(targets);
%     S1 = 30;
% 
%     net = newff(minmax(alphabet),[S1 S2],{'logsig' 'logsig' }, 'trainlm');
%     net.LW{2,1} = net.LW{2,1}*0.1;
%     net.b{2} = net.b{2}*0.1;
%     net.performFcn = 'sse';
%     net.trainParam.goal = 1;
%     net.trainParam.show = 10;
%     net.trainParam.epochs = 100000;
%     net.trainParam.mc = 0.1;
%     P = alphabet;
%     T = targets;
% end;

% % % % trainb
% % % % Batch training with weight and bias learning rules
% % % % 
% % % % trainbfg
% % % % BFGS quasi-Newton backpropagation
% % % % 
% % % % trainbfgc
% % % % BFGS quasi-Newton backpropagation for use with NN model reference adaptive controller
% % % % 
% % % % trainbr
% % % % Bayesian regularization
% % % % 
% % % % trainc
% % % % Cyclical order incremental update
% % % % 
% % % % traincgb
% % % % Powell-Beale conjugate gradient backpropagation
% % % % 
% % % % traincgf
% % % % Fletcher-Powell conjugate gradient backpropagation
% % % % 
% % % % traincgp
% % % % Polak-Ribi�re conjugate gradient backpropagation
% % % % 
% % % % traingd
% % % % Gradient descent backpropagation
% % % % 
% % % % traingda
% % % % Gradient descent with adaptive learning rule backpropagation
% % % % 
% % % % traingdm
% % % % Gradient descent with momentum backpropagation
% % % % 
% % % % traingdx
% % % % Gradient descent with momentum and adaptive learning rule backpropagation
% % % % 
% % % % trainlm
% % % % Levenberg-Marquardt backpropagation
% % % % 
% % % % trainoss
% % % % One step secant backpropagation
% % % % 
% % % % trainr
% % % % Random order incremental training with learning functions
% % % % 
% % % % trainrp
% % % % Resilient backpropagation (Rprop)
% % % % 
% % % % trains
% % % % Sequential order incremental training with learning functions
% % % % 
% % % % trainscg
% % % % Scaled conjugate gradient backpropagation

% best --- traingda gdx trainlm

[S2,Q] = size(T);

numHiddenNeurons = 30;  % Adjust as desired
net = newff(P,T,[numHiddenNeurons S2],{'logsig' 'logsig'}, 'trainlm');


% Division of Samples
% (Delete these lines to use default settings of 0.6, 0.2 and 0.2.)
net.divideParam.trainRatio = .8;  % Adjust as desired
net.divideParam.valRatio = 0.2;  % Adjust as desired
net.divideParam.testRatio = 0.0;  % Adust as desired

net.trainParam.show = 1;
net.trainParam.epochs = 1000;

% Random Seed for Reproducing NFTool Results
% (Delete these lines to get different results each time this function is
% called.)
net = init(net);

% Train Network
[net,tr] = train(net,P,T);