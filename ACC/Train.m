function CommonHandles = Train(CommonHandles);

if CommonHandles.SelectedClassifier == 1
    % svm classifier
    % prepare data
    [maxv maxi] = max(cell2mat(CommonHandles.TrainingSet.Class'));
    features = cell2mat(CommonHandles.TrainingSet.Features);
    CommonHandles.TrainingSet.svm = svmtrain(maxi', features, '-e 0.01 -b 1 -c 1 -g 5');
elseif CommonHandles.SelectedClassifier == 2
    % nn classifier
    features = cell2mat(CommonHandles.TrainingSet.Features);
    classes = [];
    for i=1:length(CommonHandles.TrainingSet.Class)
        classes = [classes ; cell2mat(CommonHandles.TrainingSet.Class(i))'];
    end;
    CommonHandles.TrainingSet.nn = edu_createnn(features',classes', CommonHandles);
elseif CommonHandles.SelectedClassifier > 2    
    % random forest weka classifyer
    
    N = length(CommonHandles.TrainingSet.Features);
    FS = length(CommonHandles.TrainingSet.Features{1});    
    % class size
    CS = length(CommonHandles.TrainingSet.Class{1});

    switch CommonHandles.SelectedClassifier
        case 3            
            classifierWekaType = weka.classifiers.functions.Logistic
        case 4
            classifierWekaType = weka.classifiers.functions.MultilayerPerceptron            
        case 5
            classifierWekaType = weka.classifiers.functions.RBFNetwork
        case 6
            classifierWekaType = weka.classifiers.functions.SimpleLogistic            
            classifierWekaType.setOptions(weka.core.Utils.splitOptions('-H 50 -M 500 -A -S -W 0.079267'))
        case 7
            classifierWekaType = weka.classifiers.functions.SMO;
            classifierWekaType.setOptions(weka.core.Utils.splitOptions('-K "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0"'))
        case 8
            classifierWekaType = weka.classifiers.meta.RandomCommittee
        case 9
            classifierWekaType = weka.classifiers.trees.RandomForest                                                
        case 10            
            classifierWekaType = weka.classifiers.bayes.BayesNet
        case 11
            classifierWekaType = weka.classifiers.bayes.NaiveBayes
        case 12
            classifierWekaType = weka.classifiers.meta.AdaBoostM1
        case 13
            classifierWekaType = weka.classifiers.meta.Bagging
        case 14
            classifierWekaType = weka.classifiers.meta.Dagging
        case 15
            classifierWekaType = weka.classifiers.meta.END
        case 16
            classifierWekaType = weka.classifiers.meta.EnsembleSelection                        
        case 17
            classifierWekaType = weka.classifiers.meta.LogitBoost
        case 18
            classifierWekaType = weka.classifiers.trees.BFTree
        case 19
            classifierWekaType = weka.classifiers.trees.FT
        case 20
            classifierWekaType = weka.classifiers.trees.J48
        case 21
            classifierWekaType = weka.classifiers.trees.RandomTree
        case 22
            classifierWekaType = weka.classifiers.trees.REPTree                                   
        case 23
            classifierWekaType = weka.classifiers.meta.Vote;
            classifierWekaType.setOptions(weka.core.Utils.splitOptions('-S 1 -B "weka.classifiers.functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a" -B "weka.classifiers.functions.SimpleLogistic -I 0 -M 500 -H 50 -W 0.0" -B "weka.classifiers.functions.SMO -C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0\"" -R AVG'))
        case 24
            classifierWekaType = weka.classifiers.meta.Vote;
            classifierWekaType.setOptions(weka.core.Utils.splitOptions('-S 1 -B "weka.classifiers.functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a" -B "weka.classifiers.functions.SimpleLogistic -I 0 -M 500 -H 50 -W 0.0" -B "weka.classifiers.functions.SMO -C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0\"" -R MAJ'))
        case 25
            classifierWekaType = weka.classifiers.meta.Vote;
            classifierWekaType.setOptions(weka.core.Utils.splitOptions('-S 1 -B "weka.classifiers.functions.MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a" -B "weka.classifiers.functions.SimpleLogistic -I 0 -M 500 -H 50 -W 0.0" -B "weka.classifiers.functions.SMO -C 1.0 -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K \"weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3.0\"" -B "weka.classifiers.meta.RandomCommittee -S 1 -I 10 -W weka.classifiers.trees.RandomTree -- -K 0 -M 1.0 -S 1" -B "weka.classifiers.trees.RandomForest -I 10 -K 0 -S 1" -R AVG'))                
        case 26
            % Nested dichotomes combining 2 class classifiers for Giuseppe
            classifierWekaType = weka.classifiers.meta.END;
            classifierWekaType.setOptions(weka.core.Utils.splitOptions('-S 1 -I 10 -W weka.classifiers.meta.nestedDichotomies.ND -- -S 1 -W weka.classifiers.trees.RandomForest -- -I 10 -K 0 -S 1'));
        otherwise
            classifierWekaType = weka.classifiers.functions.MultilayerPerceptron;
    end
    
    attributes = weka.core.FastVector(FS+1);
    for i=1:FS
        attributes.addElement(weka.core.Attribute(['feature' num2str(i)]));
    end;

    classvalues = weka.core.FastVector(CS);    
    for i=1:CS
        classvalues.addElement(['class' num2str(i)]);
    end;
    attributes.addElement(weka.core.Attribute('Class', classvalues));

    trainingdata = weka.core.Instances('training_data', attributes, N);
    trainingdata.setClassIndex(trainingdata.numAttributes() - 1);

    for i=1:1:N
        observation = weka.core.Instance(FS+1);
        for j=1:FS
            observation.setValue(trainingdata.attribute(['feature' num2str(j)]), CommonHandles.TrainingSet.Features{i}(j));
        end;
        observation.setDataset(trainingdata);
        [maxv, classLabel] = max(CommonHandles.TrainingSet.Class{i}(:));

        observation.setClassValue(['class' num2str(classLabel)]);
        trainingdata.add(observation);
    end;
    classifierWekaType.buildClassifier(trainingdata);     
    CommonHandles.TrainingSet.clRandForest  = classifierWekaType;
    disp('Weka classifier training done...');
end;
