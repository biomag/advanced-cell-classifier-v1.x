well = 'K006_K24'

folderPlate = 'x:\Stoffel\Ferdinand\Kinome\raw2\K006\anal2\';


counter = 0;
for x = 1:24
    for y=1:16
        counter = counter + 1;
        if x<10
            well = ['K006_' 'A'+y-1 '0' num2str(x)]
        else
            well = ['K006_' 'A'+y-1 num2str(x)]
        end;
        dirList = dir([folderPlate well '*.txt']);

        a=[];
        
        for i=1:length(dirList)

            data = load([folderPlate dirList(i).name]);
            a = [a' data(:, [3 15])']';
        
        end;

        if (size(a, 1) > 0)
        
            figure(1);
            subplot(24, 16, counter);
            plot(a(:,1),a(:,2), 'b.');
            axis([0.002 0.018 0.002 0.018]);

            figure(2);
            subplot(24, 16, counter);
            ih = hist(a(:,1)./a(:,2), 0:0.1:10);
            ih = ih ./ max(ih)
            plot(0:0.1:10, ih);
            axis([0 5 0 1]);
            meanG(x, y) = mean(a(:,1));
            meanR(x, y) = mean(a(:,2));
            meanRat(x, y) = meanG(x, y)/ meanR(x, y);
            figure(3);
            subplot(2, 2, 1); imagesc(meanG);
            subplot(2, 2, 2); imagesc(meanR);
            subplot(2, 2, 3); imagesc(meanRat);
        end;        
    end;
end;
