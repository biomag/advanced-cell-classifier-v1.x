%readMetaData: given a CommonHandles, and a fileName within the
%selectedPlate this function will return the features for this image. If
%the last argument (plateNumber is not provided), then the
%CommonHandles.selectedPlate will be used. 
function [SelectedMetaDataFileName, SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, plateNumber)

if nargin == 2
    plateNumber = CommonHandles.SelectedPlate;
end

% read txt file
 if  CommonHandles.DataStructureType == 1     
     SelectedMetaDataFileName = [char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.txt'];
     if exist(SelectedMetaDataFileName, 'file')         
         SelectedMetaData = load([CommonHandles.DirName filesep SelectedMetaDataFileName]);
     else
         SelectedMetaDataFileName = [filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx  '.tif.txt'];         
         SelectedMetaData = load([CommonHandles.DirName SelectedMetaDataFileName]);
     end
     
     % read hdf5
 elseif CommonHandles.DataStructureType == 2
    
     SelectedMetaDataFileName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.tif.txt'];   
     try
        SelectedMetaData = hdf5read([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) '.h5'], ['/' fileNameexEx]);
     catch e        
        SelectedMetaData = hdf5read([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) '.h5'], ['/' fileNameexEx '.tif']);         
     end
 end
