clear all;

file_list = dir('badewanne.mat');

for j=1:size(file_list, 1)

    file_list(j).name
    
    load(file_list(j).name)
    
    CommonHandles.SelectedClassifier = 2;
    
    CommonHandles = Train(CommonHandles);

    classifier = CommonHandles.TrainingSet.nn;
    
    save(['nn_' file_list(j).name], 'classifier');
    
    clear CommonHandles;
    
end;
    
