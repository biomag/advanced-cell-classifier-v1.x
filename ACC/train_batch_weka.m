clear all;

file_list = dir('b_all.mat');

for j=1:size(file_list, 1)

    file_list(j).name
    
    load(file_list(j).name)
    
    CommonHandles.SelectedClassifier = 9;
    
    CommonHandles = Train(CommonHandles);

    classifier = CommonHandles.TrainingSet.clRandForest;
    
    save(['weka_' file_list(j).name], 'classifier');
    
    clear CommonHandles;
    
end;
    
