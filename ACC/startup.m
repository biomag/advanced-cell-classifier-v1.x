%STARTUP script

addpath(genpath(pwd));
addpath('GUI');
addpath('Utils');
addpath(['Utils' filesep 'HierarchicalClustering']);
addpath(['Utils' filesep 'LowProbPred_occ']);
addpath(['Utils' filesep 'OneClassClassifier']);
addpath(['Utils' filesep 'OneClassClassifier' filesep 'SyntheticData']);
addpath(['Utils' filesep 'OneClassClassifier' filesep 'testCases']);
addpath(['Utils' filesep 'weka']);

ACC_main_GUI()