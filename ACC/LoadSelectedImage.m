function CommonHandles = LoadSelectedImage(CommonHandles)

% f_handles.classRes = [];
% 
% f_handles.datafileName = [f_handles.dataPath fileList(pos, :)];
% 
% f_handles.currentData = load(f_handles.datafileName);
% 
% cell_list_str = [];
% for i=1:size(f_handles.currentData)
%     cell_list_str = [cell_list_str; i];
% end;
% set(handles.listbox2, 'String', cell_list_str);
% set(handles.listbox2, 'Value', 1);
% 
[pathstr, fileNameexEx, ext, versn] = filepartsCustom(CommonHandles.SelectedImageName);
% CommonHandles.SelectedMetaDataFileName = [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.tif.txt'];
% CommonHandles.SelectedMetaData = load(CommonHandles.SelectedMetaDataFileName);
% 
% [pathstr, fileNameexEx, ext, versn] = fileparts(CommonHandles.SelectedImageName);
% 
% if ~CommonHandles.isHDF5Container
%     CommonHandles.SelectedMetaDataFileName = [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.tif.txt'];
%     CommonHandles.SelectedMetaData = load(CommonHandles.SelectedMetaDataFileName);
% else
%     CommonHandles.SelectedMetaDataFileName = [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.tif.txt'];    
%     CommonHandles.SelectedMetaData = hdf5read([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.MetaDataFolder filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) '.h5'], ['/' fileNameexEx '.tif']);
% end;


[CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx);

if CommonHandles.SelectedCell < 0 || CommonHandles.SelectedCell > size(CommonHandles.SelectedMetaData, 1) || (CommonHandles.RandomTrain == 0 && (~isfield(CommonHandles,'ActiveLearning') || ~CommonHandles.ActiveLearning ))
    CommonHandles.SelectedCell = 1;
end;
% [pathstr, fileNameexEx, ext, versn] = fileparts(fileNameexEx);
% 
% f_handles.imageFileName = [f_handles.imagePath fileNameexEx '.png'];
% 
CommonHandles.CurrentImage = imread([CommonHandles.DirName filesep CommonHandles.SelectedImageName]);
CommonHandles.CurrentImageSizeX = size(CommonHandles.CurrentImage,1);
CommonHandles.CurrentImageSizeY = size(CommonHandles.CurrentImage,2);
CommonHandles = PlotCurrentHistogram(CommonHandles);

% Storing bounds of visible part of the current image
CommonHandles.MainView.XBounds = [1 CommonHandles.CurrentImageSizeY];
CommonHandles.MainView.YBounds = [1 CommonHandles.CurrentImageSizeX];

CommonHandles.CurrentOriginalImageNoTouch = imread([CommonHandles.DirName filesep CommonHandles.SelectedOriginalImageName]);
 
 I = imread([CommonHandles.DirName filesep CommonHandles.SelectedOriginalImageName]);
 if CommonHandles.MainView.StrechImage == 1

     % Needs to be verifyed this saturation value
     tolr = 0.0;
     tolg = 0.001;
     tolb = 0.005;

     sl(:,1) = [0,1]';%stretchlim(I(:,:,1), tolr); %smalls = find(sl(2,:) < 0.05); sl(2,smalls) = 1;
     sl(:,2) = stretchlim(I(:,:,2), tolg);
     sl(:,3) = stretchlim(I(:,:,3), tolb);
     CommonHandles.CurrentOriginalImage = imadjust(I,sl,[]);
 else
     sl(:, 1)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
     sl(:, 2)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
     sl(:, 3)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
     CommonHandles.CurrentOriginalImage = imadjust(I,sl,[]);
 end;


CommonHandles.CurrentPrediction = {};

CommonHandles = setImageIntensity(CommonHandles);

CommonHandles = ShowSelectedCell(CommonHandles);