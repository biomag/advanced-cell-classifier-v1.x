function varargout = ACC_main_GUI(varargin)
% ACC_MAIN_GUI M-file for ACC_main_GUI.fig
%      ACC_MAIN_GUI, by itself, creates a new ACC_MAIN_GUI or raises the existing
%      singleton*.
%
%      H = ACC_MAIN_GUI returns the handle to a new ACC_MAIN_GUI or the handle to
%      the existing singleton*.
%
%      ACC_MAIN_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ACC_MAIN_GUI.M with the given input arguments.
%
%      ACC_MAIN_GUI('Property','Value',...) creates a new ACC_MAIN_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ACC_main_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ACC_main_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ACC_main_GUI

% Last Modified by GUIDE v2.5 19-Feb-2016 14:42:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ACC_main_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ACC_main_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before ACC_main_GUI is made visible.
function ACC_main_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ACC_main_GUI (see VARARGIN)

% Choose default command line output for ACC_main_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ACC_main_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global CommonHandles;

TitleImage = imread(['img' filesep 'title.bmp']);
imagesc(TitleImage, 'Parent', handles.mainView);

CommonHandles.VersionInfo = 1.4;
% initialize SALT, open the ini file, find SALT path and add to MatLab
% search path
% initialize SALT and get the list of classifiers
CommonHandles.SALT.initialized= 0;
CommonHandles = parseIniFile(CommonHandles);
if CommonHandles.SALT.initialized
    sacInit(CommonHandles.SALT.folderName);
    disp('Suggest a learner toolbox (SALT) successfully initialized');
    global CommonHandles; %#ok<*REDEF> %redef is required because sacInit destroys all global variables;    
    CommonHandles.ClassifierNames = sacGetSupportedList('classifiers');
else
    CommonHandles.ClassifierNames = {'LibSVM' 'Neural Network' 'Logistic.Weka' ...
    'MultilayerPerceptron.Weka' 'RBFNetwork.Weka' 'SimpleLogistic.Weka' 'SMO.Weka' 'RandomCommittee.Weka' 'RandomForest.Weka' ...
    'BayesNet.Weka' 'NaiveBayes.Weka' 'AdaBoostM1.Weka' 'Bagging.Weka' 'Dagging.Weka' 'END.Weka' 'EnsembleSelection.Weka' ...
    'LogitBoost.Weka' 'BFTree.Weka' 'FT.Weka' 'J48.Weka' 'RandomTree.Weka' 'REPTree.Weka'};
end;

commonHandlesVersionControl(CommonHandles, handles);


% --- Outputs from this function are returned to the command line.
function varargout = ACC_main_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
% PREDICT callback
function uipushtool3_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

CommonHandles.CurrentPrediction = {};
CommonHandles.CurrentProps = {};

% for i=1:size(CommonHandles.SelectedMetaData, 1)
%     features(i,:) = CommonHandles.SelectedMetaData(i, 3:size(CommonHandles.SelectedMetaData, 2));    
% end
features = CommonHandles.SelectedMetaData(:,3:end);

if CommonHandles.SALT.initialized    
    CommonHandles.SALT.trainingData.instances = features;
    CommonHandles.SALT.trainingData.labels = ones(size(features, 1));
    if strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
        [out, ~ ,props] = svmpredict(ones(size(features,1),1), CommonHandles.SALT.trainingData.instances, CommonHandles.SALT.model);
                
        out(out<0) = 2;
    else
        [out,props] = sacPredict(CommonHandles.SALT.model, CommonHandles.SALT.trainingData);
    end
else
    [out, props] = Predict(CommonHandles.SelectedClassifier , features', CommonHandles);
end

% print out percentages
classString = [];
for i=1:size(props, 2)
    classString = [classString 'C' num2str(i) '=' num2str((length(find(out == i))/length(out))*100) '%; '];
end

fprintf('Distribution of cell numbers among classes on the selected image:\n');
disp(classString); 

CommonHandles.CurrentPrediction = num2cell(out);
CommonHandles.CurrentProps = num2cell(props);

CommonHandles = ShowSelectedCell(CommonHandles);


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global CommonHandles;


dir_list = dir(['img' filesep 'type*.bmp']);

offset = 10;

CommonHandles.ClassHierarchy = zeros(size(dir_list, 1));

if exist(['img' filesep 'type.txt'])
    ftNamesTxt = fopen(['img' filesep 'type.txt'], 'r');
    customFTNames = 1;
else
    customFTNames = 0;    
end;
   
% --------------------------------------------------------------------
% pushtool for creating classes, placed on menubar
function createClassPushTool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to createClassPushTool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
CommonHandles.HC.Node.createClass = 0;

uiwait(createClasses_GUI);  


% --- Executes during object creation, after setting all properties.
function mainView_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mainView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate mainView


% --------------------------------------------------------------------
function uipushtool7_ClickedCallback(hObject, eventdata, handles)
% NEWPROJECT pushtool
% hObject    handle to uipushtool7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

% ask user for the data set
load_data_setGUI;
uiwait(gcf); 
if (CommonHandles.Success == 1)
    CommonHandles.PlateSelectWindow = select_plate_wellGUI();

    % create dataset
    CommonHandles.TrainingSet.MetaDataFilename = {};
    CommonHandles.TrainingSet.ImageName = {};
    CommonHandles.TrainingSet.CellNumber = {};
    CommonHandles.TrainingSet.Class = {};    
    CommonHandles.TrainingSet.Features = {};    
end;

% --- Executes on mouse press over axes background.
function mainView_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to mainView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

% position of pointer in coordinates of mainView axes
pt = get(handles.mainView,'CurrentPoint');

% limits of actual axes
xLimits = xlim(handles.mainView);
yLimits = ylim(handles.mainView);

% position of pointer in coordinates of axes6 axes
ptHist = get(handles.axes6,'CurrentPoint');

% check if the click happened on the main image (between axes limits)
if (pt(1, 1) > xLimits(1)) && (pt(1, 1) < xLimits(2)) ...
        && (pt(1, 2) > yLimits(1)) && (pt(1, 2) < yLimits(2)) ...
        && (CommonHandles.Success == 1) && (CommonHandles.RandomTrain ~= 2)
    
    % select closest point
    dist = (pt(1, 1) + CommonHandles.MainView.XBounds(1) - CommonHandles.SelectedMetaData(:, 1)).^2 + (pt(1, 2) + (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)) - CommonHandles.SelectedMetaData(:, 2)).^2;
    [minv, CommonHandles.SelectedCell] = min(dist);
    
    % refresh view
    CommonHandles = ShowSelectedCell(CommonHandles);
    
elseif (ptHist(1, 1) > 0) && (ptHist(1, 1) < 128) ...
        && (ptHist(1, 2) > 0) && (ptHist(1, 2) < 64) && (CommonHandles.Success == 1)
    MsButton=get(gcf,'SelectionType');
    if strcmp(MsButton, 'normal')
        if (ptHist(1, 1) * 2 < CommonHandles.MainView.StrechMax)
            CommonHandles.MainView.StrechMin = ptHist(1, 1) * 2;
        end
    else        
        if (ptHist(1, 1) * 2 > CommonHandles.MainView.StrechMin)
            CommonHandles.MainView.StrechMax = ptHist(1, 1) * 2;   
        end
    end;
    CommonHandles = LoadSelectedImage(CommonHandles);
end
    
% --------------------------------------------------------------------
function uipushtool9_ClickedCallback(hObject, eventdata, handles)
%OPENPROJECT PUSHTOOL
% hObject    handle to uipushtool9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

runningCommonHandles = CommonHandles;

[file,path] = uigetfile('*.mat','Select workspace');
if isequal(file,0)
else
    for i=1:length(CommonHandles.Classes)
        delete(CommonHandles.Classbuttons{1});
        delete(CommonHandles.ClassTextFields{1});
        delete(CommonHandles.ShowTrainedCells{1});
        CommonHandles.Classbuttons(1) = [];
        CommonHandles.ClassTextFields(1) = [];
        CommonHandles.ShowTrainedCells(1) = [];
    end
    
    load([path filesep file]);
    
    %check whether the used data path exists
    if ~exist(CommonHandles.DirName,'dir')
        CommonHandles.DirName = uigetdir(CommonHandles.DirName,'Data path does not exist. Specify a new data path!');
    end
    
    try
        commonHandlesVersionControl(runningCommonHandles,handles);    
        recoverGUI(handles,file);
        select_plate_wellGUI;        
        refreshClassesList;
    catch e
        warning(getReport(e));
        CommonHandles = runningCommonHandles;
    end
end

CommonHandles.PlateSelectWindow = select_plate_wellGUI();


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        button = questdlg('Save workspace before quit?','Save','Save','Quit','Cancel','Save');
        if strcmp(button, 'Save')
            uipushtool8_ClickedCallback(hObject, eventdata, handles);
        elseif strcmp(button, 'Cancel')
            return;
        end;
    end;
end;

delete(hObject);
clear all;
close all;


% --------------------------------------------------------------------
function uitoggletool3_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if (CommonHandles.Success == 1)
    CommonHandles.ShowCellNumbers = 0;
    CommonHandles = ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function uitoggletool3_OnCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if (CommonHandles.Success == 1)
    CommonHandles.ShowCellNumbers = 1;
    CommonHandles = ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function uitoggletool2_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

CommonHandles.RandomTrain = 0;
set(handles.uitoggletool2,'State','off');


% --------------------------------------------------------------------
function uitoggletool2_OnCallback(hObject, eventdata, handles)
%RANDOM train toggle tool
% hObject    handle to uitoggletool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

CommonHandles.RandomTrain = 1;
cleverGuessToggletool_OffCallback(hObject,eventdata, handles);


% --------------------------------------------------------------------
function uipushtool5_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


train_algorithmGUI;
uiwait(gcf); 


% --------------------------------------------------------------------
function uipushtool8_ClickedCallback(hObject, eventdata, handles)
% SAVEPROJECT_PUSHTOOL
% hObject    handle to uipushtool8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

[file,path] = uiputfile('*.mat','Save Plateset As');
CommonHandles.ProjectName = file;
if isequal(file,0)
else
    CurrentOriginalImage = CommonHandles.CurrentOriginalImage;
    CurrentImage = CommonHandles.CurrentImage;        
    CommonHandles.CurrentOriginalImage = [];
    CommonHandles.CurrentImage = [];
    
    CommonHandlesToReserve = CommonHandles;
    
    %Clear the saved GUI objects, we'll use the old matlab GUI savings.
    CommonHandles.MainViewHandle = [];
    CommonHandles.MainWindow = [];
    CommonHandles.SmallColorViewHandle = [];
    CommonHandles.SmallRedViewHandle   = [];
    CommonHandles.SmallGreenViewHandle = [];
    CommonHandles.SmallBlueViewHandle  = [];
    CommonHandles.HistogramViewHandle  = [];
    CommonHandles.Classbuttons = [];   
    save([path '/' file], 'CommonHandles');
    
    CommonHandles = CommonHandlesToReserve;
    set(CommonHandles.MainWindow, 'Name', ['Advanced Cell Classifier - ' CommonHandles.ProjectName] );
    CommonHandles.CurrentOriginalImage = CurrentOriginalImage;
    CommonHandles.CurrentImage = CurrentImage;    
end;


% --------------------------------------------------------------------
function uipushtool4_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

create_reportGUI;
uiwait(gcf); 


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

global CommonHandles;

CommonHandles.ColorView = get(handles.checkbox1,'Value');

if (CommonHandles.Success == 1)
    CommonHandles = ShowSelectedCell(CommonHandles);
end;
    

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

global CommonHandles;

CommonHandles.ShowContour = get(handles.checkbox2,'Value');

if (CommonHandles.Success == 1)
    CommonHandles = ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% New project menu button

uipushtool7_ClickedCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Open project menu button


uipushtool9_ClickedCallback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save project menu
uipushtool8_ClickedCallback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function Untitled_5_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Exit menu 
close;

% --------------------------------------------------------------------
function Untitled_6_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_7_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uipushtool3_ClickedCallback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function Untitled_8_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% predict plates menu
uipushtool4_ClickedCallback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function Untitled_9_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% training properties menu
uipushtool5_ClickedCallback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function Untitled_10_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_11_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.cleverGuessToggletool, 'State'), 'on')
    set(handles.cleverGuessToggletool, 'State', 'off');
    uitoggletool1_OffCallback(hObject, eventdata, handles);
else
    set(handles.cleverGuessToggletool, 'State', 'on');
    uitoggletool1_OnCallback(hObject, eventdata, handles);
end;

% --------------------------------------------------------------------
function Untitled_12_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.uitoggletool2, 'State'), 'on')
    set(handles.uitoggletool2, 'State', 'off');
    uitoggletool2_OffCallback(hObject, eventdata, handles);
else
    set(handles.uitoggletool2, 'State', 'on');
    uitoggletool2_OnCallback(hObject, eventdata, handles);
end;
% --------------------------------------------------------------------
function Untitled_13_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(get(handles.uitoggletool3, 'State'), 'on')
    set(handles.uitoggletool3, 'State', 'off');
    uitoggletool3_OffCallback(hObject, eventdata, handles);
else
    set(handles.uitoggletool3, 'State', 'on');
    uitoggletool3_OnCallback(hObject, eventdata, handles);
end;

% --------------------------------------------------------------------
function Untitled_14_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_15_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~get(handles.checkbox1, 'Value')
    set(handles.checkbox1, 'Value', 1);
else
    set(handles.checkbox1, 'Value', 0);
end;
    
checkbox1_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function Untitled_16_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~get(handles.checkbox2, 'Value')
    set(handles.checkbox2, 'Value', 1);
else
    set(handles.checkbox2, 'Value', 0);
end;
    
checkbox2_Callback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function Untitled_17_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_18_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% starts a folder selector and adds a folder to the list

global CommonHandles;

% create string list
% list of the folder containing the plates and exclude already chosen ones

DirListStr = [];
DirList = dir(CommonHandles.DirName);
DirStr = {DirList.name};
for i=3:size(DirList, 1)
    if exist([char(CommonHandles.DirName) filesep char(DirStr(i))]) == 7
        DirListStr = [DirListStr; cellstr(DirStr(i))];
    end;
end;

DirListStr = setdiff(DirListStr, CommonHandles.PlatesNames);

[SelectedPlates,isOK] = listdlg('PromptString','Select plate(s):', 'Name', 'Add new plates', 'SelectionMode','multiple','ListString', DirListStr);

if (isOK)
    CommonHandles.PlatesNames = [CommonHandles.PlatesNames DirListStr(SelectedPlates)];
    CommonHandles.PlatesNames = sort(CommonHandles.PlatesNames);    
    select_plate_wellGUI;
end;


    
% --------------------------------------------------------------------
function Untitled_19_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Here one can import already labeled data
global CommonHandles;

matFileName = uigetfile('*.mat','Select the project file containing labeled data.');

importData = load(matFileName);

CommonHandles.TrainingSet.MetaDataFilename  = [CommonHandles.TrainingSet.MetaDataFilename; importData.CommonHandles.TrainingSet.MetaDataFilename];
CommonHandles.TrainingSet.ImageName         = [CommonHandles.TrainingSet.ImageName; importData.CommonHandles.TrainingSet.ImageName];
CommonHandles.TrainingSet.CellNumber        = [CommonHandles.TrainingSet.CellNumber; importData.CommonHandles.TrainingSet.CellNumber];
CommonHandles.TrainingSet.Class             = [CommonHandles.TrainingSet.Class; importData.CommonHandles.TrainingSet.Class];
CommonHandles.TrainingSet.Features          = [CommonHandles.TrainingSet.Features; importData.CommonHandles.TrainingSet.Features];


% --------------------------------------------------------------------
function Untitled_20_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

if length(CommonHandles.PlatesNames) > 1

    [SelectedPlates,isOK] = listdlg('PromptString','Select plate(s):', 'Name', 'Remove plates', 'SelectionMode','multiple','ListString', CommonHandles.PlatesNames);

    if (isOK)

       if length(SelectedPlates) < length(CommonHandles.PlatesNames)
        % ask to be sure
        button = questdlg('Do you really want to delete selected plates from the experiment?','Delete...') ;

        if strcmp(button, 'Yes')
            CommonHandles.PlatesNames = setdiff(CommonHandles.PlatesNames, CommonHandles.PlatesNames(SelectedPlates));
            CommonHandles.SelectedPlate = 1;
            CommonHandles.SelectedImage = 1;
            CommonHandles.SelectedCell  = 1;
            set(CommonHandles.PlateListHandle,'Value', CommonHandles.SelectedPlate);
            select_plate_wellGUI;
        end;

       else
            h = msgbox('It is not possible to delete all the plates...', 'Delete warning', 'warn');           
       end;
    
    end;

else
    h = msgbox('This experiment containes not enough plates to delete...', 'Delete warning', 'warn');
end;


% --------------------------------------------------------------------
function Untitled_21_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_22_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% Create new random data set
global CommonHandles;

TrainingDataSetSizeS = inputdlg('Size of the training database:', 'Random training properties',1, {'1000'});

if ~isempty(TrainingDataSetSizeS)
    TrainingDataSetSize = str2num(cell2mat(TrainingDataSetSizeS));
    % detect all the cells
    plateNum = length(CommonHandles.PlatesNames);
    for i=1:plateNum
        ImageList{i} = dir([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(i)) filesep CommonHandles.ImageFolder filesep char(CommonHandles.PlatesNames(i)) '*.png']);
        imageNum(i) = length(ImageList{i});
    end;
    
    RandomData.lut(1:2:2*TrainingDataSetSize) = 1:TrainingDataSetSize;
    RandomData.lut(2:2:2*TrainingDataSetSize) = randperm(TrainingDataSetSize);
    
    waitBarHandle = waitbar(0,'Creating random set...');    
    for i=1:TrainingDataSetSize        
        currentPlate = ceil(plateNum*rand(1,1));
        currentImage = ceil(imageNum(currentPlate)*rand(1,1));
        [pathstr, fileNameexEx, ext, versn] = filepartsCustom(ImageList{currentPlate}(currentImage).name);
        MetaDataFileName = [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(currentPlate)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.tif.txt'];
        ImageFileName = [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(currentPlate)) filesep CommonHandles.ImageFolder filesep fileNameexEx '.png'];
        OriginalImageFileName = [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(currentPlate)) filesep CommonHandles.OriginalImageFolder filesep fileNameexEx '.png'];               
        SelectedMetaData = hdf5read([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(currentPlate)) filesep CommonHandles.MetaDataFolder filesep char(CommonHandles.PlatesNames(currentPlate)) '.h5'], ['/' fileNameexEx '.tif']);                        
%        SelectedMetaData = load(MetaDataFileName);
        currentCell = ceil(size(SelectedMetaData, 1)*rand(1,1));  
        current = find(RandomData.lut == i);
        
        RandomData.Plate(current(1)) = currentPlate;        
        RandomData.Image(current(1)) = currentImage;
        RandomData.Cell(current(1)) = currentCell;
        RandomData.ImageName{current(1)} = ImageFileName;
        RandomData.OriginalImageName{current(1)} = OriginalImageFileName;        
        RandomData.MetaDataFileName{current(1)} = MetaDataFileName;
        RandomData.Plate(current(2)) = currentPlate;        
        RandomData.Image(current(2)) = currentImage;
        RandomData.Cell(current(2)) = currentCell;
        RandomData.ImageName{current(2)} = ImageFileName;
        RandomData.OriginalImageName{current(2)} = OriginalImageFileName;        
        RandomData.MetaDataFileName{current(2)} = MetaDataFileName;
                
        donePercent = double(i/TrainingDataSetSize);
        waitText = sprintf('%d%% done...', int16(donePercent * 100));
        waitbar(donePercent, waitBarHandle, waitText);
    end;
    close(waitBarHandle);
    
    [file,path] = uiputfile('RandomOrder.mat','Save file name');

    if file~=0    
        save([path file], 'RandomData');        
    end;
end;

% --------------------------------------------------------------------
function Untitled_23_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
%% start randomized training

% open data file
[FileName,PathName] = uigetfile('*.mat','Select the randomized training set file.');

if FileName ~= 0
    
    RandomData = load([PathName FileName]);    
    CommonHandles.RandomData = RandomData;    
    CommonHandles.RandomData.Counter = 1;    
    CommonHandles.RandomTrain = 2;

end;


% --------------------------------------------------------------------
function uitoggletool6_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
CommonHandles.MainView.Red = 0;
if (CommonHandles.Success == 1)
    ShowSelectedCell(CommonHandles);
end;

% --------------------------------------------------------------------
function uitoggletool6_OnCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
CommonHandles.MainView.Red = 1;
if (CommonHandles.Success == 1)
    ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function uitoggletool4_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
CommonHandles.MainView.Green = 0;
if (CommonHandles.Success == 1)
    ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function uitoggletool4_OnCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
CommonHandles.MainView.Green = 1;
if (CommonHandles.Success == 1)
    ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function uitoggletool5_OffCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
CommonHandles.MainView.Blue = 0;
if (CommonHandles.Success == 1)
    ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function uitoggletool5_OnCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
CommonHandles.MainView.Blue = 1;
if (CommonHandles.Success == 1)
    ShowSelectedCell(CommonHandles);
end;


% --------------------------------------------------------------------
function Untitled_24_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --------------------------------------------------------------------
function Untitled_25_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_26_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in StrechIntensityCheckBox.
function StrechIntensityCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to StrechIntensityCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of StrechIntensityCheckBox
global CommonHandles;

CommonHandles.MainView.StrechImage = get(hObject,'Value');
selectedCell = CommonHandles.SelectedCell;
CommonHandles = LoadSelectedImage(CommonHandles);
CommonHandles.SelectedCell = selectedCell;
CommonHandles = ShowSelectedCell(CommonHandles);


% --- Executes on mouse press over axes background.
function axes6_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_27_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

newFolder = uigetdir(CommonHandles.DirName);

if newFolder ~= 0
    CommonHandles.DirName = newFolder;
    CommonHandles.SelectedPlate = 1;
    CommonHandles.SelectedImage = 1;
    select_plate_wellGUI;
end;


% --------------------------------------------------------------------
function uipushtool11_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% store current image as reference
global CommonHandles;

prompt={'Enter your comment here:'};
name='Add new reference image';
numlines=1;
[pathstr, name, ext] = filepartsCustom(CommonHandles.SelectedOriginalImageName);
defaultanswer={name};
options.Resize='on';
answer=inputdlg(prompt,name,numlines,defaultanswer,options);

if isfield(CommonHandles, 'ReferenceImages')
    CommonHandles.ReferenceImages{end+1}.Image = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep name ext];
    CommonHandles.ReferenceImages{end}.Comment = answer;
else
    CommonHandles.ReferenceImages{1}.Image = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep name ext];
    CommonHandles.ReferenceImages{1}.Comment = answer;
end


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

% resize function
% do not allow to resize it smaller than half binned image (1392, 1040)
minSize = [946 620];
posRect = get(hObject, 'Position');

if posRect(4) < minSize(2) || posRect(3) < minSize(1)
    posRect(1) = 50;
    posRect(2) = 50;
    posRect(3) = minSize(1);
    posRect(4) = minSize(2);
end;

set(hObject, 'Position', posRect);
posRect = get(hObject, 'Position');

% Aling right controls to the edge
% main window
if isfield(CommonHandles, 'MainViewHandle')
    mainRect = get(CommonHandles.MainViewHandle, 'Position');
    set(CommonHandles.MainViewHandle, 'Position', [mainRect(1) mainRect(2) posRect(3)-150 posRect(4)-100]);
    set(handles.uipanel2, 'Position', [(posRect(3) - 135) / posRect(3) 0.01 (130 / posRect(3)) 0.98]);
    subRect = get(handles.uipanel2, 'Position');
end;
    
% --------------------------------------------------------------------
function Untitled_28_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Add reference image



% --------------------------------------------------------------------
function Untitled_29_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function uipushtool12_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% show reference image list
global CommonHandles;

if isfield(CommonHandles, 'ReferenceImages')
    for i=1:length(CommonHandles.ReferenceImages)
        referenceList{i}= char(CommonHandles.ReferenceImages{i}.Comment);
    end
    [s,v] = listdlg('PromptString','Select reference image', 'SelectionMode','single', 'ListString',referenceList);
    
    if v
        selectedImage = imread([CommonHandles.DirName filesep CommonHandles.ReferenceImages{s}.Image]);
        
        refFig = figure;
        set(refFig,'numbertitle','off','name',referenceList{s})
        
        % prepare stretching
        if CommonHandles.MainView.StrechImage == 1
            
            % Needs to be verifyed this saturation value
            tolr = 0.0;
            tolg = 0.001;
            tolb = 0.005;
            
            sl(:,1) = [0,1]';
            sl(:,2) = stretchlim(selectedImage(:,:,2), tolg);
            sl(:,3) = stretchlim(selectedImage(:,:,3), tolb);
            selectedImage = imadjust(selectedImage,sl,[]);
        else
            sl(:, 1)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
            sl(:, 2)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
            sl(:, 3)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
            selectedImage = imadjust(selectedImage,sl,[]);
        end;        
        selectedImage(:, :, 1) = selectedImage(:, :, 1) * CommonHandles.MainView.Red;
        selectedImage(:, :, 2) = selectedImage(:, :, 2) * CommonHandles.MainView.Green;
        selectedImage(:, :, 3) = selectedImage(:, :, 3) * CommonHandles.MainView.Blue;

        
        imshow(selectedImage);
    end;
end

% --------------------------------------------------------------------
function cleverGuessToggletool_OnCallback(hObject, eventdata, handles)
% hObject    handle to cleverGuessToggletool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

uitoggletool2_OffCallback(hObject, eventdata, handles);
        
if (isfield(CommonHandles,'AC') && isfield(CommonHandles,'SamplingPool'))
    CommonHandles.ActiveLearning = 1;       

else   
    uiwait(AL_GUI);
    if (isfield(CommonHandles,'AC') && isfield(CommonHandles,'SamplingPool'))
        CommonHandles.ActiveLearning = 1;
    else
        set(hObject,'State','off');
    end
end

if (CommonHandles.ActiveLearning)    
    CommonHandles = CommonHandles.AC.proposeNextCellACC(CommonHandles);
    CommonHandles = jumpGuiToSelectedCell(CommonHandles,'NAME');
end

% --------------------------------------------------------------------
function cleverGuessToggletool_OffCallback(hObject, eventdata, handles)
% hObject    handle to cleverGuessToggletool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

set(handles.cleverGuessToggletool,'State','off');
CommonHandles.ActiveLearning = 0;

% --------------------------------------------------------------------
% pushtool for deleting a class, placed on menubar
function deleteClass_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to deleteClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiwait(deleteClass_GUI);


% --------------------------------------------------------------------
% pushtool for saving the current classes, placed on menubar
% saved file can be find in Classes as Classes.mat
% TODO set the path and filename
function saveClasses_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to saveClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiwait(saveClasses_GUI);


% --------------------------------------------------------------------
% pushtool for loading saved classes, placed on menubar
function loadClasses_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to loadClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiwait(loadClasses_GUI);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiwait(SetIntensityGUI);


% --- Executes on scroll wheel click while the figure is in focus.
% --- Zooming in and out on main selected image
function figure1_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
XBounds = CommonHandles.MainView.XBounds;
YBounds = CommonHandles.MainView.YBounds;

mainViewBoundaries = getpixelposition(handles.mainView);
pos = get(gcf,'CurrentPoint');

if mainViewBoundaries(4)/mainViewBoundaries(3) > CommonHandles.CurrentImageSizeX/CommonHandles.CurrentImageSizeY
    ratio = mainViewBoundaries(3)/CommonHandles.CurrentImageSizeY;
else
    ratio = mainViewBoundaries(4)/CommonHandles.CurrentImageSizeX;
end

pictureBoundaries(3) = CommonHandles.CurrentImageSizeY * ratio;
pictureBoundaries(4) = CommonHandles.CurrentImageSizeX * ratio;

pictureBoundaries(1) = (mainViewBoundaries(3)-pictureBoundaries(3))/2 + mainViewBoundaries(1);
pictureBoundaries(2) = (mainViewBoundaries(4)-pictureBoundaries(4))/2 + mainViewBoundaries(2);

if eventdata.VerticalScrollCount>0 % we're zooming out
    newSizeX = (XBounds(2) - XBounds(1))*1.2;
    newSizeY = (YBounds(2) - YBounds(1))*1.2;
    if newSizeX>CommonHandles.CurrentImageSizeY-1 newSizeX = CommonHandles.CurrentImageSizeY-1; end
    if newSizeY>CommonHandles.CurrentImageSizeX-1 newSizeY = CommonHandles.CurrentImageSizeX-1; end
else % we're zooming in
    newSizeX = (XBounds(2) - XBounds(1))*0.85;
    newSizeY = (YBounds(2) - YBounds(1))*0.85;
end
calcXYBounds(newSizeX, newSizeY, pos, pictureBoundaries);

CommonHandles = ShowSelectedCell(CommonHandles);

function calcXYBounds(newSizeX, newSizeY, pos, pictureBoundaries)
global CommonHandles;
XBounds = CommonHandles.MainView.XBounds;
YBounds = CommonHandles.MainView.YBounds;

posXInRegPlane = (pos(1) - pictureBoundaries(1))/pictureBoundaries(3)*(XBounds(2) - XBounds(1))+XBounds(1);
posYInRegPlane = (pos(2) - pictureBoundaries(2))/pictureBoundaries(4)*(YBounds(2) - YBounds(1))+YBounds(1);

XNewLower = posXInRegPlane - (pos(1)-pictureBoundaries(1))*newSizeX./pictureBoundaries(3);
if XNewLower<1
    XNewLower = 1;
end
XNewUpper = XNewLower + newSizeX;
if XNewUpper>CommonHandles.CurrentImageSizeY
    XNewUpper = CommonHandles.CurrentImageSizeY;
end
YNewLower = posYInRegPlane - (pos(2)-pictureBoundaries(2))*newSizeY./pictureBoundaries(4);
if YNewLower<1
    YNewLower = 1;
end
YNewUpper = YNewLower + newSizeY;
if YNewUpper>CommonHandles.CurrentImageSizeX
    YNewUpper = CommonHandles.CurrentImageSizeX;
end

CommonHandles.MainView.XBounds = [ XNewLower XNewUpper ];
CommonHandles.MainView.YBounds = [ YNewLower YNewUpper ];


% --------------------------------------------------------------------
function ShowMe_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to ShowMe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
    dialogForLoadingEveryFeature();
end

CommonHandles.SelectedClassifier = 'OneClassClassifier';

CommonHandles.AllFeaturesReduced = setupDataForOutlierTree();

outlierTree_GUI();

function dialogForLoadingEveryFeature

    d = dialog('Position',[300 300 250 150],'Name','LoadAllFeatures');
    txt1 = uicontrol('Parent',d,...
           'Style','text',...
           'Position',[20 90 130 25],...
           'String','Want to load every feature?');
       
    chb1 = uicontrol('Parent',d,...
           'Style','checkbox',...
           'Position',[180 90 50 25],...
           'String',{'Yes';'No'});
       
    btn = uicontrol('Parent',d,...c
           'Position',[40 30 70 25],...
           'String','OK',...
           'Callback',{@btn_callback,chb1});
       
    btn2 = uicontrol('Parent',d,...
       'Position',[140 30 70 25],...
       'String','Close',...
       'Callback','delete(gcf)');
          
    % Wait for d to close before running to completion
    uiwait(d);
   
function btn_callback(hObject, evenddata, chb1)
  global CommonHandles;
  if (get(chb1,'Value') == get(chb1,'Max'))
      [CommonHandles.AllFeatures,CommonHandles.AllFeaturesMapping,CommonHandles.AllCoordinates] = LoadAllFeatures(CommonHandles,1,1);
  end
  
  delete(gcf);


% Export feature-based statistics
function Untitled_30_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  export_feature_statisticsGUI;
  uiwait(gcf);

% --------------------------------------------------------------------
function occ_bintree_settings_Callback(hObject, eventdata, handles)
% hObject    handle to occ_bintree_settings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
outlierTreeSettingsGUI();

% --------------------------------------------------------------------
function uploadPushTool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uploadPushTool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

if CommonHandles.Success
    
    if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
        dialogForLoadingEveryFeature();
    end
    
    uploadSelectedCellsGUI();
    
end


% --------------------------------------------------------------------
function displayAnnotatedCells_toggleTool_OffCallback(hObject, eventdata, handles)
% hObject    handle to displayAnnotatedCells_toggleTool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if (CommonHandles.Success == 1)
    CommonHandles.DisplayAnnotatedCells = 0;
    CommonHandles = ShowSelectedCell(CommonHandles);
end;

% --------------------------------------------------------------------
function displayAnnotatedCells_toggleTool_OnCallback(hObject, eventdata, handles)
% hObject    handle to displayAnnotatedCells_toggleTool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if (CommonHandles.Success == 1)
    CommonHandles.DisplayAnnotatedCells = 1;
    CommonHandles = ShowSelectedCell(CommonHandles);
end


% --- Executes on button press in showSelectedPlateWell.
function showSelectedPlateWell_Callback(hObject, eventdata, handles)
% hObject    handle to showSelectedPlateWell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles,'SelectPlateWellVisible')
    if CommonHandles.SelectPlateWellVisible == 0
        set(CommonHandles.PlateSelectWindow, 'Visible', 'on');
        CommonHandles.SelectPlateWellVisible = 1;
%         select_plate_wellGUI();
    else
        set(CommonHandles.PlateSelectWindow, 'Visible', 'off');
        CommonHandles.SelectPlateWellVisible = 0;
%         close(get(CommonHandles.PlateListHandle,'Parent'));
    end
end


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

% eventdata

switch eventdata.Key
    % switching between contour view and color view
    case 'c' 
        CommonHandles.ShowContour = ~CommonHandles.ShowContour;
        
        set(handles.checkbox2,'Value',CommonHandles.ShowContour);
        
        if (CommonHandles.Success == 1)
            CommonHandles = ShowSelectedCell(CommonHandles);
        end
    % switch cell numbers on/off
    case 'n'
        if (CommonHandles.Success == 1)
            CommonHandles.ShowCellNumbers = ~CommonHandles.ShowCellNumbers;
            if CommonHandles.ShowCellNumbers
                set(handles.uitoggletool3, 'State','on');
            else
                set(handles.uitoggletool3, 'State','off');
            end
            CommonHandles = ShowSelectedCell(CommonHandles);
        end
    % switch cell numbers on/off
    case 't'
        if (CommonHandles.Success == 1)
            CommonHandles.DisplayAnnotatedCells = ~CommonHandles.DisplayAnnotatedCells;
            if CommonHandles.ShowCellNumbers
                set(handles.displayAnnotatedCells_toggleTool, 'State','on');
            else
                set(handles.displayAnnotatedCells_toggleTool, 'State','off');
            end
            CommonHandles = ShowSelectedCell(CommonHandles);
        end
    % switch red channel on/off
    case 'r'
        if (CommonHandles.Success == 1)
            CommonHandles.MainView.Red = ~CommonHandles.MainView.Red;
            
            if CommonHandles.MainView.Red
                set(handles.uitoggletool6, 'State','on');
            else
                set(handles.uitoggletool6, 'State','off');
            end
            CommonHandles = ShowSelectedCell(CommonHandles);
        end
    % switch green channel on/off    
    case 'g'
        if (CommonHandles.Success == 1)
            CommonHandles.MainView.Green = ~CommonHandles.MainView.Green;
            
            if CommonHandles.MainView.Green
                set(handles.uitoggletool4, 'State','on');
            else
                set(handles.uitoggletool4, 'State','off');
            end
            CommonHandles = ShowSelectedCell(CommonHandles);
        end
    % switch blue channel on/off
    case 'b'
        if (CommonHandles.Success == 1)
            CommonHandles.MainView.Blue = ~CommonHandles.MainView.Blue;
            
            if CommonHandles.MainView.Blue
                set(handles.uitoggletool5, 'State','on');
            else
                set(handles.uitoggletool5, 'State','off');
            end
            CommonHandles = ShowSelectedCell(CommonHandles);
        end
    % add selected cell to the class belongs to pressed number
    case {'1','2','3','4','5','6','7','8','9'}
        if isempty(eventdata.Modifier)
            if str2num(eventdata.Key)<=length(CommonHandles.Classes)
                trainFun = get(CommonHandles.Classbuttons{str2num(eventdata.Key)}, 'Callback');
                trainFun{1}(CommonHandles.Classbuttons{str2num(eventdata.Key)},[],trainFun{2:end});
            end
        elseif length(eventdata.Modifier)==1 && strcmp(eventdata.Modifier{1},'control')
            if str2num(eventdata.Key)<=length(CommonHandles.Classes)
                showFun = get(CommonHandles.ShowTrainedCells{str2num(eventdata.Key)}, 'Callback');
                showFun{1}(CommonHandles.Classbuttons{str2num(eventdata.Key)},[],showFun{2:end});
            end
        end
        
    % moving to previous image
    case 'pageup'
        if CommonHandles.Success
            listbox2Handle = findobj('Tag', 'listbox2','Parent',CommonHandles.PlateSelectWindow);
            
            currImNum = get( listbox2Handle, 'Value');
            if currImNum>1
                
                set( listbox2Handle, 'Value', currImNum-1);
                CommonHandles.SelectedImage = currImNum-1;
                ImageList = get(listbox2Handle,'String');
                
                CommonHandles.SelectedImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
                CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
                
                CommonHandles = LoadSelectedImage(CommonHandles);
            end
        end
    % moving to next image
    case 'pagedown'
        if CommonHandles.Success
            
            listbox2Handle = findobj('Tag', 'listbox2','Parent',CommonHandles.PlateSelectWindow);
            
            ImageList = get(listbox2Handle,'String');
            listbox2Length = length( ImageList );
            currImNum = get( listbox2Handle, 'Value');
            
            if currImNum<listbox2Length
                
                set( listbox2Handle, 'Value', currImNum+1);
                CommonHandles.SelectedImage = currImNum+1;
                
                CommonHandles.SelectedImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
                CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
                
                CommonHandles = LoadSelectedImage(CommonHandles);
            end
        end
    % open new project
    case 'o'
        if length(eventdata.Modifier)==1 && strcmp(eventdata.Modifier{1},'control')
            uipushtool9_CB = get(handles.uipushtool9,'ClickedCallback');
            uipushtool9_CB(handles.uipushtool9,[]);
        end
    % switch stretch intensities    
    case 's'
        if CommonHandles.Success
            if isempty(eventdata.Modifier)
                CommonHandles.MainView.StrechImage = ~CommonHandles.MainView.StrechImage;
                selectedCell = CommonHandles.SelectedCell;
                CommonHandles = LoadSelectedImage(CommonHandles);
                CommonHandles.SelectedCell = selectedCell;
                CommonHandles = ShowSelectedCell(CommonHandles);
                set(handles.StrechIntensityCheckBox,'Value',CommonHandles.MainView.StrechImage);
            elseif length(eventdata.Modifier)==1 && strcmp(eventdata.Modifier{1},'control')
                uipushtool8_CB = get(handles.uipushtool8,'ClickedCallback');
                uipushtool8_CB(handles.uipushtool8,[]);
            end
        end
    case 'uparrow'
        if CommonHandles.Success
            % select closest point
            currentCellX = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1);
            currentCellY = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2);
            %currentCellH = 1;
            rotMatrix = [cos(pi/4) -sin(pi/4) 0; sin(pi/4) cos(pi/4) 0; 0 0 1];
            currentImageCellCoordinates = [CommonHandles.SelectedMetaData(:,1)-currentCellX CommonHandles.SelectedMetaData(:,2)-currentCellY ones(size(CommonHandles.SelectedMetaData,1),1)]*rotMatrix;
            
            dists = sqrt(currentImageCellCoordinates(:,1).^2+currentImageCellCoordinates(:,2).^2);
            
            [orderedDists, index] = sort(dists);
            
            northQuarterIndices = find((currentImageCellCoordinates(index,1)<=0 & currentImageCellCoordinates(index,2)<=0));
            
            if length(northQuarterIndices)>1
                CommonHandles.SelectedCell = index(northQuarterIndices(2));
                ShowSelectedCell(CommonHandles);
            end
            
        end
    case 'downarrow'
        if CommonHandles.Success
            % select closest point
            currentCellX = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1);
            currentCellY = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2);
            %currentCellH = 1;
            rotMatrix = [cos(pi/4) -sin(pi/4) 0; sin(pi/4) cos(pi/4) 0; 0 0 1];
            currentImageCellCoordinates = [CommonHandles.SelectedMetaData(:,1)-currentCellX CommonHandles.SelectedMetaData(:,2)-currentCellY ones(size(CommonHandles.SelectedMetaData,1),1)]*rotMatrix;
            
            dists = sqrt(currentImageCellCoordinates(:,1).^2+currentImageCellCoordinates(:,2).^2);
            
            [orderedDists, index] = sort(dists);
            
            northQuarterIndices = find((currentImageCellCoordinates(index,1)>=0 & currentImageCellCoordinates(index,2)>=0));
            
            if length(northQuarterIndices)>1
                CommonHandles.SelectedCell = index(northQuarterIndices(2));
                ShowSelectedCell(CommonHandles);
            end
            
        end
    case 'leftarrow'
        if CommonHandles.Success
            % select closest point
            currentCellX = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1);
            currentCellY = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2);
            %currentCellH = 1;
            rotMatrix = [cos(pi/4) -sin(pi/4) 0; sin(pi/4) cos(pi/4) 0; 0 0 1];
            currentImageCellCoordinates = [CommonHandles.SelectedMetaData(:,1)-currentCellX CommonHandles.SelectedMetaData(:,2)-currentCellY ones(size(CommonHandles.SelectedMetaData,1),1)]*rotMatrix;
            
            dists = sqrt(currentImageCellCoordinates(:,1).^2+currentImageCellCoordinates(:,2).^2);
            
            [orderedDists, index] = sort(dists);
            
            northQuarterIndices = find((currentImageCellCoordinates(index,1)<=0 & currentImageCellCoordinates(index,2)>=0));
            
            if length(northQuarterIndices)>1
                CommonHandles.SelectedCell = index(northQuarterIndices(2));
                ShowSelectedCell(CommonHandles);
            end
            
        end
    case 'rightarrow'
        if CommonHandles.Success
            % select closest point
            currentCellX = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1);
            currentCellY = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2);
            %currentCellH = 1;
            rotMatrix = [cos(pi/4) -sin(pi/4) 0; sin(pi/4) cos(pi/4) 0; 0 0 1];
            currentImageCellCoordinates = [CommonHandles.SelectedMetaData(:,1)-currentCellX CommonHandles.SelectedMetaData(:,2)-currentCellY ones(size(CommonHandles.SelectedMetaData,1),1)]*rotMatrix;
            
            dists = sqrt(currentImageCellCoordinates(:,1).^2+currentImageCellCoordinates(:,2).^2);
            
            [orderedDists, index] = sort(dists);
            
            northQuarterIndices = find((currentImageCellCoordinates(index,1)>=0 & currentImageCellCoordinates(index,2)<=0));
            
            if length(northQuarterIndices)>1
                CommonHandles.SelectedCell = index(northQuarterIndices(2));
                ShowSelectedCell(CommonHandles);
            end
            
        end
    otherwise
%         eventdata
end
